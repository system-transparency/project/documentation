# Meeting minutes

  - **Date**: 2023-09-07 1000-1050 CEST
  - **Meet**: https://meet.sigsum.org/st

## Who

  - ln5
  - kai
  - philipp

rgdd will also be a fly on the wall, to take some notes.

## Agenda

  - What are the interfaces/API in ST?
  - Action plan from here on?

## Before the meet

ln5: in preparation for talking about interfaces, you might want to check out
branch interfaces of proj/documentation and cd docs.system-transparency.org &&
make to build the graph we're talking about. you'll need dot (package graphviz,
i think) and if you want the full site also hugo to be installed"

build flow:
https://git.glasklar.is/system-transparency/core/system-transparency/-/blob/main/doc/guide.md

## Running notes

The colored things: interfaces.

blue: json files on disk, can live for a long time (as opposed to network
protocols)

red: command-line interface (only one that we so far identified as interesting
to version / be cautios about how it is updated.)

green: network protocols

Linus: we should have specs specifying all those things.  Specification for
command line interface might not be right, we could call it "man page".

  - Key questions?
  - What format?
  - Where?
    - Probably system transaprency project/doc repo, at least for now

If we have machine readable specs: we can do lots of other interesting stuff,
lie some things can be automated.  Like regression.

Kai: has been thinking about JSON schemas.  (Learned that in immune, was nice.
ANd then you could check if implementation violates the schema.  Only works for
protocols that are just like JSON, protobuf, or something like that though.)

Set of test cases already helps a bit though.

Is putting in all the effort worth it right now if we mean for them to change?

Linus: we need a current picture regardless, what do we have right now.  THat's
what pilot users are using.

Tests should probably live in the Go module / package.

If you are the producer, you're the one that should also test that it parses?
E.g., more effort into stmgr to know what it is producing to make sure that what
it is producing = as expected.

endorse:
- now specified, MR waiting.
  - https://git.glasklar.is/system-transparency/project/documentation/-/merge_requests/13
- just flat files: TODO@Kai: can you expand this?
- problem with measurements: not just a file, and not a protocl.  It's the data
  we measure into the TPM, which registers, type fields, ...
- maybe its more towards protocol

Currently unclear what Jens has already done, but should probably assume that he
pushed whatever he has done so far.

Kai has written schema's before.  You can't express everything in the schema
though.  It gives you some basic checks / parsing.  Will still need human
language too.  rgdd can review schema if needed, familiar with that.

"Think of the implementer, can they implement it?"

Example of Sigsum's log protocol:
- https://git.glasklar.is/sigsum/project/documentation/-/blob/main/log.md

Kai's current idea: one document per thing in the reference folder, diataxis
framework.  Everyone seems to think that's a good thing to start with.

Linus: would like to avoid test-vectors in the docs though, should probably be
closer to code.

rgdd: would be useful to get the figure explained in text.

**Action point:** Philipp will put this as the overviev in the reference folder,
"index", Linus is happy to review.

**Action point:** Philipp and Kai will start hashing out docs for the colored
boxes and ask for review.

Each and every interface has the description of the content....the other part is
the location/address.  Where do stboot look for trust policy, etc.  Logivally it
looks like something stboot decides, but the owner of the interface = says where
it lives.  E.g., in a file system, or elsewhere.

Think of case "you don't have stboot, where should i start looking for things -
where to find them and what the format will be".  The specs needs to be clear
about this.

**Action point:** make it so that everyone can push stuff to doc repo, still
makes sense to as to review.  We need to get the baseline going before being
very caustios about changes.
