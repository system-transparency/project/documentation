# Proposal to allow relative os_pkg_url in Descriptor

## Motivation

The host configuration field `ospkg_pointer` contains one or more URLs
to ospkg json files (also called "Descriptor"). stboot fetches an
ospkg json file and proceeds to get hold of the `os_pkg_url` field in
it. The `os_pkg_url` is currently expected to contain the complete URL
to the ospkg zip file -- including http(s) scheme (and optionally
user:pass for http auth). stboot fetches this URL.

It would be practical if the `os_pkg_url` could be relative to the
`ospkg_pointer`, instead of only a complete, absolute URL. This way we
can do the following without having to poke in the json:

- serve the ospkg json/zip file-pair from a different URL easily

- serve the ospkg files behind HTTP auth, without needing the same
  auth credentials inside the json -- building of ospkg can then be
  done by somebody not knowing these credentials (or the deployment
  URL)

## Proposed changes

### stboot program

stboot should not directly fetch `os_pkg_url`. First combine the
`descriptorURL` and the `os_pkg_url` found in the Descriptor, then
fetch that.

It looks like go's
[.ResolveReference](https://pkg.go.dev/net/url#URL.ResolveReference)
can be used to join these, while maintaining backward compatibility.

See also experiment with that function in the issue:
https://git.glasklar.is/system-transparency/core/stboot/-/issues/190

### OS package specification

Document that `os_pkg_url`, in addition to being an HTTP(S) URL, also
can be relative to the Descriptor URL. And if there are any
constraints regarding this.
