# 2023-05-12

This documents contains the current ST road map.

This document will not change but an updated version of is to be
expected within three months or so.

## Artefacts, ie things we release, publish and maintain

- A.1. stlib
  - stlib (golang library for All The Things ST)
  - Tooling for building an os-pkg
  - Tooling for signing an os-pkg
- A.2. stboot
- A.3. stprov
- A.4. stauth (implementing STAM, server and client)
- A.5. stvmm
- A.6. Documentation
  - A.6.1. ST vision (kfreds)
  - A.6.2. ST design
    - Including a threat model, and clearly stating what is in scope
      and out of scope for ST in each of its supported configurations
      (based on the yet to be realised configuration-and-properties
      matrix)
  - A.6.3. ST specification (ln5)
    - Defining interfaces, in the broader sense. Including stboot
      trust policy and host configuration formats, the os-pkg format,
      the stauth quote format, and more.
  - A.6.4. High level intuitive description *with pictures* (ln5)
  - A.6.5. System documentation
    - Bridging the gap between the bigger picture from A.6.4 and
      documentation for each artefact

## Releases and what new functionality they provide

Releases are listed in *roughly* chronological order.

- R.1. Network booted stboot
  - stboot v0.5.0
    - Tooling and documentation for network booting stboot as a UEFI application
      - Support for being network booted over HTTP(S)
      - Documentation on how to do PXE booting
  - project/st-shim-review v1.0.0
    - ST UEFI shim submitted for review, with documentation of the process of
      getting a shim signed
  - project/sthsm v1.0.0
  - project/documentation vR.1.

- R.2. Provisioning PoC
  - stprov v0.1.0
    - Proof of concept provisioning tool
  - project/documentation vR.2.

- R.3. Basic documentation (ln5, jens, kfreds)
  - project/documentation vR.3.
    - ST specification (A.6.3.)
    - High level documentation (A.6.4.)
    - System documentation (A.6.5.)

- R.4. stboot reborn
  - stboot v0.10.0
    - stboot is UEFI-booted from an ISO
    - Support for reading configuration from either EFI variables or initramfs
    - New general os-pkg fetch mechanism, supporting HTTPS, local storage and initramfs
    - Using Sigsum signatures for verifying os-pkgs
  - project/documentation vR.4.

- R.5. The vision
  - project/documentation vR.5.
    - The vision (A.6.1.)

- R.6. Verification PoC
  - stauth v0.1.0
    - Proof of concept of server verification using TPM quotes

- R.7. Something to boot, take one
  - stlib v1.0.0 (A.1.)
    - Building reproducible os-pkgs based on Debian
    - Go library packages moved here from stboot
  - project/documentation vR.7.

- R.8. Provisioning
  - stprov v1.0.0
    - A drop in replacement for MPT
  - project/documentation vR.8.

- R.9. coreboot -> stboot
  - stlib vTBD
    - stboot booted by coreboot
  - stboot vTBD
    - stboot reading its configuration from CBFS
  - project/documentation vR.10.

- R.11. Virtualization PoC
  - stvmm v0.1.0
    - Proof of concept ST VM manager
    - Technology used: TBD

- R.11. Verification
  - <artefact TBD> <version TBD>
    - TBD what to include
    - TBD including credential binding?
  - project/documentation vR.11.


### Release dates

| Date       | Release | Who       |
|------------|---------|-----------|
| 2023-05-19 | R.1.    | ln5       |
| 2023-05-29 | R.2.    | ln5       |
| 2023-06-19 | R.3.    | ln5, jens |
| 2023-06-19 | R.4.    | jens      |
| 2023-06-19 | R.5.    | kfreds    |

