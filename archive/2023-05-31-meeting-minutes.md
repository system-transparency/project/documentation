# System Transparency weekly

  - Date:  2023-05-31 1200-1250 UTC
  - Meet:  https://meet.sigsum.org/ST-weekly
  - Pad:   https://meet.sigsum.org/p/ST-20230531
  - Chair: ln5

## Agenda

  - Hello
  - Status rounds
  - Decisions
  - Next steps
  - Other

## Hello

  - ln5
  - Jens
  - Kai
  - Foxboron

## Status rounds

  - ln5
    - R.1. and R.2.: Planning, reviewing, testing, installing, documenting.
  - Philipp:
    - Fixing remaining issues blocking the sthsm release
  - Jens:
    - R2 things
  - Foxboron: Doing some releases, stprov, implemented -I for the Mole
  - Kai: stauth work, precomputing expect PCR measurements (stboot and
    ospkg); working for all but two measurements

## Decisions

  - No decisions.

## Next steps

  - ln5
    - Continued work on R.1. and R.2.
    - Asking for feedback on proposed release process https://pad.sigsum.org/p/uMgpUpqVFZSptoqga1ZG#L5
  - Philipp:
    - Add unit tests and verification for multiple sthsm core library components
  - Jens:
    - Refactor Taskfiles, Documentation
	- Foxboron:
		Try to reproduce the ospkg slow downloads
  - Kai
    - Finish "core feature" of remote attestation PoC, at least for qemu

## Other

  - Releases, status update https://pad.sigsum.org/p/uMgpUpqVFZSptoqga1ZG
    - R.1. has a new release date of 2023-06-13
    - R.2. has a new release date of 2023-06-01
    - The [roadmap][] says that we have three more releases before summer holidays kick in, all of them on 2023-06-19
      - R.3. Basic documentation (ln5, jens, kfreds)
      - R.4. stboot reborn (jens)
      - R.5. The vision (kfreds)

  - ln5 said he'd seen block diagrams of a supermicro motherboard with
    three (3) TPM's but in reality, there were only two
    https://www.servethehome.com/wp-content/uploads/2023/05/Supermicro-SYS-111C-NR-1U-Block-Diagram.jpg

[roadmap](https://git.glasklar.is/system-transparency/project/documentation/-/blob/main/archive/2023-05-12-roadmap.md)

