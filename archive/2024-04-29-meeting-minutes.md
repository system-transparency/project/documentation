# System Transparency weekly

- Date: 2024-04-29 1200-1250 UTC
- Meet: https://meet.sigsum.org/ST-weekly
- Chair: rgdd
- Secretary: rgdd

## Agenda

- Hello
- Status rounds
- Decisions
- Next steps
- Other

## Hello

- zaolin
- kai
- kfreds
- nisse
- ln5
- philipp
- jens

## Status rounds

- rgdd: lots of issues with kernel modules and our super micro; these issused
  might be resolved now but to be verified and post-debug cleanup is needed.
  - https://git.glasklar.is/system-transparency/core/stprov/-/issues/63
  - https://git.glasklar.is/system-transparency/core/stprov/-/merge_requests/66
  - https://git.glasklar.is/system-transparency/core/stboot/-/issues/191
- rgdd: bunch of MRs for docs.sto and misc review + releng stuff
- ln5: documentation and infrastructure work done (related to the release)
- zaolin: Managed to built a 7mb image with yocto systemd + musl libc. Still
  trying to get it smaller by reducing systemd features and manually stripping
  out artifacts from the image.
- kai: I finished the EK cert & AIK validation code including comments
- nisse: release wrapup, mainly for stboot.
- jens: conferenes and meets last week, not much to report this week. But would
  like to talk with fredrik this week or the next week regarding coreboot
  things/support.

## Decisions

- Decision? Relative os_pkg_url
  - https://git.glasklar.is/system-transparency/project/documentation/-/merge_requests/59
  - Defer for decision until next week, everyone reads in advance please
  - Suggestion seems to be that this will be backwards-compatible

## Next steps

- ln5: ST release loose ends; signing, web, testing infra
- rgdd: ST release loose ends too, esp. stprov and docs
- kai: wiring up all components for remote attestation enrollement, creating
  some testdata once the release is out.
- zaolin: Finishing component & size reduction tests and writing/finish
  proposal. Starting with preparing PoC when Fredrik approves.
- kfreds: talk to philipp and jens
- nisse: ST release, first prio stboot, second stmgr.
- jens: would like to tune my tutortials for what will be on docs.sto.
  - rgdd: it has been merged into main, take a look there

## Other

- zaolin: Found out there is an AMD Genova OpenSIL + coreboot + LinuxBoot
  supermicro supported server which can be bought as COTS. Supermicro H13SSL-N
  board
- rgdd: would like to sync a bit more with nisse and ln5 (breakout), topic what
  (still) needs to be done to wrap up releases
- kfreds: breakout session with jens and philipp
