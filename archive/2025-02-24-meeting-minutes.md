# System Transparency weekly

- Date: 2024-02-24 1200-1250 UTC
- Meet: https://meet.glasklar.is/ST-weekly
- Chair: rgdd
- Secretary: rgdd

## Agenda

- Hello
- Status rounds
- Decisions
- Next steps
- Other

## Hello

- elias
- rgdd

## Status rounds

- elias: debugging, found "Initramfs unpacking failed: uncompression error"
  https://git.glasklar.is/system-transparency/core/stboot/-/issues/224#note_23410
  - elias will add note to archive and close issue, it's not an stboot problem
- elias: planning initial ST deploy for Glasklar servers
  - still nothing public to link, but we want that to exist $soon

## Decisions

- None

## Next steps

- elias: stprov testing, preparing for release of stprov v0.4.x
  - ask rgdd for help/input if needed
- elias: review and merge strmgr fix from quite: " Ignore -rootCert for real if
  also -isCA; ensure warning is shown"
  - https://git.glasklar.is/system-transparency/core/stmgr/-/merge_requests/102
- rgdd: page into SB in stprov (finally?), starting wed-fri

## Other

- elias: last friday -- talked a bit about st with fredrik. How it's described,
  website, etc. fredrik drawed an idea for a new structure website. One thing
  about it: would like it to be more clear that st is not just a suite of
  program/software. It's also a concept. And ST as a concept -> can be broader /
  more generally useful. And then there's ST's implementation of that concept.
  So on the website, it should be visible that is e.g. "concept" and there are
  things describing the concepts. And then there is another part
  "implementation"? But point is: make it more clear that concept and
  implementation is not the same thing?
  - So: We talked a bit about this, and that we should talk about this with the
    rest of you with at some point.
  - landing page rigth now is mostly concept, and docs.sto is ~only
    implementation
  - rgdd: from the top of my head -- www.sto feels like a good place to write
    about concepts and link to implementation; docs.sto is implementation (and
    things you need to know when you're using the implementation).
  - rgdd: to me a blog is more important/urgent
  - elias: we talked about that too, and that blog could fit under the ST
    umbrella
