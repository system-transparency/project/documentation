# System Transparency weekly

  - Date:  2024-03-04 1200-1250 UTC
  - Meet:  https://meet.sigsum.org/ST-weekly
  - Chair: nisse
  - Secretary: ln5

## Agenda

  - Hello
  - Status rounds
  - Decisions
  - Next steps
  - Other

## Hello

  - jens
  - rgdd
  - ln5
  - nisse
  - kai
  - kfreds
  - zaolin

## Status rounds

  - jens: ST is running as coreboot payload on QEMU, writing up docu/guides
    - tight on space (ROM size, 16MB), kernel is currently around 3-4M, initramfs at 10-11MB (mostly the go binary, not the actual files) -- and the rest coreboot uses.
  - rgdd: update trust policy documentation, a few minor MRs and wip compat network interfaces implementation (yet to be MR:ed, only in a branch for now).
  - kai: finished the remote attestation design doc, worked on implementing the TPM interactions; seems like we might be able to get rid of the gotpm dependencies
  - zaolin: finished stboot/systemd prototype docs. Added a bunch of scripts of code for building initial version starting to replace parts of the stboot with systemd components.
  - nisse: back from vacation

## Decisions

  - No decisions.

## Next steps

  - jens: stboot as coreboot payload on SiFive HiFive unmatched mainboard
  - rgdd: wrap up compat network interfaces, similar refactor/fixup things for OS package documentation, move naming of the TLS roots file forward in both stboot and stprov (unless nisse wants to do it), and more stprov milestone if time permits.
  - kai: I'll continue writing the TPM code and get back to the document in case kfreds has feedback/changes
  - ln5: Updating the ST quickstart / getting started documentation to a) remove the use of tasks and b) cover both virtual and hardware targets
  - zaolin: Writing more code and scripts for the stboot/systemd prototype, running tests in qemu for replacing components and documenting it in parallel.
  - kfreds: Give Jens, Philipp and Kai feedback as necessary.
  - nisse: "Fix the linter things"

## Other

  - kfreds: OK to ask some misc sw. dev. questions?
