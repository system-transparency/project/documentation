# System Transparency weekly

  - Date:  2023-09-25 1200-1250 UTC
  - Meet:  https://meet.sigsum.org/ST-weekly
  - Chair: jens
  - Secretary: rgdd

## Agenda

  - Hello
  - Status rounds
  - Decisions
  - Next steps
  - Other

## Hello

  - rgdd
  - ln5
  - nisse
  - quite
  - jens
  - philipp
  - kai

## Status rounds

  - kai: finished opening all docs-related MRs. Still need to be merged. Did
    some futher stvmm research:
    - https://git.glasklar.is/system-transparency/project/documentation/-/tree/kai/stvmm?ref_type=heads
    - maybe both good and bad ideas, input would be much welcomed!
  - nisse: 
    - Logging cleanup in review,
      https://git.glasklar.is/system-transparency/core/stboot/-/merge_requests/108,
      question on timestamps in stboot logs.
    - Merged go.mod file specifying versions under tests. Want to try out stboot
      as init, without any u-root executable in the booted system.
  - philipp:
    - Finished all previous mentioned docs MR, still need integrate feedback tho.
    - Getting started tutorial is on hold until remaining docs of the current
      state (howtos, explanation, references) are integrated.
    - So: currently blocked by not getting open doc MRs merged
  - rgdd: review and a bit of reading, nothing concrete to share at the moment
  - quite: not so much work; review some proposals, begin structuring own notes
  - jens: try to catch up on all MRs and documentation stuff, look into scheme
    definitions / JSON Schema et al. / pushed early versions of trust policy and
    host config spec
  - ln5: a little bit of review and work on documentation related things
		
## Decisions

   - Decision: Carbon-copy Sigsum's license/authorship procedures
     - https://git.glasklar.is/system-transparency/core/system-transparency/-/issues/218
     - In short: 2-clause BSD license, no CLAs or other copyright paperwork. High-level AUTHORS file listing contributors, details only in commit history.
     - [Philipp] Let's not forget checking dependcy licenses for the project
       license compliance there are some checker tools out there:
       https://github.com/google/go-licenses. Future work.
  - Decision?  On gitlab conventions/processes
    - [nisse] gitlab conventions/processes: How to use the assignee / reviewer
      roles, who should press the merge button? Proposal (similar to sigsum
      conventions): Assignee is responsible for driving the issue forward, so
      every MR should have an assignee. The assignee is expected to upload
      revisions and ultimately merge (or close) the MR, after it has been
      approved and issues resolved. Reviewer is expected to review, propose MR
      improvements as well as followup work, and pressing approval button when
      happy with merging of the MR (to reduce review roundtrips, can also do
      conditional approve: press the approve button, but also write a comment "I
      approve MR modulo the following issues that should like to see fixed in
      this way"). If the assignee does not have merge privileges, coordinate
      with a maintainer that can merge; for clarity, the MR can be reassigning
      to the merger at this point.
      - [kai] I'd also add the person making the comment should also be
        responsible for marking the thread as resolved or provide further
        feedback.
      - [kai] second, how do we add multiple reviews, for example I added Linus
        but nisse also want to take a look.
        - [rgdd]: page them with a comment that tags them asking for review?
      - [kai] in situations were we're stuck and want the feedback from the
        general st-crew, how long do we keep a MR open until we interpret a lack
        of response as abstaining from a vote.
        - [rgdd]: assignee pokes $relevant-folks if this is unclear / more input
          needed?
    - Defer, nisse will put together a more concrete proposal with input from
      $others until next week.
  - Decision:  Try commitlint in the stprov repo until next release, then re-evaluate
    - https://git.glasklar.is/system-transparency/project/documentation/-/merge_requests/29
  - Decision? should we archive the st repositories on Github and link to the
    Gitlab?
    - rgdd: think the current standing decision is "mirror" from gitlab to
      github?  I would not be ready to push this towards a decision today, would
      like to "Other" or "Defer" this.
    - linus: will take a look at getting the mirroring work.  Add in the GH
      description "Mirror of git.glasklar.is/system-transparency/..."
    - kai: can also set a description linking/describing the place where dev is
      happening on a st group level
  - Decision: Making it easier to run integration tests on subtool-changes
    - https://git.glasklar.is/system-transparency/project/documentation/-/blob/main/proposals/2023-09-11-tweakable-integration-tests.md?ref_type=heads
    - (adjourned from last week)
    - Q/nisse: is this a direction that makes sense, something that nisse can do
      incremental work on?
    - jens: no logic in ci file makes sense
    - we seem happy to continue on this incrementally

## Next steps

  - linus: take a look at GL -> GH mirroring, and specify in descriptions where
    appropriate that dev is happening on GL.
    - also update README of main repo to point at git.glasklar.is
    - also also delete (?) all repos that are not the main repo
  - linus: no other large dev stuff going on, let me know how i can unblock you
  - linus: will pickup the building situation, to ensure we have enough CI
    runners
  - kai: extend the stboot+TPM proposal, now that I have more feedback. Getting
    some changes into stboot that are still unmerged. Looking into testing esp.
    for stboot and stprov. Some stvmm research on the side.
  - rgdd: try to move the needle forward wrt. testing in stprov, then grab a
    feature-request issue
  - philipp: 
    - Working now on the documentation page theme and general page issues.
    - Trying to help out when needed with finishing the remaining missing
      documentation
  - nisse: Write up proposal on gitlab conventions
  - nisse: couple of small-ish MRs and continue looking at testing things
  - quite: continue organizing my notes into some kind of project doc on my side
  - jens: catch up remaining MRs, esp. documentation stuff.  Clean-up stboot
    related things.  And if we have desc. of current state, want to work on a
    next spec of os pkgs (so that there can be a delta)
  - linus: look at versioning of docs in the hugo template
  - jens: setup commitlint for stprov ci
  - linus: find the notes from all systems go summit discussion, share with
    everyone

## Other

  - What about discussion from all systems go summit?
    - everyone please read and provide input after linus shared notes /philipp
    - then we can do a separate discussion in a meet with higher bandwidth
