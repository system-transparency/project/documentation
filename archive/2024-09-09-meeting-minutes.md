# System Transparency weekly

- Date: 2024-09-09 1200-1250 UTC
- Meet: https://meet.glasklar.is/ST-weekly
- Chair: nisse
- Secretary: rgdd

## Agenda

- Hello
- Status rounds
- Decisions
- Next steps
- Other

## Hello

- rgdd
- zaolin
- nisse
- ln5
- kai

## Status rounds

- zaolin: Tried to build stimage.json/zip via ubuntu/gentoo and failed. Started
  to validate Kai's remote attestation work. Because of OSFC not much time for
  doing so.
  - stimages repo -- had issues. Will need to investigate, but kind of hard to
    use. Couldn't use it under ubuntu.
  - ln5: bug reports welcome
- nisse: Have an MR lying around to delete all task files:
  https://git.glasklar.is/system-transparency/core/system-transparency/-/merge_requests/286
  - rgdd: would be nice to get merged and cleaned-up
- kai: mostly working on trying out philipp's stboot with systemd things. Had
  some weird issues only I had, and kind of fixed it now. Going to add some
  documentation, because it was kind of missing. Then hopefully philipp can get
  the RA things running.

## Decisions

- None

## Next steps

- zaolin: Finish reviewing Kai's remote attestation work. Starting with planning
  as Fredrik mentioned in sync together with Kai.
- kai: what philipp said -- next big thing is to figure out milestones for
  stvmm. Not 100% clear yet.

## Other

- None
