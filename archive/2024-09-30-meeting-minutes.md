# System Transparency weekly

- Date: 2024-09-30 1200-1250 UTC
- Meet: https://meet.glasklar.is/ST-weekly
- Chair: rgdd
- Secretary: rgdd

## Agenda

- Hello
- Status rounds
- Decisions
- Next steps
- Other

## Hello

- rgdd
- Kai (async -- not here)
- nisse

## Status rounds

- kai: implemented measuring and quoting an stvmm-started VM, modified the
  remote attestation project to compute the expected PCR values.

## Decisions

- None

## Next steps

- kai: set up a test case for stboot -> ospkg w/ stvmm -> VM ospkg running in
  qemu to test the remote attestation.

## Other

- None
