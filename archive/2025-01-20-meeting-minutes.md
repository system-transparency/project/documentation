# System Transparency weekly

  - Date:  2025-01-20 1200-1250 UTC
  - Meet:  https://meet.glasklar.is/ST-weekly
  - Chair: nisse
  - Secretary: elias

## Agenda

  - Hello
  - Status rounds
  - Decisions
  - Next steps
  - Other

## Hello

  - elias
  - rgdd
  - ln5
  - nisse

## Status rounds

  - elias: considering getting new hardware for easier ST testing - possibly one racked server each for elias, nisse, rgdd, so that we can use those to learn how to run thing using ST. There is already a machine called stime that can be used for tests now.
  - elias: thinking about how to handle version info: https://git.glasklar.is/system-transparency/core/stmgr/-/issues/63
       - compare to how it sorks for stboot, where Go tooling is used to find out if local changes have been made. should probably do the same for stmgr. (see also sigsum)
  - rgdd: fyi -- bumped milestone date
    - reasonable to assume that st-v1.2.0 is out the door end of jan?
          - nisse says: stboot, yes. hopefully collection also
    - https://git.glasklar.is/groups/system-transparency/-/milestones/24#tab-issues
  - rgdd: fyi -- bumped milestone date for sigsum-loggeg OS packages (tentative date, relates to next iteration of roadmap planning)
    - reasonable to expect a collection release is out the door end of feb?
      - nisse: seems to be the right ballpark
    - https://git.glasklar.is/groups/system-transparency/-/milestones/25#tab-issues
  - rgdd: fyi -- bumped milestone date (tentative date, relates to next iteration of roadmap planning)
    - reasonable to expect collection release is out the door end of feb?
      - rgdd: thinks this is the right ballpark
    - https://git.glasklar.is/groups/system-transparency/-/milestones/25#tab-issues
  - rgdd, ln5: making progress on updated roadmap, still just a drafty draft that is very subject to change after further input/discussion.  I also didn't have time to go through the notes to see if I forgot something from last week when meeting linus in person, so the below is a commit for where i'm at right as this meeting starts!
    - https://git.glasklar.is/system-transparency/project/documentation/-/blob/roadmap-feb/archive/2025-02-03-roadmap.md
  - nisse: did some smaller/misc st-v1.2.0 things, e.g., increased reboot delay
  - nisse: reviewed change related to decryption of os-packages
  - nisse+rgdd: stprov should write additional info, description of os-pkg

## Decisions

  - None

## Next steps

  - rgdd, ln5: more iterations on the roadmap
  - rgdd: if time permits, sb in stprov
  - ln5: see review request from nisse on spec update
  - nisse: Complete stboot release
  - nisse: Issue on stprov populating description field
  - elias: suggest specific hardware to buy for our own ST testing/usage

## Other

  - rgdd: something that wasn't discussed in decrypt proposal?
    - before: no secrets in trust policy
    - after: user can put a secret (decrypt key) in trust policy
    - to be compared with: user/password in OS package URL
    - might matter in an RA scenario where stboot image is measured (and not secret)
    - fix would be to not have the decrypt key in the trust policy
    - or is rgdd missing something obvious / was this already discussed?
    - at the moment, okay to have it as is, but think about this going forward
    - when remote attestation (RA) is used, this can otherwise become a problem
    - nisse: seems evident that maybe it is in the wrong place? Should we fix it immeaditely or is it okay to fix it later?
    - we've already accepted proposal and MRs
    - let's poke odd and see?
    - if it's a lot of work for them -> live it for now
    - fix would be: have the key in efi nvram
    - ln5 think it would be worthwile to get it cleaner
    - rgdd thinks it might not be worht the time to fix this now, but would be good to check with erik/odd if they're still happy with things as is despite potential limitation
    - the decryption feature is optional, does that matter?
    - ln5 thinks this is about how much the maintenance burden would be in the future
      - rgdd: low
    - AP: sync with erik/odd
    - rgdd: reservation, would like to not block st-v1.2.0; so if they want to make changes = decrypt would probably have to land in the next release.
    - rgdd will send an email to odd/erik, cc ST list (then others can do follow-up responses on not just me). And then we see what they think.
