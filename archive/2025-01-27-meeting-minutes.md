# System Transparency weekly

  - Date:  2025-01-27 1200-1250 UTC
  - Meet:  https://meet.glasklar.is/ST-weekly
  - Chair: rgdd
  - Secretary: elias

## Agenda

  - Hello
  - Status rounds
  - Decisions
  - Next steps
  - Other

## Hello

  - elias
  - rgdd

## Status rounds

  - elias: discussed with ln5 regarding specific hardware to buy for our own ST testing/usage
    - nisse will need a machine for upcoming work
  - rgdd: erik's preference is to keep the decrypt proposal as accepted so let's leave it at that then (as discussed last weekly in the other section)
    - https://lists.system-transparency.org/mailman3/hyperkitty/list/st-discuss@lists.system-transparency.org/thread/JS7F7RBXLK24HC2S5AG42B7MVG26QGYH/
  - rgdd: suggestion for updated roadmap is being circulated, thanks for the feedback i got so far. All comments/questions/etc are much appreaciated!
    - https://git.glasklar.is/system-transparency/project/documentation/-/merge_requests/68
    - goal is to decide on the updated roadmap on our next weekly

## Decisions

  - None

## Next steps

  - elias: with ln5, plan ST deployment for Glasklar services
  - elias: decide what hardware to buy for our own ST testing/usage
    - when order is done, link to it in next meeting minutes in case others are interested in example hardware
  - elias: stmgr version info - https://git.glasklar.is/system-transparency/core/stmgr/-/issues/63
  - rgdd: ensure we're reaching a state where we decide on the roadmap next week
  - rgdd: maybe help nisse with progress on component releases (if he asks), i suspect i won't be doing that much ST progress this week (because we're going to fosdem and i think there's some lower-hanging fruit things we can do in sigsum before then)

## Other

  - Q/A about roadmap draft:
     - what monitor does is similar to what age-release-verify is doing (just PCR related instead of a Go binary)
     - MVP should have all ST parts, including remote attestation
     - stvmm not included in this MVP, possibly in a MVPv2 later
     - sounds good to elias to put these concepts together in an MVP!

