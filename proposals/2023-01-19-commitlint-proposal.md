# Proposal: Linting Commit Message

Integrate [commitlint](https://github.com/conventional-changelog/commitlint) to achieve [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/)

## Why Use Conventional Commits?
- Automatically generating CHANGELOGs.
- Automatically determining a semantic version bump (based on the types of commits landed).
- Communicating the nature of changes to teammates, the public, and other stakeholders.
- Triggering build and publish processes.
- Making it easier for people to contribute to your projects, by allowing them to explore a more structured commit history.

While you could use commitlint to test against any pattern, the configuration set here is https://github.com/conventional-changelog/commitlint/tree/master/%40commitlint/config-conventional with small adaptions.

## Conventional Commits

commit messages look like: 
```
<type>(<optional scope>): <description>

[optional body]

[optional footer(s)]
```

 _type_ is forced to be one of the following (can be edited in an own config): 
- **build** tooling, taskfile, etc
- **chore** housekeeping, dependency management, go.mod etc.
- **ci** continuous integration, workflows, etc.
- **docs** Readme, doc comments
- **feat** Source code changes introducing new functionality 
- **fix** Bug fixes, no new functionality
- **refactor** Source code changes without changing behavior
- **revert** Revert a commit
- **test** Add tests, increase coverage, which where not committed initially with a fix or feat commit 

_scope_ is optional to give more context. For context I suggest naming the package (not files!). Thant makes total sense with feat, fix, refactor and test and could look like
`fix(host/network): pass param by reference`

## Local linting minimizes overhead before MR

If you want to enable commitlint locally, check out https://commitlint.js.org/#/guides-local-setup
NOTE: You can skip the husky part and just use github hooks out of the box: 
Rename `.git/hooks/commit-msg.sample` into `.git/hooks/commit-msg` and put in
```
#!/bin/sh
npx commitlint --edit
```
This change will not be populated to the remote repo.
If you want to bypass commitlint locally you can do so by `git commit --no-verify -m"commitlint won't like"`

## Concrete proposal

Try commitlint in the `stprov` repository until the next release.  Then
re-evaluate if we want to continue, abort, or expand to other repositories.

The scope's we will use are the ones defined above (build, chore, etc).  To
enforce this, we will be running commitlint as part of the stprov CI/CD.
