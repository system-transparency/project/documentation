# System Transparency weekly

- Date: 2024-06-17 1200-1250 UTC
- Meet: https://meet.sigsum.org/ST-weekly
- Chair: kai
- Secretary: none

## Agenda

- Hello
- Status rounds
- Decisions
- Next steps
- Other

## Hello

- rgdd (async)
- nisse (mostly async)
- kai
- jens
- zaolin

## Status rounds

- rgdd: mainly review work and rubberducking, not much else to report today.
  Added a next step "???" that probably falls on me, nisse, or a combination
  thereof. I'm +1 for the queued proposal on deleting the twitter account.
- nisse: Progress on provision-on-failure
  - https://git.glasklar.is/system-transparency/core/stboot/-/merge_requests/212
- nisse: Supports twitter removal.
- kai: finished the remote attestation prototype and fixed the background on
  docs.s-t.org
- jens: updated the qemu-x86 coreboot tutorial to st-v1.0, will be online by end
  of the day
- zaolin: wrote guide documentation, worked only two days.

## Decisions

- Decision: Delete twitter account
  - https://git.glasklar.is/system-transparency/project/documentation/-/merge_requests/60
  - (prev. deferred according to
    https://git.glasklar.is/system-transparency/project/documentation/-/blob/main/archive/2024-05-20-meeting-minutes.md)

## Next steps

- ???: docs for the forceful entering of provision mode
- rgdd: releng with nisse for the next stable ST release (backlogged from last
  week)
- zaolin: finish remaining documentation topics
- jens: update the riscv coreboot tutorial to st-v1.0

## Other

- None
