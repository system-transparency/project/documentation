# System Transparency weekly

  - Date:  2024-03-25 1200-1250 UTC
  - Meet:  https://meet.sigsum.org/ST-weekly
  - Chair: nisse
  - Secretary: kfreds

## Agenda

  - Hello
  - Status rounds
  - Decisions
  - Next steps
  - Other

## Hello

  - <add participant here>
  - rgdd
  - kai
  - kfreds
  - ln5
  - nisse
  - jens (1h late)

## Status rounds

  - <add status round here>
  - rgdd: we now have a place for ST users to contribute host configurations, which will then be checked in our CI. If you are a user, please consider contributing representative host configurations that you care about.  It helps us understand what is in use, and it also helps us with regression testing (should we inadvertently make breaking changes).
    - https://git.glasklar.is/system-transparency/core/stmgr/-/tree/main/tests/hostconfigs?ref_type=heads
    - (vistor contributed three configurations, thanks!)
  - rgdd: stprov ux now accepts multiple dns and ospkg urls, also fixed a buggy ipv6 default (/32 rather than /128) for a previously undocumented shorthand in the -a option.  There are no more pre-release code changes planned to stprov.  Additional testing by users would be appreaciated.
    - Intermediate tag: v0.3.2
    - Drafty NEWS file: https://git.glasklar.is/system-transparency/core/stprov/-/merge_requests/55
    - No more stprov pre-release code changes planned.
  - rgdd: drafty stprov manual, to be accompanied by brief system documentation. Both of which will get rendered as reference documentation at docs.sto.
    - https://git.glasklar.is/system-transparency/core/stprov/-/merge_requests/57
  - rgdd: drafty flattening of docs.sto/reference section.  Includes freshning up the overview figure and a bit of text accompanying it.
    - https://git.glasklar.is/system-transparency/project/docs/-/blob/main/content/docs/reference/_index.md?ref_type=heads
  - kai:
    - implemented the enrollment phase of the remote attestation protocol for the most part.
    - tested the TPM code on multiple servers with real TPMs, added some workarounds. Also (I think) I removed the EINTR bug cause from our implementation
    - implemented event log handling for the legacy format
  - nisse: In review: fix stboot qemu-boot-from-net, add ssh-agent support to stmgr. Update u-root dep?
  - ln5: Changes to stimages, documentation feedback, Debian Snapshot
  - jens: Solved kexec problem on riscv boards: By applying the right patch from Max Brune (not in upstream coreboot yet) it is possible to:
      - start coreboot
      - jump into Linuxboot payload (RISCV Linux + u-root userland)
      - call kexec from within u-root to boot Ubuntu (from SD card)
- jens: tryed to replace u-root initramfs with ST-initramfs: not working (kernel panic), since I know u-root userland is working, try to build stboot into u-root inird again to see how far it boots. This is WIP and suprisingy difficult as both ST and u-root changed since we decoupled st from u-root.

## Decisions

  - <add decision here>

## Next steps

  - <add next step here>
  - rgdd: take a look at MRs nisse requested review on
  - rgdd: documentation and maybe release engineering
  - kai: still working on the event log handling and precomputing firmware measurements.
  - kai: I still need to up the test coverage of the TPM code too.
    - At the moment we're only testing the happy path.
  - kai: I need to look into EK certificate verification and what makes sense for us.
  - There's some guidance from TCG but I'm not sure what makes sense.
  - ln5: Signing artefacts, "quickstart" documentation updates, make sure that we do have the artifacts that we need for the quickstart Figure out what we're going to release in binary format.
  - nisse: Try to tie loose ends stboot and stmgr, hw tests (network boot, bonding), NEWS.
    - Have been looking into updating the u-root dependency. A somewhat large-ish update.
    - rgdd: Could you put together a drafty manual for this?
      - st-manager's integration with ssh-agent (the use of this for OS packages and certificates)
    - jens: try to get stboot run as uinit in u-root on unleashed board, as u-root is running, but ST initramfs is not.

## Other

  - <add any other business here>
  - rgdd: would like to briefly talk gitlab runners with nisse and ln5
    - ln5 will think about what can be done -- tag long-runner jobs? If it takes <30m will get it done, otherwise defer until after releases
    - short solution: cancel things you don't want to run
    - long term: we are also going to get more runners
