# Proposal to add a description field to the host configuration

## Background

Host configurations of machines provisioned using stprov before v0.3.5
include a provisioning timestamp. This has proved useful, e.g., when
collecting stats for a fleet of machines provisioned over a longer
time. Since there's no immediate purpose for such a timestamp in
stboot, it is not part of the host configuration specification and
current versions of stboot ignore it.

Feature request [1] asks for stboot to print a provisioning
timestamp and the version of the provisioning software.

[1]: https://git.glasklar.is/system-transparency/core/stboot/-/issues/204

## Proposal

Add an optional string field named `description` to the
[host configuration](https://docs.system-transparency.org/st-1.1.0/docs/reference/host_configuration/)
specification.

Make stboot log the string to the console when it is present and
non-empty. Before writing the description string to the boot console,
stboot removes all non-printable characters (isprint()).

## Field structure

As far as the host configuration and stboot are concerned, the description is
an arbitrary unstructured string. It is expected that an
organization's provisioning tools (e.g., stprov), and fleet management
tools, will agree on a structured format, including items such as
provisioning timestamp and provisioning software version. The details
are out of scope for this proposal, and can evolve without changes to
the stboot software or the host configuration specification.

When using this feature, one should keep in mind that the string
should be human readable, reasonably short, and helpful when reading
stboot logs. Additional information that doesn't fit easily in a short
human readable string should not be added to the description field, but
instead be stored elsewhere, e.g., in a separate EFI variable.

## Security considerations

Sensitive information must not be stored in the description string
since it is being displayed on the console at boot.

One attack scenario is an attacker that controls the host config
(e.g., after complete compromise of the machine), and tries to
leverage this to attack an operator that views the boot log. Including
raw ASCII control characters and terminal control sequences in the log
outout, as well as unicode bidirectionality controls, is dangerous in
this setting.

## Appendix

For concreteness, here is a possible format for stprov (details to
be sorted out in a separate proposal): Use semicolon-separated fields,
with each field starting with a keyword followed by a space character.
E.g.,

`stprov version v0.5.1; timestamp 2024-12-19T13:06:35Z`
