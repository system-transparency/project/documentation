# System Transparency weekly

    - Date:  2024-05-13 1200-1250 UTC
    - Meet:  https://meet.sigsum.org/ST-weekly
    - Chair: ln5
    - Secretary: nisse

## Agenda

    - Hello
    - Status rounds
    - Decisions
    - Next steps
    - Other

## Hello

    - rgdd
    - zaolin
    - kai
    - jens
    - nisse
    - kfreds
    - ln5
    - quite

## Status rounds

    - rgdd: released docs component
      - https://git.glasklar.is/system-transparency/project/docs/-/blob/v0.2.5/NEWS#L1-52
      - https://docs.system-transparency.org/st-1.0.0/
      - It would be really nice with improvement suggestions (issues or MRs), or just comments where you see fit on what you think is working and what isn't.
      - For those that like editing in the GL UI, try the "Edit this page" button!
    - rgdd, ln5, nisse: released the st-1.0.0 collection
      - https://lists.system-transparency.org/mailman3/hyperkitty/list/st-announce@lists.system-transparency.org/thread/XAFMM554TUTDAWCCLWFPGTHTJTSWNLCI/
    - rgdd: I wrote a few words about our release stuff yesterday on my blog, if anyone is interested see:
        - https://www.rgdd.se/post/we-released-a-thing-st-1.0.0/
    - rgdd: attempt to finalize the roadmap we discussed a few weeks back, see below.  Also looked at quite's proposal.
    - zaolin: Finished setting up minimal stboot distro, meta package, packagegroups and custom image. Will get the demo done for May meetup. @rgdd the docs.sto dark mode is broken because of missing css.
    - kai: I finished to PCR precalculation and TPM Quote generation code. Precalc works with QEMU at least.
    - kai: i use the stimages project to do my qemu tests and it worked great (for the most part).
        - i'm interested in what does not work :) /ln5
    - jens: Had a look at releases, plan coreboot support.
    - quite: a friend of ours, stemid, will start working at iPVN doing ST things
    - rgdd: joining for a meetup sometime in the future?
    - nisse: release stuff
    - kfreds: feedback to zaolin

## Decisions

    - Decided: Adopt ST project roadmap
      - https://git.glasklar.is/system-transparency/project/documentation/-/merge_requests/57
    - Decided: Support relative os_pkg_url
      - https://git.glasklar.is/system-transparency/project/documentation/-/merge_requests/59

## Next steps

    - rgdd: allocate ~2h for filing issues and/or write up some notes on TODO things we possibly let go of during the past weeks to cut st-1.0.0, to not lose track of them.
      - (it would be nice if ln5 and nisse also did this)
    - rgdd: would like to make the release checklists as consistent as possible across stprov, stboot, stmgr, and docs. I think stprov is good now with post-release changes.
        - Should be relatively quick, but would like to sync with nisse before any doing
    - rgdd: fyi, not here next week due to attending a conference
    - zaolin: 
        - Minimized systemd, kmod, udev build. Integration of the remaining stboot components and final image testing in qemu.
        - Fixing dark mode for docs.sto.
    - kai: write test for the Quote validation code and test against real machines.
    - nisse: Implement/document relative os_pkg_url by above proposal.
    - ln5:
        - will be filing issues according to rgdd's suggestion
        - away on conference next week, back just in time for the meetup
    - jens: vacating from May 16 to May 26. Prepare plans on coreboot support for meetup, chat with Fredrik (ideally today or tomorrow)


## Other

    - kfreds: Regarding "what things from the plumbing team will be ready for evaluation and possibly integration, and when?
        - coreboot support: qemu, "unmatched" board.
        - Secure Boot: It's done, because there are also external tools. How it's integrated is up to the core team. There is enough tooling and libraries to go that route.
        - Linux kernel module signing: It's complete. I solved it. It's documented. I'm ready and can support.
        - TPM remote attestation: hopefully done by meetup. Need tests + polish. Have to store blobs for each stage of boot, platform, stboot, ospackage, etc...
        - TPM credential protection: Not started.
        - stprov systemd approach: POC from a previous meetup. Conclusion: seems nice to use networkd.
        - stboot systemd approach: Should be ready for meetup 3.8MB initramfs + stboot binary. Yocto useful to build custom image. networkd is the most useful part of the systemd world.

    - kfreds: Breakout session afterwards with Jens, Philipp and Kai?
