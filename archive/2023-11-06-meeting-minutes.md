# System Transparency weekly

- Date: 2023-11-06 1200-1250 UTC
- Meet: https://meet.sigsum.org/ST-weekly
- Chair: Fredrik
- Secretary: rgdd

## Agenda

- Hello
- Status rounds
- Decisions
- Next steps
- Other

## Hello

- rgdd
- nisse
- kfreds
- jens
- quite
- ln5
- zaolin
- kai

## Status rounds

- rgdd: release proposal with input from most of you (see below)
- rgdd: attempt to get us self-organized around the 'WHAT' we identified last
  in-person meet-up, again with input from most of you (see below)
- rgdd: some feedback/review stuff, like going over current state of docs.sto
- quite: successfully stbooted ospkg on UEFI w custom Secure Boot keys; have
  patch for stmgr to sign UKI for Secure Boot.
- ln5: talked to one of our server hw upstreams about what their other customers
  might need
- zaolin: milestone planning, new restructuring of documentation, preparations
  of new docs repository (will move once all open MRs are merged), research
  option-rom stuff for Fredrik
- nisse: Back from vacation, catching up with what's happened the last week.
  Some trouble getting qemu to do what i'd like, any input appreaciated. See
  matrix!
- jens: working with rgdd regarding roadmapping and releases, review, planning ,
  documentation
- kai: SREcon proposal fixed, and working on stauth data channel. Works, code is
  a bit rough. ALso built some kind of ST test repository where all the
  different projects can be checked out and compiled, a bit like what current
  core/st repo does.
  - https://git.glasklar.is/system-transparency/project/documentation/-/blob/main/archive/2023-11-06-srecon-submission-notes.txt
- kfreds: Checking back in. Gone last week (worked in Gbg) - got great feedback
  from Richard and Nemo. My impression from the GK meetup (decisions on big
  things). Is anyone blocked? My focus until next meetup?
  - netboot chat with richard and nemo; secure boot and tpms etc.
  - impression from sigsum/st meetup: we decided on many big things. Is anyone
    blocked on me (fredrik) right now, or is it a straight line what we need to
    do? In theory could i (fredrik) focus on other things until next meet-up?
  - defer to other section

## Decisions

- Decision: Adopt proposal on how we get started/continued with releases
  - https://git.glasklar.is/system-transparency/project/documentation/-/merge_requests/44
- Decision: Adopt ST roadmap, re-evaluate if it is helpful before renewal
  - https://git.glasklar.is/system-transparency/project/documentation/-/blob/main/archive/2023-11-06-roadmap.md
- Decision: Cancel weekly meet on 2023-12-25 due to holidays
- Decision: Cancel weekly meet on 2024-01-01 due to holidays

## Next steps

- rgdd: update calendar with the canceled meets
- rgdd: release stprov based on the procedure in the above proposal, help jens
  and philipp with their releases on a need-basis (at least review)
- rgdd: try to ensure the mailing list and www.sto things move forward
  - would like help from ln5
  - if anyone has more opinions on mailing lists that we should or shouldn't
    have please comment here:
    - https://git.glasklar.is/glasklar/services/lists/-/issues/9
    - TL;DR: current idea is probably "announce", "dev", "theory"
    - (Maybe we other-section this depending on interest?)
- quite: cont w securebooted stboot w custom keys, and start thinking about OS
  packages. And would like to upstream the stmgr patch, removing "Draft: " from
  MR.
  - Q: any ready tooling for picking up EFI vars? A: read files from /sys, drop
    the first 4 bytes.
  - Linus has been doing OS packages for a long time, have a couple of ideas
    that could be valuable. Talk to Linus once you've started to think about it
    if you'd like.
- ln5: provision three physical machines -- two routers and one KVM host
  - (quite's secureboot work very timely, thank you!)
  - os package thinking and tooling also on my map to run the above
- zaolin: finish new docs repository, restructuring of documentation and
  option-rom research (small writeup)
  - rgdd: would really like to have released docs.sto before wed next week
- nisse: Wrap up in-progress MRs, then organize my new milestones a bit more.
- kai: take care of my code from last week, then upstream once it has tests;
  address comments from review by jens to get MRs merged. Will be away day after
  tomorrow until 23rd nov.
- jens: lots of things on GL todo list, both where assigned and things i should
  drive. Will fix the doc things that are missing from me, to not block docs.sto
  release. And other than that: stmgr and stboot releases.
- kfreds:
  - Get up to date on what's been happening since the Glasklar meetup.
  - Synthesize and communicate insights from recent meetings in Göteborg.
  - Produce ... content?

## Other

- jens: short revisit on competitor intelligence task
  - https://git.glasklar.is/groups/system-transparency/-/milestones/10#tab-issues
  - fredriks take on what was handed out as concrete TODOs: see meet-up notes
    (netbooting stboot, not the ospkg. clarification. I.e., how does uefi get
    stboot.)
  - stprov/stboot: was about taking a look around (context uki), basically what
    are other people using to solve things that are pretty close to our
    artifacts and their essences. Anything we can use / improve? Anything we
    should keep an eye on? Etc.
  - imo: skip sbom,stvmm,stauth for now - simply because one of the insights
    from meet-up is we should make a release of robust netbooting stuff first.
- kfreds: If I recall correctly
  - stboot alternatives:
    [Enumerate possible UEFI bootup methods](https://git.glasklar.is/system-transparency/project/documentation/-/issues/24)
- kfreds on st/sigsum meet-up
  - impression was that we got a lot of stuff done and figured out concrete
    roadmap. Q: is anyone blocked, or doesn't have a clear idea of what to do or
    prioritize? Do you anticipate being blocked on fredrik the next ~2 weeks?
    - "no"
  - Q: does anyone feel like we don't have a clear plan until the meetup?
    - "no"
  - Q: does anyone anticipate being blocked on fredrik until the next meet-up
    mid december?
    - "no"
