# ST weekly

  - Date: 2023-01-09 1330-1420 CET
  - Meet: https://meet.sigsum.org/ST-weekly
  - Pad:  https://pad.sigsum.org/p/ST-230109

## Agenda

  - Hello
  - Status round and next steps
  - Decisions
  - Other

## Hello

  - Fredrik
  - Foxboron
  - Linus
  - Jens

## Status round and next steps

### Foxboron

- Some stprov, some stmanager.
- Next up: Meeting with Victor; Getting the stmanager changes merged.

### Jens

- Vacation previous week
- Next up: Gitlab workflow planning with rgdd

### kfreds

- First workday of the year.

### ln5

- First day.
- Next up: Outstanding issue plus GPG signing; Meeting Victor.

## Decisions

- No decisions.

## Other

### Support password/key protecting downloads from the provisioning server or not?

In the stboot configuration there is a username and password, used by
stboot when fetching an OS package. Do they have to be the same on all
ST-booted systems? How can we make that easier to revoke?

Decision: Yes, ST must support protecting os-pkgs. For two reasons: 1)
The word "transparency" in ST doesn't mean "transparency towards the
entire universe" and 2) good for on-ramping onto ST.

### Work planning

Suggestion: Postponing stprov work and instead focus on stboot until
a later date.

Q: When are we picking up stprov again?

A: We should aim for releasing a first version of stprov by mid March

#### Workdays

- Morten workdays: Mon-Tue Sigsum; Wed-Thu ST
- Linus workdays: Mon-Thu
- Jens workdays: Tue-Wed ST; TBD: perhaps some review Mon or Fri as well
