# Backwards-compatible `"network_interfaces"` in host configuration

## Background

### Current configuration syntax

The host configuration includes a key `"network_interfaces"`, which is
an array of objects with two keys `"interface_name"` and `"mac_address"`,
e.g.,
```
"network_interfaces": [
  {"interface_name":"eth0","mac_address":"aa:bb:cc:ee:dd:ff"},
  {"interface_name":"eth1","mac_address":"11:22:33:44:55:66"}]
```
In bonding mode, only the interface names are used by stboot, while in
non-bonding mode, only the mac adresses are used by stboot. This is
somewhat confusing, but changes to that is out of scope for this
proposal, which aims for no behavioral changes.

### Old configuration syntax

Prior to the [new field][] change, 2022-10-19, there was a different
key, `"network_interface"`, the value being a single MAC address. E.g.,
```
"network_interface": "aa:bb..."
```

### Intermediate configuration syntax

The [new field][] change, 2022-10-19, introduced the
"network_interfaces" key, but with a different syntax than the current
configuration which was implemented in the [dropped][] change,
2023-02-09.

Non-bonding configurations kept using `"network_interface"` with a MAC
address, as before. The key `"network_interfaces"` was introduced, in
parallel, with value being an string array, representing interface
names. E.g.,
```
"network_interfaces": ["eth0", "eth1"]
```
This key was used only for bonding configurations.

### The problem

Any platform provisioned with a host configuration that does not set the
`"network_interfaces"` field needs to be re-provisioned to upgrade
`stboot`. Configurations without this key, would get the default behavior (i.e., guess an
interface), which would be unexpected if `"network_interface"` is being set but ignored.

Any platform provisioned with a host configuration using the intermediate syntax
for the `"network_interfaces"` field also need to be re-provisioned to upgrade
`stboot`, or booting will fail because the host config is invalid.

[new field]: https://git.glasklar.is/system-transparency/core/stboot/-/commit/334d268333f0ed7a4bda02cff561c07c7ed3c269
[dropped]: https://git.glasklar.is/system-transparency/core/stboot/-/commit/c3ccf5555caa09578100b0a6f3c71b28f74e1da0
[stboot source code]: https://git.glasklar.is/system-transparency/core/stboot/-/blob/bd1dce57d59fc6916bbab1412e5c4e4ee3ccbc8a/host/network/network.go#L91-119

## Proposal

Make stboot recognize the old variants, and handle them as if the
equivalent configuration had been expressed in the current syntax.

If the configuration contains a `"network_interfaces"` key where the
value is non-null, and conforms to the current syntax (array of
objects), that is used as is. The backwards compatibility logic is not
applied.

The proposal is structured in two parts: Translation of old syntax to new syntax
(or rather, a somwhat more liberal syntax, allowing null fields), and the logic
that determines precedence between the related keys and detects conflicts with
the setting of the bonding mode.

### Translation

If the configuration contains a `"network_interfaces"` key where the
value is an array of strings, e.g.,
```
"network_interfaces": ["eth0", "eth1"]
```
translate that to
```
"network_interfaces": [
  {"interface_name": "eth0", "mac_address": null},
  {"interface_name": "eth1", "mac_address": null}]
```

If the configuration contains a `"network_interface"` key, e.g,
```
"network_interface": "aa:bb..."
```
translate that to
```
"network_interfaces": [
  {"mac_address":"aa:bb...", "interface_name":null}]
```

### Precendence and error handling

If both keys `"network_interface"`, `"network_interfaces"` are present
and non-null, then the deprecated `"network_interface"` key is silently
ignored. Note that this includes (invalid) configurations where
`"network_interfaces"` is set to the non-null but empty value,
```
"network_interfaces": []
```

If the translation from the old `network_interface"` syntax is applied to a
configuration with bonding enabled, the resulting configuration is invalid and
rejected. Bonding requires non-null values for `"interface_name"`.

If the translation from the intermediate `"network_interfaces"` syntax is
applied to a configuration with no bonding enabled, the resulting configuration
is invalid and rejected. Then non-bonding network setup in stboot requires
non-null values for `"mac_address"`.

These rules are intended to preserve the behavior of older versions of stboot,
for all previously valid configurations, with one exception: Consider a
configuration using the intermediate configuration syntax, where both
`"network_interface"` and `"network_interfaces"` are present, and bonding is
disabled. In this case the old behavior, for stboot versions using this syntax,
was to use the MAC address specified using the `"network_interface"` key. Under
this proposal, that key is be ignored, and the resulting configuration, without
any MAC address, is rejected as invalid.

As far as we are aware, there are no existing configurations in the wild of this
unsupported type; all configurations where both keys are present also enable
bonding.

## Motivation

The proposed change would make the transition to `"network_interfaces"`
backwards compatible for existing configurations. This avoids re-provisioning
hundreds of servers.

## Required changes

This proposal has more than modest complexity, but it is still manageable.  A
drafty implementation from an earlier revision of this proposal is
[available][].  The final proposal has comparable implementation complexity.

Note that the required changes can be fully contained within the parsing of a
host configuration.  No parts of the existing networking setup needs to change.

[available]: https://git.glasklar.is/system-transparency/core/stboot/-/merge_requests/149/diffs?commit_id=962539b73b5e23d8e46418a774782838d8e2e044

## Downsides

This makes it harder to do clean-up in the networking code, such as always
doing network configuration based on MAC addresses rather than interface names.

## How to document this

The stboot reference documentation should list which host configuration version
is implemented.  This section is amended to specify what parts of the parsing is
relaxed for the purpose of backwards-compatibility.  The relaxed parsing will be
removed eventually, but only when other interfaces must be broken anyway as
well.  This reduces the number of times that breakage is forced on users.

Note: none of this affects writers of host configurations, such as `stprov`.

## Related notes

  - [Analysis of real-world host configurations that break](https://git.glasklar.is/rgdd/hostcfg)
