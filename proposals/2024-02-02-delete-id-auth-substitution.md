# Proposal to delete the $ID/$AUTH substitutions

## Motivation

Currently, the url or filename used to fetch the OS package is
constructed from three different fields in the host config:
`ospkg_pointer`, `identity`and `authentication`. Before using the
`ospkg_pointer`, the fixed strings "$ID" and "$AUTH" are replaced by
corresponding values from the host config. Current spec:
https://git.glasklar.is/system-transparency/project/docs/-/blob/97dca8e74ff787e5b55174e20cb49067220834c6/content/docs/reference/data-structures/host_configuration.md

This substitition adds to complexity of both code and documentation,
and it is not necessary. The provisioning tool that writes the
`ospkg_pointer` can use any template machinery of its own. E.g., the
stprov tool has its own templating, and unlike the substitutions done
by stboot, this stprov feature is used in production. Only the final
result is needed in the host config, for use by stboot.

Furthermore, as far as we are aware, there's no current or historic
use of this feature in stboot.

There may be future usecases with substitution of values stored in
some different security domain then the host config itself. Or related
usecases, e.g., specifying credentials for HTTP-level authentication.
Deleting the current substitutions based on other fields in the host
config does not rule out designing new substitution or authentication
features if/when there is a concrete usecase for that.

## Proposed changes

### stboot program

Delete this substitution from stboot. Like other obsolete fields,
stboot will not raise any error if these fields occur in the host
config, they will just be ignored.

### host config specification

Delete the `identity` and `auth` fields from the host config
specification.

### stprov program

The stprov program used to populate these host config fields with
dummy "foo" / "bar" values, but it no longer sets these values at all,
so no changes needed for this proposal.
