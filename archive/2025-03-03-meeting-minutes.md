# System Transparency weekly

  - Date:  2025-03-03 1200-1250 UTC
  - Meet:  https://meet.glasklar.is/ST-weekly
  - Chair: rgdd
  - Secretary: elias

## Agenda

  - Hello
  - Status rounds
  - Decisions
  - Next steps
  - Other

## Hello

  - elias
  - rgdd
  - nisse
  - ln5

## Status rounds

  - elias: saved notes about initramfs uncompression error
    - https://git.glasklar.is/system-transparency/project/documentation/-/blob/main/archive/2025-02-25-initramfs-uncompression-error.md
  - elias: Closed issue "fetching OS package from initramfs failed: open /ospkg/debian-package.json: no such file or directory"
    - not solved, but the problem is likely not in stboot itself
    - https://git.glasklar.is/system-transparency/core/stboot/-/issues/224
  - elias: stmgr fix from quite "Ignore -rootCert for real if also -isCA; ensure warning is shown" has been merged
    - https://git.glasklar.is/system-transparency/core/stmgr/-/merge_requests/102
  - elias and ln5: planning initial ST deploy for Glasklar servers
    - something public about this planning will become available
  - nisse: did anyone look at stprov release testing?
    - no
  - rgdd: reading and thinking about Secure Boot (SB); and taking notes.
    - have some notes, will add in archive soon
    - looked at formats used, what things need to be initialized. etc.
    - focused on understanding the different parts that are needed, which tools exist, kernel modules, etc.
    - may or may not end up using sbctl tool, not decided yet
    - good progress, want to discuss more on Thursday

## Decisions

  - None

## Next steps

  - nisse: stprov release testing; then stmgr and finally st-v1.2.0
  - ln5: publish ST deploy plan
  - rgdd: wrap up reading and thinking about sb, talk with you all; share notes
    - aim to have more concrete plan for SB after this week

## Other

  - elias: "stboot | Remote mirror update failed" email - gitlab<-->github mirroring
    - already broken since before, broken due to too large size of stboot repo
    - leave it, not worth the effort to try to solve it now
    - rgdd: there are already notes in archive about this
      - https://git.glasklar.is/system-transparency/project/documentation/-/blob/main/archive/2024-11-18-meeting-minutes.md
  - quick SB questions
    - generation of keys (for example "key exchange keys" (KEK)), should it be the same keypair for all servers, or separate keypair for each server?
      - nisse: per organisational unit? In Glasklar case, just one org. so one keypair.
    - Kai's prototype used one keypair per server, but we don't want that now
    - The servers that are configured using SB, they should never see or never need to touch the private keys? --> correct
    - When it comes to updates: one thing is to provision first time, but then if an update is to be done, should it be possible to go into some stprov update mode?
      - example: if you want to update the contents of a database
      - nisse: could it be achieved that those things cannot be updated from the os-package?
        - ln5: Supermicro seem to have different possible ways
        - rgdd: one example is that there are some keys you want to revoke
      - rgdd: one question is, do you need to create a new os-package in order to make a change?
      - should the keys be on hardware tokens?
      - rgdd: suppose you have signed something that you want to add to your system, should there be a special "update flow" that can be used, that is different from the initial provisioning flow?
      - ln5: it depends on if there are significant security issues with allowing it to be done from an existing already provisioned system
  - ln5: we are considering how much to do before the middle of May regarding ST deploy for Glasklar's own servers
    - one thing to decide is how provisioning is done: we want to provision a number of things that are os-package config that are similar to stboot config, for example network config. Question to rgdd: will you extend the protocol?
      - rgdd: yes, I plan to extend that so that 4 things can be sent:
        - PK: platform key
        - KEK: key exchange key
        - DB: allowed signature database, including pubkeys and hashes of allowed
        - DBX: like DB but the other way around, hashes that are to be rejected
        - The above works as a hierarchy: if an update is to be done of a KEK, then that update must be signed using the PK. If you want to update a DB then that must be signed by a KEK.
        - Want to send the initial state of this via the protocol that stprov is using.
        - In Secure Boot Setup it is possible to change the above keys and DB things, at boot time there is some way to enter that setup mode by pressing some F key or similar.
      - Possible tools that may be useful:
        - mokutil
        - osslsigncode
        - sbsigntools
        - efitools
      - The contents of the EFI variables are not very complicated
      - Painful parts may be "PE" (?) format
      - sbctl builds on a library for working with efi variables, it may be possible to use that library directly instead of using sbctl
  - ln5: extending the protocol that stprov is using seems like the right thing to do for us to achieve provisioning the way we want, so if you (rgdd) are going to extend that anyway then that is interesting for us
    - rgdd: right now there is only one or two endpoints, it is possible to add more endpoints there. May want to add 4 endpoints.
    - ln5: we discussed how things could be restricted on the server side, do not want to allow remote execution of anything
    - rgdd: the protocol can define a certain endpoint, then the client can choose to poke at that endpoint and then you can decide what to do on the server side
    - ln5: regarding ST deploy: there are advantages with doing many systems, to learn about issues that appear then, but there are also arguments why it may be more interesting to have different kinds of systems
      - ln5: for example we are considering if we should start with only physical machines or if we should do VMs as well already in a first phase
      - rgdd: one physical server and one VM seems like a good starting point
      - rgdd: after having done one physical server and one VM, then decide next step
      - nisse: milestones could be "one system", then "three systems", then "many"
      - rgdd: recommend to start with 1 physical server and 1 VM
