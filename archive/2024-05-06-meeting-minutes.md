# System Transparency weekly

- Date: 2024-05-06 1200-1250 UTC
- Meet: https://meet.sigsum.org/ST-weekly
- Chair: nisse
- Secretary: rgdd

## Agenda

- Hello
- Status rounds
- Decisions
- Next steps
- Other

## Hello

- rgdd
- philipp
- jens
- nisse
- ln5

## Status rounds

- rgdd: got my release tkey setup, verified keys with nisse and ln5
  - my local cheat-sheet notes for release signing/verifying below
  - https://git.glasklar.is/system-transparency/project/documentation/-/blob/main/archive/2024-05-03-rgdd-release-signing-notes.txt
- rgdd: wrapped up stprov v0.3.5 release
  - https://lists.system-transparency.org/mailman3/hyperkitty/list/st-announce@lists.system-transparency.org/thread/LJSFTHRJJ4BOTNA3HUVTAEZSTEPSZCHJ/
- rgdd: misc wip things on docs/, still trying to get things tied up for
  collection release
- nisse:
  - made stboot v0.3.6 release,
    https://lists.system-transparency.org/mailman3/hyperkitty/list/st-announce@lists.system-transparency.org/thread/332SWEPTXANDOSPIDTWXDTY4MCHTT2LD/
  - next, stmgr
  - thinking about NEWS files to go with the collection, we're thinking it
    should go in the core/system-transparency repo along side manifest.
- zaolin: finished initial proposal, got the size of the image down to 3mb.
  Working on yocto/reading documentation to setup custom distro/target. Can give
  an example of this at the meet up in may.
  - Q: how large is the kernel image and how you deal with the config? You can
    do that with yocto (then it makes it small,~2.5MB). Can also set up custom
    kernel config, but philipp thinks maximal minimum kernel is maybe something
    like 1.8MB; at the cost of missing a lot of features.
- ln5: QA stboot images, signing and publishing
- ln5: TKey for release signing set up
- jens: continued on coreboot support work, and looked a bit into the releases
  you've been working on.

## Decisions

- None

## Next steps

- nisse: stmgr today, today or tomorrow. And review various release docs.
- rgdd, ln5, nisse: tie up docs/ and collection release tue+wed
- zaolin: setup custom distro with minimal footprint
- ln5: more disk for GL builders
- jens: planning to update my docs based on your new build tutortial. Once have
  all info regarding coreboot support, will schedule a meeting with fredrik.
  - nisse wonders about how to get coreboot support tests into our qemu
    pipelines
  - rgdd: at least an intuition for what needs to be done to get it working in
    QEMU would be nice
  - jens: please look at the tutortial first, see:
    - https://git.glasklar.is/system-transparency/platform-plumbing/st-platform-features/-/tree/main/coreboot-support?ref_type=heads

## Other

- None
