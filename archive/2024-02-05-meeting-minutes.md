# System Transparency weekly

- Date: 2024-02-05 1200-1250 UTC
- Meet: https://meet.sigsum.org/ST-weekly
- Chair: rgdd
- Secretary: rgdd

## Agenda

- Hello
- Status rounds
- Decisions
- Next steps
- Other

## Hello

- ln5
- nisse
- rgdd

## Status rounds

- philipp: Finished the crypt container integration, demo preparation and talk
  (async report)
- rgdd: draft on backwards-compatible hostcfg
  - https://git.glasklar.is/system-transparency/project/documentation/-/merge_requests/51
  - https://git.glasklar.is/system-transparency/core/stboot/-/merge_requests/149
  - https://git.glasklar.is/rgdd/hostcfg
  - Would like to push for the proposal next week, asked for detailed review
    from nisse as step 1
- nisse: testing stboot from disk, wip. Have an MR for associated scripts and
  running in CI.
- ln5: progress on qa images, debian stable image that's built automatically
  when the repo is tagged. Not what we want (we want build when there are
  updates in packages) -- but a step in the right direction.
  - nisse: maybe just rebuild every day could be a reasonable first step? Or
    does it need to be smarted?
  - ln5: sounds like a good first step

## Decisions

- Decision? Delete $ID/$AUTH substitution, see
  https://git.glasklar.is/system-transparency/project/documentation/-/merge_requests/52
  - Defer until next week

## Next steps

- all: meetup
