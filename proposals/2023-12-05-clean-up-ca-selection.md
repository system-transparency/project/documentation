# Proposal on cleaning up the selection of TLS signing roots

## What are we doing today

`stboot` reads trusted TLS signing roots from the following file:
`etc/ssl/certs/isrgrootx1.pem`.

Despite the file's name indicating that the Let's Encrypt TLS signing root is
expected, no such correspondence is enforced.  In other words, it is possible to
place any TLS signing root in this file; even multiple TLS signing roots.

The certificates in this file are in X.509v3 format, then PEM encoded.  Multiple
PEM encoded certificates that are concatenated will form a list.

Relevant pointers:

  - https://git.glasklar.is/system-transparency/core/stboot/-/blob/main/stboot.go?ref_type=heads#L46
  - https://git.glasklar.is/system-transparency/core/stboot/-/blob/main/stboot.go?ref_type=heads#L169
  - https://git.glasklar.is/system-transparency/core/stboot/-/blob/main/stboot.go?ref_type=heads#L194
  - https://git.glasklar.is/system-transparency/core/stboot/-/blob/main/internal/certutil/certutil.go?ref_type=heads#L9-38
  - https://git.glasklar.is/system-transparency/core/stboot/-/blob/main/stboot.go?ref_type=heads#L237
  - https://git.glasklar.is/system-transparency/core/stboot/-/blob/main/host/network/download.go?ref_type=heads#L40-43

None of this is defined in our reference documentation.  The `isrgrootx1.pem`
file is mentioned in two places, one of which is an RA reference document:

    ~/src/git.glasklar.is/system-transparency/project/docs/content/docs$ git grep isrgrootx1.pem
    guides/howtos/stboot_image.md:17:-files contrib/initramfs-includes/isrgrootx1.pem:etc/ssl/certs/isrgrootx1.pem \
    reference/endorsev0.md:108:`/etc/ssl/certs/isrgrootx1.pem` respectively.

## Proposal

Start reading trusted TLS root certificates from the following file:
`etc/trust_policy/tls_roots.pem`.

The file's content is expected to contain at least one TLS certificate, which a
client can use to build a trusted certificate chain when connecting to a server.

The certificate is in X.509v3 format, then PEM encoded.  If the file contains
multiple certificates, they are concatenated which is common in PEM files.

## Motivation

The selection of TLS CAs is a trust decision.  For example, if a deployment
wants to keep OS packages and their user:password credentials private, it is
necessary to download on a secure channel.  Similarly, OS package downgrading
would currently be possible if an on-path attacker is able to intercept TLS.

This is why the TLS signing roots should be located in `etc/trust_policy`.

Selection of TLS root CAs is further not a one-fits-all kind of trust decision.
This makes it inappropriate to hard-code a filename that implies use of Let's
Encrypt.  For example, it is not unreasonable to use one's own TLS root CA.

This is why the file should be called `tls_roots.pem`.

The suggestion to keep all TLS roots in a single file (rather than requiring
that multiple `.pem` files are located in a single directory) is somewhat
arbitrary.  That said, the expected use is likely that one or a few trusted TLS
signing roots are specified.  So, a single file should not become very messy.

## Backwards compatibility

`stboot`: this is a breaking change, a file path must be updated.  Note that
tooling preparing the initramfs for `stboot` will also have to be updated.

`stauth`: this is a breaking change, a file path must be updated.

`stprov`: have yet to start using the same TLS signing roots as `stboot`.  We
will not introduce this (bug fix) until this proposal is accepted.
