# System Transparency weekly

  - Date:  2024-12-02 1200-1250 UTC
  - Meet:  https://meet.glasklar.is/ST-weekly
  - Chair: ln5
  - Secretary: elias

## Agenda

  - Hello
  - Status rounds
  - Decisions
  - Next steps
  - Other

## Hello

  - elias
  - ln5
  - nisse
  - zaolin
  - kai

## Status rounds

  - kai: stvmm is cleaned up and documented. It part of the st-platform-featutes repository. The README contains a quick overview of the architecture and an detailed tutorial on how to get it running. The necessary OS packages can be built with stimages.
    - not as clean as the RA stuff, but should be enough
    - part of same repo as everything else
  - kai: I did a quick check to make sure qemu, firecracker and cloud hypervisor can run simultaneously. The do.
     - a future version of stvmm could have a choice of hypervisor
     - are we reinventing libvirt? not necessarily true, but need to explain the reasons, that the goal is not the same thing as libvirt.
  - nisse: MR for logging IP addresses in stboot dl's of images, https://git.glasklar.is/system-transparency/core/stboot/-/merge_requests/234
      - the purpose is that users should be able to see what ip address you actually connected to (or tried to connect to)
  - ln5: gitlab runners, almost done
  - ln5: renovate will not happen now -- it takes more than 1h
      - still think it could be useful, but takes more time to find out exactly how to use it
  - zaolin: worked last week on documentation
     - another MR coming in shortly, possibly today
     - we will wait for rgdd to try things out

## Decisions

  - No decisions.

## Next steps

  - ln5: actually getting the runners up
  - ln5: review MR's to stimages
       - one MR is about mmdebstrap
       - tests are not in the stimages repo but in another repo

## Other

  - kai and zaolin are here maybe for the last time --> ask them about things?
  - kai and zaolin will still be available later to answer questions
  - zaolin is going to FOSDEM, likely nisse and elias also
  - OSFC conference, kai will be there
  - kai may have time to deploy a server running a sigsum witness
