# System Transparency weekly

    - Date:  2024-10-14 1200-1250 UTC
    - Meet:  https://meet.glasklar.is/ST-weekly
    - Chair: nisse
    - Secretary: nisse

## Agenda

    - Hello
    - Status rounds
    - Decisions
    - Next steps
    - Other

## Hello

    - nisse

## Status rounds

    - None

## Decisions

    - None

## Next steps

    - None

## Other

    - None
  
