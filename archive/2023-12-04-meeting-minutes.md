# System Transparency weekly

- Date: 2023-12-04 1200-1250 UTC
- Meet: https://meet.sigsum.org/ST-weekly
- Chair: zaolin
- Secretary: rgdd

## Agenda

- Hello
- Status rounds
- Decisions
- Next steps
- Other

## Hello

- rgdd
- ln5
- zaolin
- nisse
- kai
- quite
- jens

## Status rounds

- nisse:
  - Back from CATS, and able to do more ST work.
  - MR
    https://git.glasklar.is/system-transparency/core/stboot/-/merge_requests/110
    still in review. Would like to merge soon.
- zaolin:
  - take care of mkosi/uki, doing research on it to be able to move it forward.
    E.g., the signature stuff; talking about it with nisse. Built some drafty
    code while exploring, just to get the hang of things. Working on proposal,
    to be made available ~today.
- jens:
  - finalized Milestone "Finalize / test loading stboot from UEFI"
  - synced with zaolin on further steps for documentation
  - started discussion with kai about CI/CD pipeline tests in distribueted repos
  - worked on how-to guides
  - review stboot
  - preparing topics for the meetup
- kai:
  - got the integration tests with qemu running in the CI:
    https://git.glasklar.is/kai/system-transparency
  - we're still in dicussion on how to and where to integrate this
  - all my changes to stboot are now upstreamed.
  - stauth works including the device bound key. the provisioning phase still
    needs to be moved from stauth to stprov
  - "stauth done", wrt what features the current poc is meant to achieve
- ln5:
  - some docs.sto fixes
  - playing with building ospkgs (again)
- quite: mostly doing research of internal systems, config mgmt. Also found out
  that stprov doing its new HEAD test on ospkg HTTPS URL failed because missing
  CA root.
- rgdd: pass

## Decisions

- None

## Next steps

- rgdd: properly page in what needs to be done for the CA trust root stuff
  - https://git.glasklar.is/system-transparency/core/stprov/-/issues/32
- ln5: (generally available mon, tue, thu)
  - [Enumerate possible UEFI bootup methods](https://git.glasklar.is/system-transparency/project/documentation/-/issues/24)
  - if you need anything from me please let me know (e.g., deploymentwise or
    setting up test things). Don't be shy if anything's blocking you.
- jens:
  - prepare meetup (CI/CD)
  - looking at stboot MRs, chat about nisse's MR tomorrow with nisse and also
    talk about the vendor folder (which initially had to do with u-root stuff)
  - work on How-To guides
- kai:
  - prepare the TPM presentation for next week.
  - clean up the milestones
  - if I still got time, start with the integration of stauth into stprov.
- nisse: make some progress on stboot-as-init, look at and understand the tpm
  test things
- quite: looking at internal config stuff for deployment things
- zaolin: finish two presentations for next week, will mostly work on that and
  finish the proposal for today. Would like to talk with Linus in a breakout
  session.

## Other

- It appears go mod vendor (used in the stboot repo) is incompatible with the
  cgo hack in the github.com/google/go-tpm-tools/simulator/ recently added as a
  dependency. (See
  https://github.com/google/go-tpm-tools/blob/main/simulator/internal/include.c
  ...) Would be nice to reorganize the test, dealing with this test dependency
  in some other way.
