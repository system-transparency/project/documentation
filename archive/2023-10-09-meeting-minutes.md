# System Transparency weekly

- Date: 2023-10-09 1200-1250 UTC
- Meet: https://meet.sigsum.org/ST-weekly
- Chair: Linus
- Secretary: rgdd

## Agenda

- Hello
- Status rounds
- Decisions
- Next steps
- Other

## Hello

- rgdd
- nisse
- joel
- ln5

## Status rounds

- Philipp (async):
  - Finished documentation theme initial version
  - Secure Boot proposal is ready
- rgdd: mostly behind the scene things (planning/review/feedback), and add
  running of stprov unit tests (that within reason can run) in our ci
- nisse: Updated license/authors for stboot and stprov.
- ln5: identified issues that are both wanted and not XXX
  https://git.glasklar.is/groups/system-transparency/-/milestones/8
  - **Fantastic milestone if you want something to do that someone has asked
    for, please assign yourself if you've planning to take a stab at something**
- joel: nothing to report today, but eye:ing to take a look at the new docs.
  - (will let ln5 known if I can't resolve threads in gitlab.)

## Decisions

- None

## Next steps

- Philipp (async):
  - Integrate theme feedback
  - Write missing howtos and overview section
    - Roadmap
    - License
    - Components and concepts
- rgdd: work towards milestone 8.
- nisse: looking at stprov issue to HEAD the ospkg, have some questions. Would
  like to dig a bit more in my kernel/qemu trouble.
- joel: if time permits, look at some docs!

## Other

- stprov, questions
  - Context: HEAD request on the OS package?
  - Remote seems to have dhcp, static, and run. Commands.
  - Run: the magic talk to the server thing.
  - Can the head thing be done already at the static/dhcp command? If yes,
    that's where we would like to do it.
