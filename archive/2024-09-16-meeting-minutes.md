# System Transparency weekly

- Date: 2024-09-16 1200-1250 UTC
- Meet: https://meet.glasklar.is/ST-weekly
- Chair: rgdd
- Secretary: rgdd

## Agenda

- Hello
- Status rounds
- Decisions
- Next steps
- Other

## Hello

- rgdd
- nisse
- zaolin
- kai
- joel

## Status rounds

- Kai: finished review of stboot w/ systemd prototype, fixed some bugs, added a
  README. Started working on stvmm, it's booting a kernel + initrd in
  firecracker.
- Kai: basic ideas and api + a bunch of milestones (planning)
  - https://pad.sigsum.org/p/stvmm (don't persist pad)
- nisse: Deleted the task files, see
  https://git.glasklar.is/system-transparency/core/system-transparency/-/merge_requests/286
  and
  https://git.glasklar.is/system-transparency/core/system-transparency/-/issues/223.
  Also closed a couple of related issues.
- zaolin: finished reviewing remote attestation + added some additional
  documentation
- joel: sec-t talk on confidential computing
  - https://m.youtube.com/watch?v=vdj9Pr-6dq8

## Decisions

- None

## Next steps

- Kai: get networking set up for the firecracker VMs, add support for actual OS
  packages instead of unpacked kernel/initrd
  - "get stvmm running -- start vm etc; bare minimum that we can iterate on"
- zaolin: working now on the API stubs of the vmm daemon and getting into the
  topic.

## Other

- Is there any ETA on when to do decisions on novemember meetup date?
  - Date is decided, city is decided. Exact location not decided.
  - 4 nov - 6 nov. Gothenburg.
