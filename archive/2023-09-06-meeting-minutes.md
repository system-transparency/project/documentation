# System Transparency weekly

  - Date:  2023-09-06 1200-1250 UTC
  - Meet:  https://meet.sigsum.org/ST-weekly
  - Pad:   https://meet.sigsum.org/p/ST-20230906
  - Chair: Fredrik

## Agenda

  - Hello
  - Status rounds
  - Decisions
  - Next steps
  - Other

## Hello

  - ln5
  - rgdd
  - Kai
  - Fredrik
  - quite
  - philipp
  - nisse

## Status rounds

  - ln5: st meetup last week, nothing else to report.
  - Kai: worked on the stauth protocol references: !7, !8, !13 in the docs repo.
    Also submitted a proposal: !10. I still need to finish moving SMM and RA
    explainers from stauth to docs repo (!9). I also started brainstorming about
    the v1 of the remote attestation protocol.
    - Waiting for review, please pitch in
  - Philipp: catching up with last week's meetup documents.  Prepared two
    proposals.  Working on documentation, incorporating feedback from joel.
  - Fredrik: Taking over the responsibility of project lead of ST from Linus.
    Trying to figure out what is important and urgent re the ST project.
    - Why Linus/Fredrik switching roles?
    - TL;DR: Linus works a bit less now, part time.  And Fredrik has more time /
      fewer projects going on.  And we think it is more effective for Linus to
      focus on knowing the painpoints and infra of pilot users.
  - rgdd: nothing to report, but will spend ~50% of my time on ST from now on
  - quite: nothing to report
  - nisse: checking out the ST repos.  Made an MR to slightly improve task
    install.  Will spend ~50% of my time on ST from now on

## Decisions

  - Decision: new meet time is Mondays, 1200-1250 UTC
    - (Background: nisse does not work on Wednesdays.)
  - Decision? Use TPM 2.0 to store keys and enable Secure Boot for stboot
    - https://git.glasklar.is/system-transparency/project/documentation/-/merge_requests/10
    - Defer, will not be decided this week

## Next steps

  - ln5: review MR's
  - ln5: support nisse and rgdd in ST onboarding, and support handover of the
    tasks fredrik will be running from now on
  - nisse: discuss warm-up ST tasks with ln5
  - Kai: finish the explainers and open the MR, and start documenting ideas for
    the next remote attestation protocol 
  - philipp: finnish the "getting started tutortial", and improve documentation
  - quite: understand various ways of booting stboot, and their benefits -- also
    how these tie to various hw constraints/options.
    - When there are questions about how the project works, concerns, confusion,
      etc., it would be very valuable to get that relayed to us as you
      experience it
  - rgdd: start paging in ST, ~end of next week

## Other

  - https://docs.system-transparency.org/
  - docs repo maintainer(s)?
    - docs repo: https://git.glasklar.is/system-transparency/project/documentation
    - who should be the maintainers?  As kai mentioned, he can't merge there
    - Good question, make progress on this async
    - nisse: one idea is to just let everyone push, it will not break things in
      subtle ways
  - Upcoming proposal discussion (priorities)
    - Philipp did two, kai did one.  How do we get feedback and move them
      forward?
    - Fredrik: don't want to promise that we discuss a particular thing at any
      particular weekly.  Want to make sure that the general question is how we
      make effective progress towards our goals.
  - Fredrik: on the topic of when we should have the ST weekly.  Was a bit too
    quick to dismiss UTC aspect.  Would like to listen in the pros and cons of
    UTC, right after this meet and make a decision on it without too much
    discussion.  And document the decision in the pad, to not move it several
    times.  (Edit: same time as our earlier decision, but expressed in UTC.)
  - Fredrik about general questions.  What is most important, what is most
    urgent.
    - Processes (rgdd, ln5)
    - Painpoints and infra of pilot users (ln5, quite)
    - Major interfaces (kai, philipp)
      - What are they? What are the categories? Etc.
    - Releases and code quality, possibly other practises of development cycles
      (nisse, kai)
    - Contribution Licence Agreement (CLA) (tentatively: linus, nisse)
    - Open question: anything else you're missing in this list?
      - philipp: testing and quality assurance, definition of done
      - niels: would need a bit more st page-in before having an informed
        opinion
    - Note: not saying "you decide", rather, who works on proposing drafts
  - Would like to re-iterate that: fredrik intend and prefer as project lead ->
    as hands-off as possible.  Will ask y'all to work on specific things now
    during a transition period.  Then you'll be able to plan your time and
    execute on the goals/vision more independently.
  - Fredrik would like to talk with you all individually what each topic means
    in more detail
  - Also: not asking to only work on these things and nothing else until they're
    done
  - First talk to your partner, then reach out to someone else that has
    opinions.  And after that to Fredrik.
