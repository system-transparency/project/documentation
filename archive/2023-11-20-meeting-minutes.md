# System Transparency weekly

- Date: 2023-11-20 1200-1250 UTC
- Meet: https://meet.sigsum.org/ST-weekly
- Chair: kfreds
- Secretary: zaolin

## Agenda

- Hello
- Status rounds
- Decisions
- Next steps
- Other

## Hello

- <add participant here>
- zaolin
- rgdd
- ln5
- jens
- kfreds

## Status rounds

- <add status round here>
- rgdd: released stprov, see st-announce
- rgdd: brief update on how it's going on the milestones I'm "driving"
  - Status-quo releases of ST components:
    - https://git.glasklar.is/groups/system-transparency/-/milestones/14#tab-issues
    - we're mostly there now, mainly docs.sto that's still bubbling
    - then there's the final step of considering what we want to improve until
      next time in the release process, comments much appreaciated here:
      - https://git.glasklar.is/system-transparency/project/documentation/-/issues/29
  - Update website and mailing lists
    - https://git.glasklar.is/groups/system-transparency/-/milestones/9#tab-issues
    - done, except that we're blocked on getting a landing page
- nisse:
  - Fixed crash on missing TPM,
    https://git.glasklar.is/system-transparency/core/stboot/-/issues/148
  - Will work mainly on sigsum, not ST, rest of this month, for CATS workshop.
- zaolin:
  - worked on the ST release for docs repository v0.1.0.
  - discussion with rgdd and jens about the upcoming.
  - overall docs.sto improvements based on feedback from rgdd and others.
  - discussion with nisse regarding uki signatures. Proposal next week incoming.
- jens:
  - released stboot and stmgr
  - worked on documentation
  - synced with zaolin and rgdd regarding roadmapping / milestones
  - worked on ideas for further release improvements
  - stared review of stboot issues / MRs
- kfreds: a bit of planning, fyi:
  - rgdd / nisse are not available for two weeks because of CATS / Sigsum
    events.

## Decisions

- <add decision here>

## Next steps

- zaolin:
  - Finish docs release before Wednesday and close wrap-up docs milestone.
  - Mainly focus on the rest of the milestones, especially the getting started
    guide.
  - Writing proposal for UKI signatures in the sigsum context.
- ln5:
  - Linus might be available for some milestone related matters.
- Jens:
  - Working on getting started guide milestone related documentation issues.
  - Getting feedback for my own milestone from ST folks.

## Other

- kai is back from vacation on Thursday.
