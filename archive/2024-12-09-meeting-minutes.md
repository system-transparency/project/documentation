# System Transparency weekly

- Date: 2024-12-09 1200-1250 UTC
- Meet: https://meet.glasklar.is/ST-weekly
- Chair: rgdd
- Secretary: rgdd

## Agenda

- Hello
- Status rounds
- Decisions
- Next steps
- Other

## Hello

- rgdd
- elias
- ln5
- nisse

## Status rounds

- nisse: merged ip-address logging MR in stboot
- nisse: started thinking about sigsum verified os packaged, started related
  refactoring in stboot to be able to get that in later
- nisse: misc other things
- elias: some small MRs/discussion with nisse (e.g. guestfwd fixing which wasn't
  needed anymore)
- ln5: configured 4 GL runners, i think they have been tested a bit now.
  - elias tested something, right? stmgr? yes, did a rerun on a job, worked
  - (changed config in the way ln5 suggested -- aka changed config for stmgr
    project)
- nisse: ci jobs that rerun periodically every night?
  - talked about this
  - e.g. then we would detect that qa certs expire

## Decisions

- Decision? How to add sigsum logging of OS packages
  - https://git.glasklar.is/system-transparency/project/documentation/-/merge_requests/64
  - nisse: maybe not ready for decision today but would like to talk about it
  - want to get sigsum sigs for os packages without making radical changes
  - 1st iteration -> what is in proposal right now
  - nisse: things it is important that one can publish os packages that work
    both on old stboot and new stboot (aka works with and without sigsum
    depending on stboot)
  - nisse: sigsum proof as in the proposal -> nothing that expires
  - nisse: can keep signing root cert as we already have it today; but if we
    have a sigsum trust policy then we require a sigsum spicy signature.
  - we think this might be a simpler proposal, nisse will take a stab at it
  - one problem nisse sees with cert for sub keys: hard to know which subkeys it
    should care about
  - aka if the root cert can be owned -> can create new keys that the monitor
    can't see. Because certs not transparent.
  - nisse will take a stab and ask for more input

## Next steps

- rgdd: pick up where i left off (aka github mirroring and sb). Maybe also the
  "file issues for dep updates" script.
  - ln5: monitor-01 please
- ln5: fix runner settings globally for the st group in gl (not just stmgr)
  - what's not been done: don't use "instance runners", so that's what's missing
  - please keep an eye on if something breaks (you should get email when it
    fails)
  - please trigger some rebuilds in your favorite repos (ln5 enabled now)
- elias: will trigger reruns
- ln5: two things needs to be fixed with runners. One is caching of container
  images. We never resolved this before. 1st fix ln5 thought of is: fetch from
  our own GL registry. So just upload the images we want to use. And then use
  them. Then we can fetch them on every run again without worrying about rate
  limits. So that's part 1. Part 2 is to to change all yaml files to fetch from
  our own registry.
- ln5: second thing: runners on multiple systems (e.g. sigsum will have its own
  vm). Would like to make them have a shared cache (e.g. golang things and
  everythying else).
  - might not do all of this this week, but it's what's on my list wrt. runners
- nisse: new stab on sigsum proposal
- nisse: started looking at the logging lifetime on certs (st-v1.2.0 milestone)
  - clear messages in the log or want to fail early?
  - rgdd: fail as early as possible
  - nisse: https certs, do we do anything with them? why (not)?
  - because it can be many. maybe can die if all expired?
  - what if someone wants to run with http?
  - you don't have to have an https cert
  - could e.g. die when you see https in url

## Other

- None
