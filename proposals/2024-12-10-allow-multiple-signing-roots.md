# Allow multiple certs in ospkg_signing_root.pem

## Motivation

There are a couple of potential use cases for multiple signing root
certs. 

* When using provisioning package, the provisioning package may be
  signed by a different org than the main OS packages, with different
  trust roots.
  
* When not using CA certs, but instead having the signing cert (and
  keys) directly in the trustpolicy, multiple certs enable use of more
  than one signing key (with configured threshold).

Allowing multiple certs is backwards compatible.

## Proposal

Allow multiple certs in the `/etc/trust_policy/ospkg_signing_root.pem`
file. This is a text file, with each cert in its own `-----BEGIN
CERTIFICATE-----`, `-----END CERTIFICATE-----` block, similar to the
`/etc/trust_policy/tls_roots.pem` file.

## Security considerations

The signature threshold in the trust policy applies only to signing
certificates, requiring at least n distinct and properly certified
keys to sign the package. For an attacker to create a valid set of
signatures (in the OS package descriptor) for an OS package of the
attacker's choice, the attacker needs to either:

1. Compromise n keys (attacker's choice) out of all certified signing
   keys.
2. Comprimise one key (attacker's choice) of the CA keys in the trust
   policy.

Hence, adding additional trusted CA certs directly increases the
attack surface for the second type of attack. Additional CAs typically
also imply a larger set of certified keys, increasing the attack
surface for the first type of attack.

## Implementation

Needs changes to the cert-handling code i stboot, and to the reference
docs,
https://docs.system-transparency.org/st-1.1.0/docs/reference/trust_policy/#os-package-signing-keys.

The test `stboot/integration/qemu-provision.sh` would benefit from
this feature, and is a natural place to get integration test coverage.
