# System Transparency weekly

- Date: 2024-02-12 1200-1250 UTC
- Meet: https://meet.sigsum.org/ST-weekly
- Chair: kfreds
- Secretary: rgdd

## Agenda

- Hello
- Status rounds
- Decisions
- Next steps
- Other

## Hello

- away: nisse
- rgdd
- jens
- philipp
- kai
- kfreds

## Status rounds

- nisse:
  - meetup last week
  - I'd like the $ID/$AUTH proposal below to move forward.
  - I support
    https://git.glasklar.is/system-transparency/project/documentation/-/merge_requests/51,
    with the caveat that I'm assuming the network_interfaces deserialization can
    be implemented in a "textual" way orthogonal to the bonding setting, more or
    less as I outlined in a comment on the MR.
- rgdd: meetup last week, not much to report other than some progress on
  proposals
- jens: meetup last week, commented on efi stub topic in stmgr
- kai: meetup obv. fixed some remaining bugs in the tools developed for the
  meetup
- philipp: meetup. investigate more stuff regarding systemd credential
  protection
- kfreds: Time to plan what we (me, Philipp, Kai, Jens) should work on until the
  next Glasklar meetup.

## Decisions

- Decision: Delete $ID/$AUTH, see
  https://git.glasklar.is/system-transparency/project/documentation/-/merge_requests/52
- Decision: Take the initial steps towards backwards-compatible hostcfg
  - https://git.glasklar.is/system-transparency/project/documentation/-/merge_requests/53

(Next week we will have a follow up proposal on "network_interfaces", wip:
https://git.glasklar.is/system-transparency/project/documentation/-/merge_requests/51)

## Next steps

- rgdd: proposals and stprov milestone
- jens: follow up on ongoing mentions/questions in st issues/proposals/MRs
- kfreds: figure out what the platform plumbing team should work on
- kai, philipp, jens: ^

## Other

- kai: reviewing the pads, some of them are in Swedish, e.g. the "users Q&A
  session".
  - rgdd will fix so there's an english alternative
