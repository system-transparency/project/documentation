# System Transparency weekly

  - Date:  2023-06-14 1200-1250 UTC
  - Meet:  https://meet.sigsum.org/ST-weekly
  - Pad:   https://meet.sigsum.org/p/ST-20230614
  - Chair: ln5

## Agenda

  - Hello
  - Status rounds
  - Decisions
  - Next steps
  - Other

## Hello

  - ln5
  - Foxboron
  - kfreds
  - Joel
  - Kai
  - Philipp

## Status rounds

  - Foxboron
    - ospkg dl slowness
    - leaning towards that this isn't really fixable with stboot
    - this might be something the ST operators might need to think about
  - kai
    - enrollment part of stauth implemented, adding test/demo to CI/CD
  - philipp
    - fixed stuff in sthsm, plus tests being run in CI/CD (using a real HSM)


## Decisions

  - None.

## Next steps

  - ln5
    - finish sthsm review
    - create a "release definition document" in core/system-transparency listing code and documentation source information, and something using it (probably Makefile or mkrel.sh, TBD)
    - release R.2. "Provisioning PoC"
    - prepare HSM provsioning hw for signing the UEFI shim
    - share the start of a documentation setup i've been working on
  - Jens focus
    - Until vacation: documentation
    - After vacation: stboot
  - Foxboron
    - Depends on what we decide wrt the slow dl issue
  - kfreds
    - Planning/Decisions with Linus
    - Documentation
  - Kai
    - quote generation is next up
    - if you have a tpm2 device in a computer, you can try enrollment
  - Philipp
    - shim review prep

## Other

  - None.
