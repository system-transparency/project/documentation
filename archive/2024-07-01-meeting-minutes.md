# System Transparency weekly

- Date: 2024-07-01 1200-1250 UTC
- Meet: https://meet.sigsum.org/ST-weekly
- Chair: rgdd
- Secretary: rgdd

## Agenda

- Hello
- Status rounds
- Decisions
- Next steps
- Other

## Hello

- rgdd
- kfreds
- ln5
- jens

## Status rounds

- rgdd: release things with nisse -- didn't release anything yet but it should
  mostly be mechanical from this point and forward unless anything unexpected is
  found while going through the release protocol.
- jens: updated tutortial for hifive board to use stable release of ST. Deep
  dive in hardware generator number problem. Will do write up wrt. what is
  important.
  - The updated tutortial is not checked in yet

## Decisions

- None

## Next steps

- rgdd: wrap up releases
- jens: wrapping up the tutorial

## Other

- https://bornhack.dk/bornhack-2024/
  - "Camping, hacking, and the combination of the two"
- jens: when redoing the tutorial, documentation on stable release turned out
  really nice. Looked once it was finnished because curious, but when came back
  to it and used it -- this was really nice work!
