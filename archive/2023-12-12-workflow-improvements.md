# Release Improvements - still WIP by Jens

This document is a retrospective on the current release workflow. It also takes
into account how we work towards releases. This includes the milestone based
organization of work and the different roles like *milestone driver*,
*assignee*, *maintainer*.

The collection of topics in the document can serve as a starting point for
further discussion, and parts of it can be taken as drafts for proposals to
further develop and improve our workflows. 

## Benefits of release workflow 
- Consumers get information quick and easy 
  - Announcements on mailing lists 
  - NEWS file shows changes and information in upgrading 
- Lightweight  
  - A release is mainly just a tag 
- Per-Repo-Releases  
  - Allows for independent development  
  - Still provides a way to mark interoperable versions of artifacts in ST

## Release Workflow Improvements

### Automation of NEWS File Generation
I assume the current situation where we have a maintainer or a dedicated person
responsible for cutting releases of our different repositories and also other
developers contributing to the respective code bases. 

Currently, the maintainer needs to scan all commits since the last release to
identify new features and breaking changes to clearly communicate these in the
NEWS file. Although it is often clear from the commit messages, sometimes things
can be hidden or merged in commits.

An automated generation of the NEWS file based on clearly structured commit
messages can help to accelerate and harden the information transfer from commits
to the changelog/NEWS file. Designing the automation based on structured commit
messages adds redundancy in the way that the commit history itself becomes a
well-defined changelog. The NEWS file could structure the different types of
changes like fixes, features, refactoring, etc.

A well suited convention for this kind of commit messages is Conventional
Commits. The implementation of this spec in the CI/CD pipeline can be done in
different ways: Either by integrating respective tooling or just using regex
checks. The important part is that is can also be integrated in the developer's
local setup. 

Enforcing these kinds of commit messages comes with the need of more carefully
splitting the development work into commits that only contain isolated peaces of
work. For example, a developer need to split bug fixes or code refactoring from
implementation of new features and also put breaking changes in separate
commits. This shifts some work from the maintainer doing the release to the
developer's contribution. Although the maintainer should have a good and deep
understanding of the repository, the author of new code is the expert in his
changes. It also makes developers think about breaking changes in the first
place, which I think is very important in the multi-component ST universe.

Coming back to the maintainer doing the actual release: Automation of the NEWS
file generation does not necessarily mean that the maintainer can/should not
further edit/check the generated NEWS files. The auto-generated text based on
conventional commits just serves as a pretty good draft. 

### Extending the Release Bundle

At the moment we call a certain tag a release, announce it and create a release
page for it in the respective repositories. On these release pages, one can
download the archived code base for this release.

It would be nice if the NEWS and maybe also the RELEASE files would be shown
there as well, to quickly get the important information without checking out the
repot at the dedicated tag. This improves browsing through recent releases,
finding the right one - especially in the early phase of ST doing releases and
people need to find and use not always the latest one. 

### Open Questions 
In each repository, each release is a tag. But should each tag also be a
release? (Example stboot v0.3.0)
- PRO
  - can be used to refer to new states of the program/artifact in other
    programs/artifacts of ST 
- CON
  - Tags are recognized by users and due to our release workflow users assign
    tags to releases, so a tag without a release can confuse the user looking
    for information on what changed with this tag. It generally introduces
    inconsistency. 
  
## Benefits of Milestone-Based Work Planning 
- With top level ST milestones there is a good overview of ongoing tasks
- The upcoming task in ST usually cannot be assigned to a single repo, it is
  common that changes are need in different repos to achieve the goal. This is
  nicely manageable with top level milestones.
- Therefore, it is helpful to have a dedicated person responsible to drive this
  milestone.

## Improvements of Milestone-Based Work Planning
Further discuss and specify the role of "Driver of a Milestone". 
I propose: A driver of a milestone 
- May not necessarily be a maintainer of one of the affected repositories
- May not do any implementation work 
- Should coordinate and assign Issues 
- Coordinate with the team to get the milestone done. 

## Open Questions 
