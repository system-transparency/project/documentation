# System Transparency weekly

    - Date:  2024-05-20 1200-1250 UTC
    - Meet:  https://meet.sigsum.org/ST-weekly
    - Chair: nisse
    - Secretary: <add name here>

## Agenda

    - Hello
    - Status rounds
    - Decisions
    - Next steps
    - Other

## Hello

    - nisse

## Status rounds

    - None

## Decisions

    - Deferred: Clean up ST's organisation on GitHub
        https://git.glasklar.is/system-transparency/project/documentation/-/merge_requests/55
    - Deferred: Remove Twitter account
        https://git.glasklar.is/system-transparency/project/documentation/-/merge_requests/60

## Next steps

    - None

## Other

    - None
