First run integration/qemu-boot-from-disk.sh in the stboot repo.

Copy the resulting images integration/out/disk.img and
integration/out/stdata.img to two usb sticks (dd to entire device,
e.g., dd of=/dev/sda).

Insert both sticks into machine under test, and convince bios to boot
from usb disk (with secure boot disabled).

Tested with stboot as of commit
b3450f3ed06f46e76cccff53a0a8bbfc10e827d3. Successfully booted on
Rasmus' laptop, and on the "stime" supermicro server.

For the case of stime, log in to the bmc, open the console, get into
the bios menu (if needed, by restart followed by pressing <DEL> at the
right time). Set the first entry under "FIXED BOOT ORDER Priorities"
to "UEFI USB Key" and reboot.
