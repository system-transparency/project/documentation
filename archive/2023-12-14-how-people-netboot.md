# Competitor Intelligence - stboot and stprov

How do people do provisioning?
How will people netboot in the future?


## Intel AMT
We have never seen anyone using Intel AMT in the field.
Actually, governments and such completely disable it due to it being very non-transparent.

## iPXE
Still very popular. Just works. Many boot servers out there
Interesting project for further reading https://netboot.xyz

## Via BMC
The second best is probably via BMC/RoT as seen on the new OCP-MHS platforms where the flash is not even directly attached to the host anymore. 
Further reading: 
- OCP "flashless boot" (early project name) https://drive.google.com/file/d/1XRKpd5VWLFwnrn4yMQlTU2oAIdMOVvFg/view
- OCP Modular Hardware System https://www.opencompute.org/wiki/Server/DC-MHS

## UEFI HTTP/HTTPS or other UEFI stuff
- no known usage

## LinuxBoot style individual netboot
These methods are in general similar to our approach:
- Some have fancy LinuxBoot netboot payload. 
- Some use some gRPC magic 
- Others used to use specialized versions of
- u-root pxeboot
- u-root netboot
- u-root fbnetboot, but even Meta is not using it anymore, but a more specializes internal version.

## Insight in how some hyperscalers do it:
They pay big money so that someone pre-installs the machines manually by hand at their supplier.
This includes:
- flashing the OS on every disk
- setting up raid
- configuring hostnames
- etc. 
Same for the BMCs, they got them initially provisioned from their supplier and then only do the last mile via API 
- e.g. embed keys as needed
So veeeery manual workflow.
