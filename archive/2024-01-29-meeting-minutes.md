# System Transparency weekly

    - Date:  2024-01-29 1200-1250 UTC
    - Meet:  https://meet.sigsum.org/ST-weekly
    - Chair: ln5
    - Secretary: nisse

## Agenda

    - Hello
    - Status rounds
    - Decisions
    - Next steps
    - Other

## Hello

    - rgdd
    - ln5
    - nisse
    - philipp
    - kai

## Status rounds

    - rgdd: making progress on stprov milestone with assists from ln5
        - https://git.glasklar.is/groups/system-transparency/-/milestones/20#tab-issues
    - rgdd: debugging what's changed in host config the past ~2y wrt. deployed systems
        - https://git.glasklar.is/system-transparency/core/stboot/-/issues/162
    - ln5: building stprov boot images using the stimages repo
        - https://git.glasklar.is/system-transparency/core/stimages
    - jens: looked and still looking into issus/MRs where I was tagged, and try to help
      with feedback / annotations
    - jens: wrapping up current findings regarding OS package / OS image redesign 
    - nisse: working on how to create a bootable diskimage (not iso) with stboot.
    - philipp: started to work on systemd_cryptenroll and finishing secure boot roles
    - kai: finished the systemd-enabled stprov (systemd-stprov :)), booted a supermicro server with it via the BMC.
        - https://git.glasklar.is/system-transparency/platform-plumbing/st-platform-features/-/tree/main/stprov-systemd-approach

## Decisions

    - No decisions.

## Next steps

    - rgdd: docdoc issues in stprov milestone and testing on real hardware; and ref specs
    - ln5: building boot and ST images for QA testing
        - https://git.glasklar.is/system-transparency/project/qa-images
    - jens: prepare OS package / OS image session / discussion at meetup
    - kai: figuring out pcrlock and how to use it to precompute stboot pcrs
    - philipp: finishing pcr pre-calculation/resealing, systemd-cryptenroll together with Kai, integration of all components on real-world hardware.
    - nisse: continue disk tests https://git.glasklar.is/system-transparency/core/stboot/-/issues/158, maybe polish stboot-related scripts.

## Other

    - Next chair: rgdd

    - Meetup in eight days. Sessions?
      - jens: I'll add one regarding OS package / OS image format

### Host config, stboot and stprov

	- Let's talk some hostcfg?  Would like to sync a bit on what could be cleaned up, and what would be reasonable to possibly clean-up asap to make the spec and implementation easier without breaking things.  It would be good to also start talking about how we would like to deal with the known incompatibilities wrt. already deployed systems for our "status quo" releases (see issue 162 in rgdd's status round).


Recall current hostcfg, what stboot is expecting:

    - network_mode ("static" or "dhcp")
    - host_ip (ip addr + netw prefix)
    - gateway (ip addr)
    - dns (list of ip addr)
    - network_interfaces (list of name+macaddr)
    - ospkg_pointer (comma-separate list of URLs, or initramfs file path)
    - identity (string)
    - authentication (string)
    - bonding_mode (string)
    - bonding_name (name of the logical interface to bond into)

Would it make sense to clean up, i.e. remove network_mode, identity, authentication?

    - Kai: seems reasonable, might be good idea to ask jens
    - ln5: network_mode explicit, doesn't bother me
    - nisse: the more redundancy in the config -> the more states you can have
    - ln5: regarding id/auth, was ideas some time ago that we might want to have a single ospkg per thing.
        - nisse: but you can do that in stprov

Should null values be allowed in the future? 

Host config with some omitted keys? Could make sense.

null vs empty string -> nisse don't have an opinion right now.

Separate to this we also have backwards compatibility issues to resolve

    - "network_interface" -> no longer supported
    - "network_interfaces" -> then also changed format after being introduced
    - "provisioning_urls" -> the new thing is "ospkg_pointer", not just a rename
    - "dns" -> used to be a single ip address, now a list of ip addresses

ln5: would be nice to sit down with jens, rgdd will try to page him with higher bandwidth

heads-up from kai: became big fan of networkd, because can do complex network config. One way forward could be hostcfg -> network + one url.  Or just a networkd config.  To do less networking setup things in our code bases.

(networkd: just sets up interface, bonding, etc.  Much less than network manager.)

Kai suggesting shipping sytemd/networkd as one binary; stboot starts that daemon in the background and uses that for configuration.








