# System Transparency weekly

  - Date:  2023-09-18 1200-1250 UTC
  - Meet:  https://meet.sigsum.org/ST-weekly
  - Chair: Fredrik
  - Secretary: rgdd

## Agenda

  - Hello
  - Status rounds
  - Decisions
  - Next steps
  - Other

## Hello

  - ln5
  - rgdd
  - kai
  - quite
  - nisse
  - kfreds
    - "hyrum's law"
  - doctor-love
  - jens
  - philipp

## Status rounds

  - rgdd: not much to report yet, briefly started paging in my navigating repos,
    enganging in a few MRs/issues/meets.
  - nisse: Some warmup MRs, below proposal on testing. Fixed CI.
  - quite: run through tutorials, learning more; submitted some fixes for task files
  - philipp: All systems go! & Image Linux Container summit last week. Learning
    about SHIM signing and secure boot integration and options. Also get into
    immutable systems and new security technologies.
  - kai: i was at all systems go! last week. Not much happeded code-wise, but I
    got some ideas about the future of ospkgs and some stvmm.
    - systemd community tackling many similar issues as us
    - shim review process mostly deadlocked at the moment, very hard to get one
      of those through at the moment (is the rumour)
  - ln5: looked into reproducible-builds issues with debians snapshot service;
    HPE test systems procurement
    - about pitching in and contributing to the components used/required by st
    - HPE server will be able to run open BMC
    - "a platform we have tested ST on"
    - hopefully more systems/platforms to test on later
  - fredrik: working on osfc talk, including digitalizing many pen-and-paper
    sketches
  - jens: paging in after vaccay, working on trust policy references
  - doctor-love: a ghost until ~october, observing for now!

## Decisions

  - Decision? https://git.glasklar.is/system-transparency/project/documentation/-/blob/main/proposals/2023-09-11-tweakable-integration-tests.md?ref_type=heads
    - Nothing stands out, seems like a good direction forward
    - Detailed input from jens/linus would be fantastic
    - Deferred until next week (see action point below)

## Next steps

  - rgdd: more paging in, then eyeing $feature-request issue(s)
  - kai: still writing documentation (references). ospkg is done, trust policy
    is WIP and stauth measurements are next.  stvmm if I need to switch focus
    for a bit.
  - quite: digging & learning, especially secureboot stuff which is hard to
    fully get
  - philipp: still writing documentation. secure boot docs are nearly done
    (proposal). Working on the leftovers, getting started guide is also nearly
    ready. Documenting the remaining interfaces from Kai.
  - nisse: experiment with stboot using u-root's libinit
    - if that works out nicely -> building things will be much simplified
    - "can stboot run as the init process?"
    - jens: "nice!"
  - nisse: happy to review interface docs, assign me as reviewer.
  - jens: go through all MRs in stboot/system-transparency/documentation.
    Review.  Other than that, reference documentation.
  - jens, linus: move the above decision forward until next week
  - jens: review docs added by kai/philipp
  - ln5: general support, infrastructure in particular
    - if you see 137 CI error, please let me know
  - fredrik: osfc talk, comment on proposals, triage

## Other

  - Q: are the options: either glasklar signing the shim, or user doing it on
    their own?
    - (Currently: no decision on how to move forward here.)
    - Philipp will push docs about this
    - option 1: disable secureboot, "things work anyway we like after that"
    - option 2: users install their own keys for secureboot, "nice, but rather
      complicated for users to do"
    - option 3: attempt to boot with microsoft keys that are already in the
      machine when you buy it, but then we need the shim signed by microsoft.
      That's the only case when shim is needed.
    - philipp: there are more options.. which comes with different flavors.  So
      there are more ones, but roughly, yes.
    - request from fredrik: please make the docs to a tree.  "Top of the tree,
      is secureboot enabled or disabled".  Etc.
    - Fredrik's understanding:
      - Disable SB
      - Enable SB:
        - Use default (Microsoft) keys
          - ...
          - ...
        - Provision own keys
    - High-level question: meaning of the microsoft signatures?  That "that
      company is doing nice things and we revoke the key if we find evidence of
      otherwise?"
      - roughly yes /philipp
  - u-root as a library?
    - comment about this from fredrik
    - in general, trying to use everything we can as a library -> good, gives us
      more control over which functions are invoked and less magic
    - Q to jens: any reasons not to run u-root as a library?
      - In regard to tooling or something else?
      - What nisse considers is stboot only, but when running stprov we we shell
        utilities and such.  So currently thinking jsut about stboot.
      - jens still missing some context
      - jens: not issuing u-root in tooling, would like to issue u-root library
        code for that in stmgr
      - nisse and jens will discuss later
  - OS-pkg vs UKI?
    - Request: pros and cons in a proposal would be helpful if we want to change
      direction on that
  - Fredrik would like to chat more about u-root as a library and os-pkg / uki
    with $people until next week and get the notes of that discussion down.
    - Running out of time now.
    - Fredrik takes this as an action point to reach out.
  - Curious about stvmm, what's that?
    - Short summary is:
    - stboot booting an os pkg that contains firecracker (vmm manager)
    - some time of authenticated api that essentially does: list, stop, start
      vms.
    - benefits of having that: if you want to run a new workload that is still
      transparent (say a jitsi server), normally you have to reboot entire
      machine.  If the service is running inside VMs, we can still maintain an
      unbroken chain of measurements [without hard machine reboot].
    - [many nice properties from using firecracker...]
    - [cloudhypervisor project, sharing firecracker code?  Maybe interesting to
      look at.]
 - Systemization of Knowledge: Attestation in Confidential Computing
   - https://www.youtube.com/watch?v=pJXvKvJVHt8
