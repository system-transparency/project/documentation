# System Transparency weekly

- Date: 2024-07-08 1200-1250 UTC
- Meet: https://meet.sigsum.org/ST-weekly
- Chair: rgdd
- Secretary: rgdd

## Agenda

- Hello
- Status rounds
- Decisions
- Next steps
- Other

## Hello

- rgdd

## Status rounds

- rgdd: handled the the issues and MRs from quite, and responding to a few other
  things in gitlab as well. And did a bit of rubberducking for kfreds who's
  writing on a new www.sto landing page. There's been some nice progress on that
  now!

## Decisions

- None

## Next steps

- rgdd: (still) sit down for a day and wrap up the st-1.1.0 release
- rgdd: get updated www.sto deployed with kfreds

## Other

- Reminder: weekly meets are on pause now until Aug 19
  - Keep in touch async and/or do meets on a need basis
