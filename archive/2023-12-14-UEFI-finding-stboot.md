Ways for UEFI to find a boot loader

- Local storage
- Virtual CD/USB provided by BMC
- PXE
- UEFI HTTP boot

## Local storage

Disk or USB device.

File is read from path, cf. FIXME:link-to-spec

## Virtual CD/USB

The BMC provides a virtual device and boot proceeds as in the local storage case.

## PXE

Protocols: DHCP, TFTP, DNS, HTTP(S).

iPXE is a popular free/open source implementation.

## UEFI HTTP boot

Protocols: DHCP, DNS, HTTP(S).

The DHCP response provides a URI to find the Network Boot Program (NBP), in our case stboot.uki.

Implementations may vary. For example, some observed Supermicro servers do provide a way to configure the URI in "Setup" menus (and as well provisioning of CA root certs for accessing HTTPS URI), but the UEFI HTTP boot still always does DHCP for getting its IP address.

Appeared in the UEFI Specification 2.5, 2015

### References
- https://uefi.org/sites/default/files/resources/UEFI_Spec_2_10_Aug29.pdf sect. 24.7
- https://firmwaresecurity.com/2015/05/09/new-uefi-http-boot-support-in-uefi-2-5/
