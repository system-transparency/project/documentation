# System Transparency weekly

- Date: 2025-01-13 1200-1250 UTC
- Meet: https://meet.glasklar.is/ST-weekly
- Chair: rgdd
- Secretary: rgdd

## Agenda

- Hello
- Status rounds
- Decisions
- Next steps
- Other

## Hello

- elias
- rgdd
- nisse

## Status rounds

- nisse: Reviewed and merged first ospkg decrypt MR, reviewing followups.
  - https://git.glasklar.is/system-transparency/core/stboot/-/merge_requests/241
  - only the doc MR left
  - a bit unclerar to nisse if stprov needs something
  - rgdd: nothing should be needed if the goal is to hardcode the decrypt key in
    the image
  - but if a general image with different keys was wanted -> then something in
    stprov would be needed. But no-one has asked for this right now.
- nisse: Updated and merged proposals decided last week. Created issue for the
  note description proposal (for implementation).
- nisse: Increased reboot timeout in stboot, added IP address logging for
  stprov's HEAD request.
- elias: looked a bit at the decrypt MR, looks reasonable

## Decisions

- None

## Next steps

- nisse: wrap up st-1.2.0, starting with an stboot component release.
- elias: thinking about doing the error handling wrt. looping over URLs, nisse
  had a few pointers. But haven't started yet.
  - nisse: just changing debug level would already be an improvement
  - nisse: and collecting the issues and summarizing them -> a bit more
    ambitious
- rgdd: st planning with ln5
- rgdd: sb in stprov

## Other

- nisse: have elias and linus thought more about which services to stboot?
  - elias: no, we haven't started yet. But I assume we will start with services
    we think will be easy.
  - maybe first step: we use stboot at boot, but then still install a lot of
    things after. To get started.
