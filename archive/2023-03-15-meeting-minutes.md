# System Transparency weekly

  - Date:  2023-03-15 1200-1250 UTC
  - Meet:  https://meet.sigsum.org/ST-weekly
  - Pad:   https://meet.sigsum.org/p/ST-20230315
  - Chair: ln5

## Agenda

  - Hello
  - Status rounds
  - Decisions
  - Next steps
  - Other

## Hello

  - jens (leaving 13:35)
  - philipp
  - linus
  - morten

## Status rounds

  - jens: catching up after vacation
  - jens: syncing with linus re work items planning
  - morten: mkuki has been fixed, and possibly tested by philipp
  - ln5: still fighting Yubico wrt returns
  - ln5: figuring out how to deal with stprov develoment

## Decisions

  - In order to be able to start working in parallell on all ST
    things, we will postpone implementation of the generic fetch
    mechanism and add the single "initramfs" mechanism, without the
    generic framework. This is motivated by the need to get started
    with stprov.

## Next steps

  - jens
    - update the milestones to reflect the decision to "go parallell" ASAP
    - make sure that stboot and the tooling (and stmgr) use the
      new trust policy and host config
    - implement loading from initramfs, without the more generic
    "fetch mechanisms", activated when the trust policy says "fetch:
    initramfs"

  - morten: finish mkuki and incorporate it into tasks

  - philipp
    - looking into the shim review process and
    - finish documentation for the http boot

  - ln5
    - ask fredrik for high level documentation
    - reproduce reported stboot download issues reported on shitty links

## Other

  - morten comments on the [shim review discussions](https://git.glasklar.is/system-transparency/project/documentation/-/blob/main/archive/2023-03-13-uefi-shim-notes.md)
    from two days ago
    - skipping a cert from a CA is fine
    - the dbx is being deprecated
    - agree that attestation is certainly a good to have on top of secure boot

  - philipp wants a central documentation site, with high level descriptions, also with figures
    - including explaining the value of ST, and not only dev docs and user docs

  - examples of remote attestation
    - https://github.com/google/go-attestation/
    - https://fosdem.org/2023/schedule/event/image_linux_secureboot_ultrablue/

