# System Transparency weekly

- Date: 2024-04-22 1200-1250 UTC
- Meet: https://meet.sigsum.org/ST-weekly
- Chair: ln5
- Secretary: rgdd

## Agenda

- Hello
- Status rounds
- Decisions
- Next steps
- Other

## Hello

- ln5
- rgdd
- zaolin
- kai
- nisse

## Status rounds

- zaolin: Still researching component reduction, especially with focus on
  network manager. Already investigated,systemd minimal builds with buildroot,
  manual. Other network manager solutions with musl, uclibc and alpine linux.
  - (200 MIB in my earlier poc, now trying to get it down in size.)
  - Some conclusions have been written down locally, that's where it's at right
    now
- rgdd: still trying to make progress on getting our releases ready, some notes
  on wrapping up below
  - https://git.glasklar.is/system-transparency/project/documentation/-/blob/main/archive/2024-04-16-releng-notes.txt?ref_type=heads
- rgdd: been looking at / reviewing / and suggesting some improvements for the
  testing-stboot docs; and have been able to test on both our SM and my NUC now.
- rgdd: fixing so that we're able to pull in documentation to docs.sto, wip
- kai: worked on platform firmware measurement analysis and the provisioning
  mode fallback (got one round of feedback so far -- would like feedback again
  now)
  - ln5: firmware measurement, how stable is it? Unclear, Kai hopes it gets
    better. Kai wants to use all PCRs. What some projects do is not use all PCRs
    and e.g. just use PCRs one controls on their own. But then you lose a lot of
    the RA value. An option in the future is to bind to less PCRs if what Kai is
    trying now doesn't work.
  - ln5: overlap with the kind of testing being performed by the coreboot
    project? Kai says coreboot has a QA, but they mostly run in QEMU. Philipp
    says they test on some hardware but not everything. For open source firmware
    its easier. For closed source its unpredictable, hard to figure out.
- nisse: looking at collection releases, made a script to create tarball from
  manifest and signed tags. Seems to work. And looking at the docs for stboot
  right now. Also want to figure out how to do the ssh signing of the tarball
  artifacts (wip atm).

## Decisions

- None.

## Next steps

- zaolin: Trying out further options to reduce the size. Researching more
  network managers and potential systemd reduction with uclibc.
- rgdd, nisse, ln5: get releases wrapped up until EOB thu is the goal for this
  week
- rgdd: setup my release tkey
- rgdd: wrap up my build.sh MR in docs.sto (and before that a bunch of other
  small MRs relating to docs.sto), then handover to linus
- rgdd: make stprov ready for component release; release it. Will probably also
  be pretty involved in getting docs.sto component-released as well after that.
- kai: prov mode proposal is still in WIP
- kai: ek certificate validation for real life instances
- nisse: rewrite stboot README; stboot and stmgr component releases
- ln5: getting started guide, and actual release tarballs to a web server etc ->
  on my table

## Other

- quite, async: stboot README.md still says
  `Make sure your Go version is >=1.13 && <1.16`. I think that for release,
  there is a need to document which go version is required for building (all
  st\*). Might be 1.19? Stuff still (mostly?) builds (and works?) with go1.20+,
  but also causes automatic tweaks to go.{mod,sum}. Let's make people less
  confused.
  - stay on 1.19 for the releases now, then upgrade (following stable backports
    in debian)
- rgdd: might be a good idea to include git-commit or git-tag in URLs when
  referencing docs from other repos? Makes it more explicit what a component is
  meant to be implementing, which helps both users that consume component
  releases directly and ourselves when we have to figure out if we can make a
  consistent collection release for our components or not.
  - commit-ids: sounds like a good idea, versioning them could be a good idea
  - sounds like we'll give this a go
- rgdd: docs/stboot-system.md contains some TODOs -- should they be left as is?
  Should they be removed in favor of issues? Should they be moved to a single
  place? Looks a bit incomplete to have "TODO: ..:" in the running text.
  - rgdd: especially the provision mode TODO should probably be fleshed out
  - makes sense, nisse will take a stab
