# System Transparency weekly

- Date: 2024-02-26 1200-1250 UTC
- Meet: https://meet.sigsum.org/ST-weekly
- Chair: rgdd
- Secretary: rgdd

## Agenda

- Hello
- Status rounds
- Decisions
- Next steps
- Other

## Hello

- rgdd
- kai
- philipp
- ln5
- kfreds

## Status rounds

- nisse, rgdd: proposal on backwards-compatible "network_interfaces"
- rgdd: refactored and brought host configuration reference up to date
  - https://git.glasklar.is/system-transparency/project/docs/-/blob/main/content/docs/reference/data-structures/host_configuration.md?ref_type=heads
- rgdd: wip refactoring trust policy documentation and adding missing bits like
  how the OS package and TLS signing key hierarchies actually work
- rgdd: wip hashing out the next step for moving TLS signing root file forward.
  Initially started on a proposal, but after typing it down and checking
  assumption I am not so sure it makes sense. Linking what will probably be
  aborted anyway.
  - https://git.glasklar.is/system-transparency/project/documentation/-/merge_requests/54
- jens: worked on coreboot prototype. Initial setup almost done.
- jens: review proposal on backwards-compatible "network_interfaces"
- philipp: outline for credential-protection is done. Finished kernel module
  signing proposal. And stboot systemd prototype, wip.
  - https://git.glasklar.is/system-transparency/platform-plumbing/st-platform-features/-/tree/main/credential-protection?ref_type=heads
  - https://git.glasklar.is/system-transparency/platform-plumbing/st-platform-features/-/tree/main/kernel-module-signing?ref_type=heads
- kai: took a look at TCG MARS:
  https://trustedcomputinggroup.org/wp-content/uploads/TCG_MARS_Library_Spec_v1r14_pub.pdf
- kai: wrote a design doc on the st remote attestation:
  https://git.glasklar.is/system-transparency/platform-plumbing/st-platform-features/-/blob/remote-attestation/remote-attestation/enrollv1.md?ref_type=heads
- kfreds: I haven't gotten around to reading and giving feedback.

## Decisions

- Decision: Adopt proposal on backwards-compatible hostcfg "network_interfaces"
  - https://git.glasklar.is/system-transparency/project/documentation/-/merge_requests/51

## Next steps

- jens: coreboot prototype
- philipp: trying to finish systemd + stboot prototype outline. After that need
  fredrik from fredrik regarding outlines and docs, and what to focus on.
- kai: the design doc still needs to be finished. Havn't read through from top
  to bottom yet, maybe editorial. Then work on possible feedback from fredrik.
- ln5: updates to the ST quickstart / getting started documentation to a) remove
  the use of tasks and b) cover both virtual and hardware target
  - Whether it becomes a proper "tutortial" or just a "getting started guide" --
    no idea yet. Ideas welcome.
- kfreds: Read the "prototype specs" of jens, philipp and kai
- rgdd: reference specifications, MR implementation for the accepted proposal

## Other

- nisse's summary of his read-up on bonding that was linked in irc/matrix
  - https://git.glasklar.is/system-transparency/project/documentation/-/blob/main/archive/2024-02-22-notes-802.3-bonding.md?ref_type=heads

### Plumbing sync (breakout session)

- jens:
  - I'll finish the coreboot stuff tomorrow, work on documentation and after
    that we can review.
- kai:
  - I don't "need" you. I could just move on with the implementation. This is
    the protocol I intend to use.
  - Remote Attestation design doc:
    https://git.glasklar.is/system-transparency/platform-plumbing/st-platform-features/-/blob/remote-attestation/remote-attestation/enrollv1.md?ref_type=heads

Risks:

- Rough outline, how the thing has to work
- Who are the stakeholders
- What are the goals If you're fine with that there won't be any large changes.

Philipp:

- Finished the OSPKG kernel module signing proposal. It's possible with standard
  tooling. This one is completely finished. There's nothing more to do.
- Credential protection: There are multiple options. This one I need feedback
  on.
  - philipp: would be nice if freddrik can read it.
    - https://git.glasklar.is/system-transparency/platform-plumbing/st-platform-features/-/tree/main/credential-protection?ref_type=heads
- systemd + stboot approach: I don't need feedback on that. I'd just extend it
  from stprov.
