Fallback to Provisioning Mode
=============================

This proposal describes a mechanism for a provisioned stboot-enabled device to
fall back to provisioning mode.

## Problem

A provisioned server may need to be reprovisioned if its provisioning data,
like the OS package URL, are wrong or outdated. There is no way to make stboot
enter provisioning mode if provisioning data is found on the device. For now,
the operator needs to boot into a general-purpose operating system and delete
the provisioning data and reboot into stboot. Changing the booted OS can be
difficult to do at scale, especially because System Transparency recommends
custom Secure Boot keys.

## Background

stboot includes an OS package that contains the necessary tooling to provision
a device for System Transparency. The provisioning OS package contains at least
stprov to write stboot's provisioning data to UEFI variables. Generally, System
Transparency allows users to supply their own OS package to do additional
provisioning steps like storing keys or formatting the disk. Thus, we need to
assume the OS package is a general-purpose operating system. The provisioning
OS package is included in the stboot UEFI application that is protected by
Secure Boot.

## Proposal

After starting, but before the host configuration is loaded, stboot waits 5
seconds for the user to press a key. If that happens, stboot follows the same
code path as if no host configuration was found.

If a fatal error is encountered during the execution of stboot or if handing off
control to the OS package fails, stboot will also show the prompt.

## Discussion

In case the attacker cannot inject keystrokes, the system is secure. If the
attacker can inject keystrokes, the security of the system depends on two
additional factors: the Secure Boot keys and the UEFI menu password.

If no UEFI menu password is set, the attacker can access the UEFI menu, disable
secure boot, change the boot order, and run arbitrary code on the machine. If a
UEFI menu password is set and Secure Boot is using Microsoft's keys for
validation, the attacker can run any off-the-shelf operating system by storing
it on disk and changing the boot order by writing to EFI variables from the
provisioning OS package.

If Secure Boot is only using custom keys and a UEFI menu password is set, the
attacker can enter the provisioning OS package and can access all unencrypted
data on the server. The attacker can also write new provisioning data to the
server and make it boot arbitrary OS packages that pass the trust policy
checks.

In case we extend System Transparency with remote attestation, all three cases
can be detected, although not prevented. If additionally, all credentials are
encrypted using some variant of TPM sealing, the attacker cannot access the
data on the server.

## What needs to be done

Implement provision mode fall back, when normal boot fails or operator
presses some key. Will be always enabled, as long as a valid
provisioning OS package is included in the initramfs. Document the
feature and its security implications in stboot-system.md.

## Future options

To prevent misuse by an attacker, that is assumed to have console
access, some options are:

    * Require a provisioning password before booting the provisioning
      OS package. There could be an entry in the trust policy file with
      password hash + salt.

    * Have provisioning imply "factory reset" of the server. At minimum,
      wipe all private keys stored in NVRAM, and possibly wipe other
      mutable storage too, before booting into the provisioning OS package.
