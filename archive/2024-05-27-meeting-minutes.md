# System Transparency weekly

- Date: 2024-04-27 1200-1250 UTC
- Meet: https://meet.sigsum.org/ST-weekly
- Chair: rgdd
- Secretary: rgdd

## Agenda

- Hello
- Status rounds
- Decisions
- Next steps
- Other

## Hello

- rgdd
- jens
- nisse
- ln5
- zaolin (async)

## Status rounds

- zaolin: finialized stboot systemd prototype demo, finished 80% of the
  presentation.
- jens: just returned from vacation, no updates
- nisse: started to look into provisioning interrupt thing, currently wondering
  what would be the minimal refactoring to make that straight forward
  - open questions: what do we want to do in the case the attacker can get
    console access. Some options: disable feature via trust policy, or asking
    for a pw, or doing some factory reset like wiping the sensitive data in
    NVRAM before starting OS package.
  - https://git.glasklar.is/system-transparency/project/documentation/-/merge_requests/58
    - https://git.glasklar.is/system-transparency/core/stboot/-/issues/126
- rgdd: nothing to report
- ln5: nothing to report

## Decisions

- None

## Next steps

- zaolin: getting presentation done today, attending glasklar meetup
- jens: attedning meetup
- nisse: meetup
- rgdd: meetup, maybe get started on www with fredrik? Maybe clean up release
  protocol files as mentioned two weeks ago (backlogged).
- ln5: meetup, hope we will be able to do things that are hard to do offline
- ln5: FYI: will be less active in ST over the summer. I will be here during
  meetings. My focus will be on R-B, in particular Debian services and infra
  structure things. Goal: setup a rebuilder. That we do this is important for ST
  as a project.

## Other

- None
