# System Transparency weekly

- Date: 2024-06-03 1200-1250 UTC
- Meet: https://meet.sigsum.org/ST-weekly
- Chair: rgdd
- Secretary: rgdd

## Agenda

- Hello
- Status rounds
- Decisions
- Next steps
- Other

## Hello

- rgdd
- zaolin
- nisse
- kai
- jens

## Status rounds

- rgdd et al: been doing a bit of planning, nisse and i are ramping up progress
  on the next collection release which will include forceful entering of
  provision mode
  - (This is not a change in priority, just an FYI that we're now making
    progress on part one in our
    [roadmap](https://git.glasklar.is/system-transparency/project/documentation/-/blob/main/archive/2024-05-13-roadmap.md).)
- kai: finished my presentation for the meetup. implement all remote attestation
  functionality and increase test coverage
- zaolin: meetup last week + presentation
- started to look at provision mode, and recactoring. Realized i want a proper
  integration test for provision mode first. Hacking a qemu script to do that.
- nisse: new tag for stmgr, maybe it should have been a 0.4 tag. Will make a 0.4
  tag so we don't forget before the release and iterate from there on.

## Decisions

- Decision: Put weekly meets on pause during the summer between 15/7 - 12/8. The
  first weekly meet after the break is 19th aug.
  - Keep in touch async on irc/matrix and do adhoc meetings on a need basis if
    you are working during the summer

## Next steps

- rgdd: start looking at what needs to be done from a test/releng perspective
  before the next collection release
- rgdd: review MRs from nisse
- kai: improve the documentation (e.g., based on what was asked during the
  meetup) and make testing easier. Most tests shouldn't require a TPM simulator
  running. Trying to use fuzzing stuff in go; and report tpm things to replay
  later. Working on that. I think after this I feel "done" with this software.
- zaolin: remove dark mode from theme from st homepage. Extensive documentation,
  fixing build scripts + clean up, kernel source builds by default for stboot
  systemd v2.
  - (It's currently underdocumented, so it will become easier for you to
    understand and possibly integrate at some point in the future.)
  - dark theme MR about to be pushed, have it almost done locally!
- jens: have next steps but won't tackle this week. I'm goin to test coreboot
  builds for qemu and sifive unmatch with ST v1 release. And update docs.
- nisse: working on the provisioning things.

## Other

- https://git.glasklar.is/system-transparency/core/stboot/-/issues/194
  - uroot, use stprov to write hardcoded config, three line shellscript kinda
  - comment: makes sense, sounds goods
