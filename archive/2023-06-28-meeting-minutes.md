# System Transparency weekly

  - Date:  2023-06-28 1200-1250 UTC
  - Meet:  https://meet.sigsum.org/ST-weekly
  - Pad:   https://meet.sigsum.org/p/ST-20230628
  - Chair: ln5

## Agenda

  - Hello
  - Status rounds
  - Decisions
  - Next steps
  - Other

## Hello

  - ln5
  - jens
  - kfreds
  - Foxboron
  - Kai
  - Philipp
  - doctor-love

## Status rounds

  - ln5: tested sthsm and planned for CA setup
  - jens: wrapping up ongoing work
    - mostly open MRs in stboot / system-transparency
    - not much other work done this week
  - Foxboron: stboot slow dl; looked at stauth changes
  - Kai: stauth is at an MVP stage -- enrollment, attestation work; requesting feedback from the product owner
  - Philipp: sthsm testing working now, signing CSR's, now adding EFI application signing

## Decisions

  - None.

## Next steps

  - jens: vacation until 24.07.23 -- tomorrow is last day before vacation
  - Foxboron
      - resolve the stboot slow dl issue, before jens leaves
        - split out the timeout and progress bar
      - test stauth, "pipe cleaning"
  - Kai
      - fixing stuff in system-transparency (issues 186, 187)
  - Philipp
      - shim docker file integration, reprobuilding in the CI/CD

## Other

  - Should we set up some kind of dependency tracking helper in gitlab?
    https://git.glasklar.is/glasklar/services/gitlab/-/issues/48
    - renovate is used by arch linux, integrated in their gitlab flow; seems to be working
  - kfreds: Where can I find everything related to shim and sthsm?
    - Including configuration of the shim
      - It's on its way into the st-shim-review repo, done by Philipp
    - https://techcommunity.microsoft.com/t5/hardware-dev-center/updated-uefi-signing-requirements/ba-p/1062916
    - https://github.com/rhboot/shim/blob/main/BUILDING
    - https://git.glasklar.is/system-transparency/project/st-shim-review/-/blob/st/ST_BOOT_PROCESS.md <-- Building SHIM/Options
    - https://github.com/rhboot/shim/blob/main/SBAT.md <-- SBAT
    - https://git.glasklar.is/system-transparency/project/st-shim-review <-- our repo to be "submitted for review"
    - https://git.glasklar.is/system-transparency/project/sthsm <-- our tool for provisioning
  - Kai: who's the system-transparency repo maintainer and who's their stand-in for vacations?
    - Jens is the maintainer of core/system-transparency
    - Linus is the backup maintainer, happy to merge stuff he does understand
    - We're a bit short on planning in general and vacation planning in particular
  - OSFC, who's going there and who's doing a talk?
    - We should have an ST talk
    - We've had one very early, and then a short update around 2020
    - Should we be telling "the user story of an early user"?
    - Or a broader overview with current state and such
    - Linus being consumer oriented could talk about less technical things, user centric
    - What is Fredrik going to talk about?
      - Fredrik, Rasmus and Linus will plan this in person at PETS (in ~2 weeks)
    - CFP due July 16
