Unsorted notes from meetings and ln5 thinking on the topic of how we
should organise code and docu. This work was triggered by the work on
ST's initial road map in April 2023.


# From the road map draft proper

TBD: "Go monorepo" or "There is a one-to-one relationship between
artefact and git repository"?

This is a larger decision to make later. For now, we're acting
pragmatic avoiding any big rearrangements for the sake of either
monorepo or multirepo. What is important is that ST *interfaces* are
versioned and can be clearly communicated to users.

Interfaces are where an instance of a program meets another instance of a program, f.ex.
- stboot host config and trust policy formats (stmgr/stprov -> stboot -> ospkg)
  - f.ex. hard-to-upgrade stboot sharing these document formats with rapidly moving ospkg tooling
- command line utilities, documentation (human -> cmd) and flags (script -> cmd)
- golang library code shared between artefacts
  - f.ex. stboot and stprov sharing code
- \<more interfaces go here\>

The most pressing issues are (i) where should stmgr code go and (ii)
where should stprov and stauth live.

Releases can probably be encoded as gitlab milestones, once we (linus)
have pushed our current milestones one level upwards.

Gitlab milestones will be useful for planning and carrying out
development, with separate milestones for release candidates (rc's)
and the real thing (like v1.0.0). For users and bystanders, a document
like this one *without* rc's will be more useful.

TBD: Rename "Release" to "Milestone"? Unless "release" is a user
facing thing while milestone is internal to development.

Releases are separate from milestones in that they're user facing and
have release announcements and other external effects. We will have
milestones that are one of several parts of a release (or more than
one release).

# From the working notes in the road map draft pad

## dependencies between repo's

### current situation

- system-transparency uses stboot
- system-transparency uses stmgr
- stmgr calls stboot library code

## notes from meeting with jens and ln5 on 2023-04-06
- one go module means one version (with several go pkgs)
- we should have one go module per repository
- separate versioning, esp. between stboot and the rest (as far as
  we've understood so far -- might be more of these) is going to be
  important, if not already important
  - think of a user with ~500 servers running stboot in a shaky ISO
    mounted over smb/http from ~50 management server -- they do not
    upgrade stboot every time stmgr gets a new cmd line flag
- what is the cost of, both in dev time and in confusion for users,
  - splitting up modules and/or repos, and
  - merging modules and/or repos?
- ln5 wants us to refrain from making any changes to structure without
  first estimating i) the cost of this change and ii) the cost of
  reverting it, should it later show to be the wrong choice after all

### 2023-04-12 ln5 thoughts on the need of separate versioning
- claim: a single version would still work fine
- because as long as semantic versioning is kept, and especially the
  requirement that interfaces don't break within a major release,
  there is nothing stopping a user from keeping stboot at version 1.N
  while everything else moves to 1.N+1
- but the price for using one single version is high in testing and
  release engineering
  - with N releases of K programs (using one common library defining
    all interfaces) there are N * T(K) combinations of program
    interactions to test, where T(n) is the nth triangular number
    (T(1)=1,T(2)=3,T(3)=6,T(4)=10,T(5)=15,T(6)=21)
    - this is a worst case number, with all programs communicating
      both ways with all programs including itself
    - the number of possible interactions shrink when refining the
      dependencies by enumerating which programs produce and consume
      data for and from other programs, but even in the case of K
      around half the number of programs ST has, N grows when *any*
      program (or the library) need a release
- in contrast, with K programs and one library being released
  individually, the number of interactions to test are TODO (intuition
  says it's lower than N*T(K) but let's expand this)

## ln5 thinking out loud
- that last cost -- user confusion and complexity -- can be mitigated
  by us releasing traditional tar balls with everything in them, one
  per major release series
  - thank you nisse for pointing this out over lunch
- this give the developers the freedom to rearrange git repos, go
  modules and what not without breaking the contract with users --
  they keep doing `curl URL | tar xf - && make -C st install` like
  they always did
- our CI jobs would test (some) compatibility between versions by
  building and running R1.n and use (selected) artefacts from that in
  a build of R1.n+1, and so on
- within a major release series are no changes to ST interfaces that
  introduce breakage in any of the components producing or using them
- we can easily produce nightlies in addition to proper releases, to
  give users a chance to test on their unique hardware and catch
  regression (and new bugs)
- this would buy us flexibility to move things around "behind the
  scenes"
- the tar balls would be our point of contact with "consumers", ie
  operations teams and package maintainers, while our git repos would
  be how we communicate with other developers using ST for building
  new things

## scenario to consider
- there's a single repo with a single go module for all of ST
- ST-0.9 is released
- A install stboot-0.9 on all their systems and provision them using
  stprov-0.9, generating hostconfig and trustpolicy of version 0.9
- B build ospkgs using stmgr-0.9 generating ospkgs of version 0.9
- A run the ospkgs from B on their systems, all is good
- ST-0.10 is released, after verfication that
  - stboot-0.9 works well with all combinations of hostconfig and trustpolicy documents of versions 0.9 and 0.10
  - stboot-0.10 works well with all combinations of hostconfig and trustpolicy documents of versions 0.9 and 0.10
- B build ospkgs using stmgr-0.10 generating ospkgs of version 0.10
- A run the ospkgs from B on their systems, all is good
- ST-1.0 is released, after verification that
  - stboot-0.9 and stboot-0.10 fail in a decent way when coming across
    hostconfig and trustpolicy documents of version 1.0
  - stboot-1.0 fails in a decent way when coming across hostconfig and
    trustpolicy documents of versions 0.9 and 0.10, all combinations
- B build ospkgs using stmgr-1.0 generating ospkgs of version 1.0
- A upgrade stboot from 0.9 to 1.0 on one of their systems and
  re-provision them again, or alternativeliy run some program to
  convert hostconfig and trustpolicy from version 0.9 to 1.0
- A run the 1.0 ospkgs on its stboot-1.0 system, all is good
- A wants the 1.0 ospkgs to run on all their systems because they
  contain features and bug fixes they need and will have to either
  upgrade the rest of their fleet to stboot-1.0 or make B build ospkgs
  of version 0.x containing the features and fixes

## some ln5 conclusions regarding user commitments
- Users fall into two categories: consumers and other devs
  - Consumers take code that the ST project has packaged and run it
  - Other devs take code that the ST project has published and use it
- ST the project must be precise and predictable toward its consumers
  while it's enough to be explanatory and helpful toward other devs
- ST the standard will become a platform to which some entities
  publish ospkgs and some entitites consume them
  - In certain scenarios it's the very same organisation that is
    publishing and consuming ospkgs, but even here they are probably
    often not the same teams or individuals
  - This separation of concerns must be accomodated for in ST's
    versioning scheme


## notes from 2023-04-12; kai, jens, ln5

- recapitulating jens+ln5 discussions last wednesday and ln5 thinking (above)
- jens recommends using separate version numbers and not releasing 1.0
  before we're sure we're ready for it
- jens will keep using and develop the library in stboot keeping in
  mind that we will want to break it out into its own repo, with its
  own version series, at a later stage
  - sharing version number between stboot and lib might become painful
    once stboot has matured and is rarely released while other
    components have not and need changes to the lib; decided to keep
    the lib in stboot until that becomes a problem; TODO ln5: document
    this in a way that makes us aware of it and able to split the lib
    out of stboot as soon as necessary, but no earlier
- the publishing of tar balls, releases and nightlies, will buy us
  flexibility to rearrange repos and versioning while still providing
  consumer users a stable single thing to interact with and at the
  same time letting users of the type "other devs" able to follow a
  quicker development cycle
- we should have a versioned specification, perhaps in the
  system-transparency repo, which stboot and other components
  implement
- AP jens: clean up the system-transparency repo by removing most and keeping only
  - a simple and stupid Taskfile putting it all together, used by CI
  - instructions on how to do it without Task, for users
- jens: argument against putting go code in the same repo as the
  specification: golang structs and methods implementing interfaces;
  example: ulid specification, not implemented in the same repo but
  pointing at some other repo -- separating interface and implementation; example:
  - https://github.com/ulid/spec
  - https://github.com/oklog/ulid/tree/v2.1.0
  - https://pkg.go.dev/github.com/oklog/ulid/v2
- AP ln5 (DONE): add "specification" to our list of artefacts, in
  addition to documentation and code
- jens: a specification should contain as little as possible but no
  less; rationale: having more than necessary makes it hard to add
  things to implementation without changing spec, which should be
  stable;
  - ln5: a good spec helps an implementor with more than the hard
    facts like structs, but i agree that it must not limit the
    implementor more than strictly necessary

# Misc

- rendering documentation, see https://git.glasklar.is/system-transparency/project/documentation/-/issues/1
