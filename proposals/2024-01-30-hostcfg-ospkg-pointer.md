# Backwards-compatible `"ospkg_pointer"` field in host configuration

## Background

The host configuration field `"ospkg_pointer"` is a [generalization that
replaced][] `"provisioning_urls"`.  This renaming is an incompatible change.
The format of the generalized `"ospkg_pointer"` is also incompatible: multiple
provisioning URLs are listed as a single "string" with comma as the separator.
In contrast, the type of the old `"provisioning_urls"` is "list of strings".

Any platform provisioned with a host configuration that carries the old
`"provisioning_urls"` field needs to be re-provisioned to upgrade `stboot`.

[generalization that replaced]: https://git.glasklar.is/system-transparency/core/stboot/-/commit/9b8fc85fd7f041663c7a76b9ffe211375ef96b1b

## Proposal

If the `"ospkg_pointer"` field is unset, try to parse the old
`"provisioning_urls"` field instead.  If this succeeds, ignore the initial
parsing error and convert the found list into an `"ospkg_pointer"` "string".

## Motivation

The proposed change would make the transition to `"ospkg_pointer"` backwards
compatible.  This avoids re-provisioning hundreds of servers.

## Required changes

The required change is [modest][]: where we assert that `"ospkg_pointer"` is
set, add a hook for parsing and converting `"provisioning_urls"` before
aborting.

[modest]: https://git.glasklar.is/system-transparency/core/stboot/-/merge_requests/149/diffs?commit_id=c9d023872223cb6e7463cc98876ccb601fae47fd

## How to document this

The stboot reference documentation should lists which host configuration version
is implemented.  This section is amended to specify what parts of the parsing is
relaxed for the purpose of backwards-compatibility.  The relaxed parsing will be
removed eventually, but only when other interfaces anyway must be broken as
well.  This reduces the number of times that breakage is forced on users.

Note: none of this affects writers of host configurations, such as `stprov`.

## Related notes

  - [Analysis of real-world host configurations that break](https://git.glasklar.is/rgdd/hostcfg)
