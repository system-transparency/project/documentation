# System Transparency weekly

- Date: 2025-01-07 1500-1550 UTC
- Meet: https://meet.glasklar.is/ST-weekly
- Chair: nisse
- Secretary: rgdd

## Agenda

- Hello
- Status rounds
- Decisions
- Next steps
- Other

## Hello

- rgdd
- elias
- ln5
- nisse
- odd
- erik

## Status rounds

- rgdd: deployed email notifications on new (direct) dependency updates in
  stmgr, stprov, and stboot. The check runs every day at 0800 on
  monitor-01.gtse.
  - nisse: all reported issues can be viewed by
    https://git.glasklar.is/search?search=godep-check&scope=issues
- rgdd: feedback to proposals, and a bit of preliminary planning (wip). Since
  we're a bit behind on the current milestones we will renew them roughly when
  feb starts.
- elias: added issue about error handling:
  https://git.glasklar.is/system-transparency/core/stboot/-/issues/215
- ln5: generated a new ST QA signing cert for 2025Q1, a little bit late and a
  little bit wrong but still! TODO: automate relevant parts of this process, or
  at least document it properly
- nisse: Just back from vacation, catching up.
- erik: working on implementing the encrypt/decrypt proposal. To be able to
  store OS package in a CDN. Been working on both the MR and the proposal that
  goes with it. I think I've addressed all the comments you've raised. Think I
  also managed to get the integration test working, but unable to run the CI
  pipeline.
  - pipeline not working?
  - maybe because erik has the repo under his user?

## Decisions

- Decision: Add a note string to host config
  - https://git.glasklar.is/system-transparency/project/documentation/-/merge_requests/67
  - Suggestions from linus, not yet incorporated in MR: Rename "note" ->
    "description". Treat contents as untrusted, and filter out control
    characters (delete or quote?) when displaying.
  - sounds good with allowlisting, *plus ones*
  - how about name?
  - note, description? nisse has no strong opinion.
  - linus suggests description because "note" is so open. Description is more
    like "description of this host configuration".
  - linus also thought about "provisoining_info" or similar -- i.e., even harder
    scoping -- but wouldn't be super sad if it's not this.
  - rgdd and nisse are both fine with "description" -- let's go with this!
- Decision: Encrypted OS packages
  - https://git.glasklar.is/system-transparency/project/documentation/-/merge_requests/66
  - one private key per machine and encrypt it to all machines that want it, or
    shared private key between machines? Probably shared.
  - note: this is already running and tested in prod (branch from st-collection
    release)
  - +1 from rgdd, very nicely prepared proposal
  - nisse: looks good, haven't look too much in detail on age but seems good
  - nisse: would it have worked with symetric encryption?
    - it would probably have worked
    - (it would have been more alike the previous password feature)
    - (but age doesn't recommend it)
  - ln5: it's also a good example of a proposal that's external from the
    project, thanks!
  - nisse haven't looked at the implementation yet, but docs will go into
    stboot.md?
  - nisse: will at least need to update trust policy doc, in addition to the
    code MR
  - nisse will do review etc to get this merged
- Decision: Sigsum signatures
  - https://git.glasklar.is/system-transparency/project/documentation/-/merge_requests/64
  - Current proposal is the "minimal" one,
    **proposals/2024-12-09-sigsum-logged-os-package-2.md**
  - looks good to rgdd
  - ln5 haven't looked at the last stab, but if nisse and rgdd happy -> not
    worried

## Next steps

- rgdd: pick up / continue ST planning together with ln5
- ln5: add erik to ST group as developer so he can run pipelines; and check if
  it is possible to make users run pipelines (or if it is already possible and
  something needs to be clicked manually -- linus will check how this works for
  a separate user).
- nisse: review erik's decrypt MR in stboot, and ensure there's enough docs in
  all places
- nisse: click merge button on all proposals
- erik: will push a new mr from stboot repo so that ci runs correctly, and do
  fixes (if any) based on nisse's review

## Other

- elias: should st tools such as stmgr have option to show their version number,
  like for example "stmgr version"?
  - nisse: yes that would be good; already added in stboot but haven't done it
    yet in the other tools.
  - rgdd: +1
  - elias will create an issue (nisse: there might be an issue already)
  - rgdd: feel free to provide an MR based on nisse's example in stboot (or
    sigsum tools, there nisse wrapped it)
- ln5: me and elias will be working on deploying ST internally at glasklar. We
  don't run anything but test things right now at Glasklar -- and it's high prio
  to change this. If you have ideas on services you'd like to see early (or not
  early because you're so dependent on them -- e.g. we won't do gitlab first) --
  please let us know!
  - nisse: perhaps jitsi is an easy starter because it doesn't have a lot of
    state
  - rgdd: www.glasklarteknik.se ?
