# System Transparency weekly

  - Date:  2023-01-25 1200-1250 UTC
  - Meet:  https://meet.sigsum.org/ST-weekly
  - Pad:   https://meet.sigsum.org/p/ST-20230125
  - Chair: ln5

## Agenda

  - Hello
  - Status rounds
  - Decisions
  - Next steps
  - Other

## Hello

  - ln5
  - jens
  - Foxboron

## Status rounds

  - [rgdd] added a drafty milestone to support sigsum signatures, see:
    - https://git.glasklar.is/groups/system-transparency/core/-/milestones/11#tab-issues
    - (placeholder so that it can be discussed and prioritized into our roadmap)
  - [jens] adaptions in st tooling takes a lot of time
    - Blocks further implementations in stboot
      - CI cannot validate new st features in end-to-end test
    - Currently cleaning up tooling aka Taskfile as much as possible
      - make it slim
      - remove magic
      - reduce levels of configuration overrides
      - aim:
        - Easy to read
        - Usable for CI, not forgetting demo purpose
    - Autodetection done
    - HTTPS root cert / CA bundle done
  - [Foxboron]
    - Refactoring os-pkg download logic

## Decisions

  - None.

## Next steps

  - [jens] Maybe need to reschedule milestones:
    - put "new OS fetching mechanism" on hold
    - make stmgr using stboot libs
      - enable stmgr to build (default) JSON config files
      - finish stmgr mkiso
    - use this(stmgr) in Taskfile to make it even more light wight
    - continue with stboot work, since adaptions in tooling are easy then
  - [Foxboron] 
    - Finish the os-pkg download refactoring
    - Network stuff
    - Looking into UKI tooling
  - [ln5]
    - Get the outstanding MPT issue implemented and released
    - Road mapping

## Other

  - Regarding golang linting, we're using a long list of *default* linters enabled and we do suffer a bit from it. When something is annoying enough for us to formulate a reason for disabling it, we can do that.

  - User feedback #2: PXE booting will become valuable
    - There are operational issues with virtual ISO:s being remotely mounted by BMC's.
    - It seems likely that BMC's get worse and not better.
    - Adding support for PXE booting to ST should be doable. At this point it's unclear exactly how and when.
    - The network stack available for PXE is often horribly behind the modern world, so that should be kept in mind.

  - User feedback #3: We're thinking of kexecing into a new kernel, for upgrading it, from our running system. Would this conflict with any basic premises of ST? And how would we verify an ospkg, practically?
    - This is about kexec:ing from an OS pkg, specifically one that's already had other software upgraded using the distribution's package management system.
    - How would TPM measurement of this kexec:ing be handled? This is currently being discussed on the LKML. Would probably be using another PCR than the proper boot chain but there's a lot of details not hashed out yet.
    - From an stboot perspective, this does not seem problematic since stboot isn't even involved.
    - Regarding verification, it seems like a good idea to add a 'verify' command to stmgr regardless.
    - Summary: kexec from os-pkg does not seem to be violating any ST basic presumptions directly, and might be a reasonable option short or medium term. In the longer perspective it might make more sense to use the planned solution using an ST-compatible VMM for booting VM's though.

