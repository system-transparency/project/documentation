# System Transparency weekly

- Date: 2025-02-17 1200-1250 UTC
- Meet: https://meet.glasklar.is/ST-weekly
- Chair: rgdd
- Secretary: rgdd

## Agenda

- Hello
- Status rounds
- Decisions
- Next steps
- Other

## Hello

- elias
- rgdd
- nisse
- ln5

## Status rounds

- nisse: stboot@v0.5.2 is now released
  - https://lists.system-transparency.org/mailman3/hyperkitty/list/st-announce@lists.system-transparency.org/thread/2CUARAC2Z7Y4LTZUPO4MONTT2BFIYHE7/
- elias: finished stboot release testing, earlier problem with test server boot
  from USB stick was solved thanks to BIOS upgrade on test server
  - https://git.glasklar.is/system-transparency/core/stboot/-/issues/219
- elias: debugging stboot issue that happens only sometimes (~50% chance)
  - https://git.glasklar.is/system-transparency/core/stboot/-/issues/224
- rgdd: looked at + merged the x509 MR from quite
  - https://git.glasklar.is/system-transparency/core/stmgr/-/merge_requests/99
- nisse: merged go-uefi updates from quite (secure boot signatures)
  - https://git.glasklar.is/system-transparency/core/stmgr/-/merge_requests/95
- ln5: have a plan (with elias and help from nisse) for how to do key management
  for "st deploy sysadm project" at glasklar. Don't have a public pointer yet
  (mostly because it is in swedish), but we will be publishing this publicly
  somewhere soon. This means we now have what we need to get started with ST
  deploy.

## Decisions

- None

## Next steps

- rgdd: meetup, probably won't be making any major ST progress until next week
  - same for ln5 and nisse
- elias: more debugging, see issue 224 link above

## Other

- elias: release email got stuck in moderation a while. Maybe we should ensure
  that doesn't happen for too long?
  - should it be without moderation when sender is nisse perhaps?
  - moderation was on for everyone before, ln5 has now started clicking so that
    nisse should be able to send without moderation.
  - nisse should be able to moderate for his own emails though
  - rgdd: person who sends email that gets stuck -> needs to ensure it arrives
    at the list. Poke someone if you can't fix it yourself.
