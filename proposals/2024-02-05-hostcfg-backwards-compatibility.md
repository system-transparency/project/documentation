# Supplementary notes on backwards-compatible host configuration

Considered options:

  - Require old systems to be reprovisioned.  We abandoned this idea to reduce
    the number of times things break.  Forcing breakage is particularly
    unsatisfying as there haven't even been any conceptual changes in stboot.
  - Rewrite host configurations on old systems with a tool.  While technically
    feasible, it is a bit impractical because such rewrites need to be in sync
    with the updating of stboot; and if things go wrong = systems don't boot.
  - Make an stboot release that understands old formats and then rewrites them
    to new formats.  Drop this rewriting in a subsequent release, and require
    that all old systems are upgraded to the release that can do the rewriting.
    This idea was abandoned because stboot is not meant to write host configs;
    and we have the same issue that if things go wrong = it's hard to roll back.
  - Make stboot understand the old formats, and drop such legacy understandings
    when we will anyway need to make other backwards-compatible changes in the
    future.  This is the approach we're proposing -- complexity is manageable.

It's worth noting that if we had been a bit more careful about how the
transition from `"network_interface"` to `"network_interfaces"` and bonding was
conducted (as well as how these fields are used when configuring the network --
which is messy), it would have been a lot easier to propose backwards-compatible
alternatives.  We're leaving backwards-compatibility for these two fields out of
scope until next week to get all details right, starting now with the easy ones:

  - (Backwards-compatible "dns")(./2024-01-29-hostcfg-dns.md)
  - (Backwards-compatible "ospkg_pointer")(./2024-01-30-hostcfg-ospkg-pointer.md)
  - (Backwards-compatible omitted fields)(./2024-01-31-hostcfg-optional-keys.md)
