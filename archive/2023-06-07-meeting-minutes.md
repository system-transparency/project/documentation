# System Transparency weekly

  - Date:  2023-06-07 1200-1250 UTC
  - Meet:  https://meet.sigsum.org/ST-weekly
  - Pad:   https://meet.sigsum.org/p/ST-20230607
  - Chair: Linus

## Agenda

  - Hello
  - Status rounds
  - Decisions
  - Next steps
  - Other

## Hello

  - Linus
  - Morten
  - Kai
  - Fredrik
  - Philipp
  - Joel R

## Status rounds

  - Morten
    - The problem with stboot loading slowly has been reproduced: https://git.glasklar.is/system-transparency/core/stboot/-/issues/123#note_6320
    - Been poking and looking a bit at stauth
  - Kai
    - stauth remote attestaion: precomputing expected values
      - Pushed a WIP version which actually has a working component, for "closing the loop" early
      - Found some issues with stboot measuring, will talk  to Jens about it
      - Kai, let's find a time to sync /Fredrik
  - Philipp
    - Finishing sthsm
  - Fredrik
    - Uploaded design sketch: https://git.glasklar.is/system-transparency/project/documentation/-/tree/main/proposals/vision_and_architecture_wip
    - Scanned the internet for terminology regarding remote attestation
      - IETF SCITT (Kai said "rats": https://datatracker.ietf.org/wg/rats/about/) could be useful, f.ex.
  - Linus
    - Testing and reviewing for both R.1. (netbooted stboot) and R.2. (stprov PoC)
    
## Decisions

  - None.

## Next steps

  - Morten
    - stboot slow loading problem
  - Kai
    - Clean up stauth
    - Reply to some of Fredriks questions in https://pad.sigsum.org/p/ST-QnA-lowlevel
  - Philipp
    - sthsm documentation, adding auth key capability templates
    - Prepare signing of the shim, add missing documentation
    - Set up CI/CD for testing, we have a spare Yubihsm2 and can set up a gl runner here
  - Fredrik
    - Talk with Kai about stauth / remote attestation / Q&A
    - Strategy and planning with Linus (next week on Mon and Tue)
  - Linus
    - Finish the R1 and R2 releases

## Other

  - Short conversations on releases
    - "Here's what we think we can promise"
    - What do we need in order to say "this is well tested"
    - "We tested x, y, z and xy worked but not z"
    - "It runs on my computer" vs being useful in the real world
    - Robustness, Quality, Performance, Understandability of the code

  - General discussion about RA, and TKey
    - Joel
      - "I'm very excited for someone to solve this problem. I'd love to document some things that are obvious to you."
      - "What hardware should I use?"
      - "I want to boot things securely over the network and verify what's running on the system."
      - "A lot of solutions seem to work on individual details. No one seems to take an overarching perspective."
      - "It's a mix between curiosity (I want to trust my machines), experiences from work - it'd be great to just provision and then be done".
    - Linus
      - "I'm very keen on having someone following the documentation / instructions and give us feedback on what's missing / doesn't make sense."
    - kfreds
      - Joel, check out https://git.glasklar.is/system-transparency/project/documentation/-/tree/main/proposals/vision_and_architecture_wip
    - kai
      - A remote attestation market survey?
