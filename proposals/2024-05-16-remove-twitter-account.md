# Remove our Twitter account

## Background

We have a [Twitter account](https://twitter.com/st_transparency) according to 
[our GitHub account](https://github.com/system-transparency). We're not using it.

## Proposal

I propose we remove the link from our GitHub account as well as retire the Twitter account itself.

## Motivation

We're not using the account. Nor do I think we should.

## Required changes

- Remove the link from our GitHub account
- Figure out who owns the Twitter account and ask them to retire it

## Related notes

Who's in control of the Twitter account? Philipp?
