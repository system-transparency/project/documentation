# System Transparency weekly

  - Date:  2023-01-18 1200-1250 UTC
  - Meet:  https://meet.sigsum.org/ST-weekly
  - Pad:   https://meet.sigsum.org/p/ST-20230118
  - Chair: ln5

## Agenda

  - Hello
  - Status rounds
  - Decisions
  - Next steps
  - Other

## Hello

  - ln5
  - Foxboron
  - Jens
  - kfreds

## Other

  - Certificates in JSON docs 

    - https://git.glasklar.is/system-transparency/core/stboot/-/issues/?sort=created_date&state=opened&milestone_title=Implement%20updated%20loading%20of%20host%20configuration&milestone_title=Trust%20policy&first_page_size=20

    - trust_policy.json -- https://git.glasklar.is/system-transparency/core/stboot/-/issues/88
      - contains: Version (of this file), valid signature threshold, fetching mechanism (former "boot-mode")

    - host_config.json -- https://git.glasklar.is/system-transparency/core/stboot/-/issues/89

    - Proposal #1
	  - Use a traditional CA trust bundle bundle, and add it to initramfs somewhere (semi-)standard (like /etc/ssl?).
	  - Users can populate this as they wish and our default might be to use Mozilla's bundle (at the cost of space).
	  - Decision: Sounds good, Jens creates an issue.

    - Proposal #2
	  - Add an option for cert pinning, and add this to either `trust_policy.json` or `host_config.json`.
	  - Decision: Yes later, Jens creates an issue.

  - User feedback #2: PXE booting will become valuable
    - Postponing.

  - User feedback #3
  
    - We're thinking of kexecing into a new kernel, for upgrading it,
      from our running system. Would this conflict with any basic
      premises of ST? And how would we verify an ospkg, practically?

    - Postponing.
