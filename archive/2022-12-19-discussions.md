Discussions at the end of todays meeting (2022-12-19).

Present: Fredrik, Morten, Linus

## An stprov architecture question
* Morten: stprov is an OS package that mostly does what mpt does now.

* Linus: We need to document very clearly "the efivars interface", and
  perhaps in a contrib/ directory provide som PoC code (go, bash+jq,
  more?)

## What do you all think Fredrik should do?

* Make sure that Mullvad gets something out of ST. If they are not
  interested it's not going to work. Keeping Mullvad in sync. (short
  term it's LN's job, but long term it's FS's job)

* External monitoring: What other people and organisations are doing
  with ST-related technology, technology that could make ST irrelevant
  or more useful, keeping in sync with the broader ecosystem. Example:
  What mjg wrote about VMM's recently.

* Ensuring that STAM actually works in practice and solves actual
  problems. Is STAM capable of solving real-world issues? Which? How?
  Getting a draft ready and published, and get feedback, is a very
  important part of that.

* Pin down two dates in the coming year where we can give the very
  high level of what this is good for. Basically a roadmap
  for 2023. E.g. my friend who is tinkering with stuff, "I'll ping you
  March 23" when "make install" works.

## Website uncertainties

* AP Fredrik: Send an email to Jens, Philipp and Kai saying we will
  move the web site to our systems.

* AP Fredrik: Sorting out the "Who" in the footer.

	* Background discussion: Can we remove 9e and Mullvad from the
      "Who" part of the footer on the web site? Maybe not until we
      have a "history" section mentioning them?
