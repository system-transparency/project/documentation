# System Transparency weekly

- Date: 2023-12-18 1200-1250 UTC
- Meet: https://meet.sigsum.org/ST-weekly
- Chair: kfreds
- Secretary: zaolin

## Agenda

- Hello
- Status rounds
- Decisions
- Next steps
- Other

## Hello

- zaolin
- rgdd
- kfreds
- Jens
- Kai
- Vistor

## Status rounds

- zaolin: Aside meetup, looked into new UKI signing code from Morten and tried
  to debug PoC implementation. Worked on the UKI signing proposal which is now
  publicly avaiable
- rgdd: pass (just meet-up last week)
- kai: meetup obv, I worked on getting stprov and stboot running in the gitlab
  ci. Also integrated stauth (partially) into stprov.
- Vistor: Working on ST at Mullvad.
- kfreds: Meetup last week. I spent the weekend reconsidering how to move
  forward - roadmap, who works on what, why, etc.
- jens: meetup

## Decisions

- We will revisit the ST roadmap at the 8th of January 2024.

## Next steps

- kai: implement all test cases (3) and make the test suite usablable from other
  projects.
- zaolin: Getting server hardware to deploy ST for the getting the initial
  "getting started tutorial" written down. UKI signing feedback integration.
- rgdd: none right now
- Jens: Working on remaining milestones tasks, especially documentation related.

## Other

- discussing roadmap
- No full roadmap planning today. Retrospective and review about the previous
  sprint. Also preparing roadmap session for next week.
- Jens idea for going forward with roadmapping: 1.) Jens wrote an overview of
  the current state of milestones: https://pad.sigsum.org/p/2023-12-18-roadmap
  2.) Discuss next steps (group meeting / 1on1s / async) 3.) Add these to that
  doc 4.) Prepare it for decision
- How can I force a reboot of stboot into provisioning mode?

Top of mind for Vistor:

- Up until recently docuemntation was the most important thing.
- We have very few ways of verifying what works and and doesn't work.

1. Overwrite host config auto boot when re-provision is needed (force run
   stprov)
1. Documentation
1. Verifying that things work

- Is it verifying that I've signed the package twice?
