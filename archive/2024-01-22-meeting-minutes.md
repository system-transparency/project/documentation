# System Transparency weekly

  - Date:  2024-01-22 1200-1250 UTC
  - Meet:  https://meet.sigsum.org/ST-weekly
  - Chair: nisse
  - Secretary: nisse

## Agenda

  - Hello
  - Status rounds
  - Decisions
  - Next steps
  - Other

## Hello

  - jens
  - nisse
  - kai
  - rgdd
  - ln5

## Status rounds

  - jens: working on UKI / PE , chat with kai wrt alternative
  - nisse: Started writing stboot system docs, https://git.glasklar.is/system-transparency/core/stboot/-/merge_requests/137, but out sick most of last week, catching up.
  - rgdd: bumping up test coverage in stprov ci with qemu, in review
    - https://git.glasklar.is/system-transparency/core/stprov/-/merge_requests/36
  - rgdd: filed and talked to myself in a bunch of issues, e.g., some nits in host cfg documentation; trying to understand why we have templated URLs in stprov and id/auth things in stboot; why there's a timestamp sent from stprov local to stprov remote, etc.  The interest might find some interesting issues from ST's overall issue board:
    - https://git.glasklar.is/groups/system-transparency/-/issues
  - rgdd: merged/closed some stale MRs, cleaned up our abandoned milestones
  - kai: worked on simplifying initial network config for stprov. mostly done, needs testing though
  - ln5: working on automated build of ST images (aka ospkgs), for automated testing and for running glasklar VM's
  - philipp: building specialized stprov image with qemu testing based on bash + qemu + mkosi


## Decisions

  - None

## Next steps

  - jens: start writing down findings of possible ospkg format changes
  - rgdd: get stprov test coverage MR merged, move on to a few more minor fixes and ref docs; and building/testing of stprov on real hardware.  Maybe want to discuss that brefiely in the other section.
  - kai: write a converter networkd config -> host_configuration.json. writing ansible roles for provisioning stboot w/ philipp
  - ln5: finish automation of ospkg build and deploy; work with rgdd and nisse in person on testing and releasing
  - philipp: writes ans tests secure boot ansible provisioning role
  - nisse: cathing up, acting on feedback when it arrives (e.g., would like feedback on stboot doc)
    - question(jens): what docs to put where?
    - answer: think tool docs closer to the tool's repo, but wip as long as we still get the things rendered on docs.sto in one way or another

## Other

  - do we have a network or not? do we want to boot devices that are offline? etc. -> requirement documents is kinda missing, which makes processing of some issues "harder".
    - timestamp is just one example
    - "do we care about good time source or not" -> part of threat model
