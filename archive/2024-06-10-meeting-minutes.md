# System Transparency weekly

- Date: 2024-06-10 1200-1250 UTC
- Meet: https://meet.sigsum.org/ST-weekly
- Chair: nisse
- Secretary: rgdd

## Agenda

- Hello
- Status rounds
- Decisions
- Next steps
- Other

## Hello

- ln5
- kai
- rgdd
- jens
- zaolin
- nisse

## Status rounds

- nisse: Preliminary stboot refactoring, preparing for provision mode changes.
  - (need to reorg main function in stboot to make this easy, wip)
- rgdd: mainly review and user-support work
- kai: finished writing tests for the most critical parts and streamlined the
  TPM tests.
  - (Runs without the simulator, with recorded things. Can still run with the
    simulator ofc.)
  - Everything has been committed in a way that it looks planned and organized
- zaolin: finished reproducible builds, custom kernel builds and cleanup of
  scripts
  - (Everything is now reproducible with yocto.)
  - Question - what was fixed? It was hard to debug, but not hard to fix. Yocto
    does stripping with a GNU tool that doesn't work with golang binaries. We
    have to let Go strip the binaries instead. But TL;DR the issue was some ID
    in the binary.

## Decisions

- Decision:
  https://git.glasklar.is/system-transparency/project/documentation/-/merge_requests/58
  "add provisioning mode fallback proposal".
  - Basically: always enable if a provisioning OS package is included in the
    initramfs is the proposal.
  - Comment (not an objection): wonder if it is possible to start at the end of
    "making the first implementation less user-friendly and more safe by
    default". If that doesn't add to much work. But if it is equal amount of
    work, we could go from that end rather than going from the other end.
  - nisse: only option that would be easy to do right now is to have a flag to
    enable or disable the feature (say wipe).
  - nisse would prefer not to knob right now if there is a provision.zip, rgdd
    agrees but don't feel stronglyt about it. rgdd thinks its "default off by
    not putting a provision.zip". It's a kinda weak usecase and put a
    provizion.zip on it, and need a second image anyway to fix mistakes.
  - kai: the easiest thing first and get it out, then we can make it more
    complicated later. If you want to it "securely", doing it right is hard.
  - we seem to agree that moving forward with the proposal as is = reasonable

## Next steps

- kai: i'm finishing up documentation. Should be done by today or tomorrow.
  After this off for the summer.
  - you'll still have to read the code, but the docs will at least say more
    about why the things are the way they are etc
- zaolin: extensive documentation for this week
- zaolin: will also fix the darkmode thingy (or if Kai is done before he will
  take a look)
- rgdd: review, user support, releng with nisse probably
- nisse: provisioning thing, and try to resist refactoring stboot more than
  necessary
- jens: same as last week, reviewing my two tutorials and updating them to the
  first stable release of ST.

## Other

- kai & philipp will be traveling tue-sun and may have intermitted availability
