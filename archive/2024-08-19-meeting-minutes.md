# System Transparency weekly

  - Date:  2024-08-19 1200-1250 UTC
  - Meet:  https://meet.glasklar.is/ST-weekly
  - Chair: nisse
  - Secretary: ln5

## Agenda

  - Hello
  - Status rounds
  - Decisions
  - Next steps
  - Other

## Hello

  - rgdd (async, not here today)
  - nisse
  - ln5

## Status rounds

  - rgdd: wrapped up ST component releases (~mid july)
    - stmgr v0.4.1:
      https://lists.system-transparency.org/mailman3/hyperkitty/list/st-announce@lists.system-transparency.org/thread/KGKCD6AAY5BUI6G5JHKDGOVFII723HTX/
    - stprov v0.3.9:
      https://lists.system-transparency.org/mailman3/hyperkitty/list/st-announce@lists.system-transparency.org/thread/HHF2WTBVVNDN3K3BRJZPW4MM6EGX6BAO/
    - stboot v0.4.3:
      https://lists.system-transparency.org/mailman3/hyperkitty/list/st-announce@lists.system-transparency.org/thread/PJ6JVUQADKD3V6WVWBCORQJSJWS2DRMX/
    - (We expect these versions to be part of the st-1.1.0 collection release.)
    - It is TODO to wrap up docs componenet release + do the collection release
  - nisse: Back from vacation.
    - added version info to stboot, MR
      https://git.glasklar.is/system-transparency/core/stboot/-/merge_requests/226. Use
      go module version if available, fall back to git version, needs
      further work for release tarball builds.

## Decisions

  - None.

## Next steps

  - nisse: Collection release, with help from
    ln5. https://git.glasklar.is/system-transparency/project/docs/-/issues/61

## Other

  - rgdd: more GL runners would be very helpful. The current runner
    situation is especially noticable when doing release testing and
    having to, e.g., make one edit to stboot and get it merged,
    followed by tagging and getting another MR merged in stprov.
    We're talking ~30m to make a trivial change.  A few of those and
    1/2 of the day is gone.
    - As a short-term fix rgdd disabled "pipeline must succeed"
      checks, so that it is possible to override the waiting time and
      merge early when that makes sense.
    - setting up of new runners are being tracked in
      https://git.glasklar.is/glasklar/services/gitlab/-/issues/78
