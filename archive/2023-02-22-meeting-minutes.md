# System Transparency weekly

  - Date:  2023-02-22 1200-1250 UTC
  - Meet:  https://meet.sigsum.org/ST-weekly
  - Pad:   https://meet.sigsum.org/p/ST-20230222
  - Chair: ln5

## Agenda

  - Hello
  - Status rounds
  - Decisions
  - Next steps
  - Other

## Hello

  - kfreds
  - ln5
  - jens
  - zaolin
  - Foxboron

## Status rounds

  - kfreds: Nothing to report. I need to leave today at 13:15.
  - ln5: MPT/stprov released.
  - ln5: Resurrected stime from before some infrastructure work and got zaolin hooked up to it.
  - zaolin: Nothing to report.
  - jens: stboot tooling to unblock zaolin
  - Foxboron: mkiso changes, after discussions IRL last week
  - Foxboron: More network code testing.

## Decisions

  - None.

## Next steps

  - kfreds: I want us to finalize the roadmap and any other big planning we need to do, so we can move on. If anyone has more feedback on the roadmap document, please type it up. I intend to work on the roadmap and related things this week. When ln5 and I have polished it a bit more we'll circle back with everyone and finalize it.
  - ln5: Ordering 2 x YubiHSM 2 FIPS devices, for UEFI shim key and more (Sigsum need this too).
  - ln5: Mirror our repos to GH.
  - ln5: Moving our Gitlab milestones up one notch, to the system-transparency group level.
  - jens: prepare tooling for stboot mkiso
  - Foxboron: Finishing mkiso subcommands and network code testing, prioritising the former.

## Other

  - zaolin: Kai will be available earlier than expected, details coming soon
  - ln5: Reminder that I'm not around tomorrow Thu (nor Fri), so let me know what you might need from me.
  - jens: vacation from March 6th to 10th
  - Road map discussions: https://pad.sigsum.org/p/kmTBOWVDunxkKZ33ajg5
