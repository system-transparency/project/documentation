# System Transparency weekly

    - Date:  2024-02-19 1200-1250 UTC
    - Meet:  https://meet.sigsum.org/ST-weekly
    - Chair: ln5
    - Secretary: nisse

## Agenda

    - Hello
    - Status rounds
    - Decisions
    - Next steps
    - Other

## Hello

    - ln5
    - rgdd
    - nisse
    - jens
    - kai
    - philipp

## Status rounds

    - kfreds:
        - I have planned prototyping work together with Jens, Kai and Philipp:
          - Jens is prototyping [ST coreboot support][] due to a user request. I'm omitting details here as I'm not sure of the user's privacy preferences.
          - Philipp is prototyping [OSPKG Kernel Module Signing][].
          - Kai is prototyping [Remote Attestation][].
          - Note that two of the prototypes represent features that are not currently on the core team's roadmap. All three prototypes will be presented at the ST meetup in mid-April. Whether to put it on the roadmap for the ST project proper is up to the core team - Linus, Rasmus and Niels.
    - rgdd: got the decided hostcfg changes merged with review from nisse
    - nisse:
        - systemd uefi stub embedded in stmgr (for creating UKIs), delete? See https://git.glasklar.is/system-transparency/core/stmgr/-/merge_requests/57, https://git.glasklar.is/system-transparency/core/stmgr/-/issues/49
        - reading up on how bonding (802.3ad) works
        - merged boot-from-disk integration test in stboot repo
    - kai: some cleanup of the previous ppg prototypes. writing documentation for the remote attestation protocol, see kfreds^
        - ppg: "platform plumbing group"
    - jens: workplanning with the plumbing team
    - philipp: wrote documentation, project outlines, planning and meetings with the pp-team
        - pp: "platform plumbing"

[ST coreboot support]: https://git.glasklar.is/system-transparency/platform-plumbing/st-platform-features/-/tree/main/coreboot-support?ref_type=heads
[OSPKG Kernel Module Signing]: https://git.glasklar.is/system-transparency/platform-plumbing/st-platform-features/-/tree/main/kernel-module-signing?ref_type=heads
[Remote Attestation]: https://git.glasklar.is/system-transparency/platform-plumbing/st-platform-features/-/tree/main/remote-attestation?ref_type=heads

## Decisions

## Next steps

    - nisse: final proposal for hostcfg network interfaces
    - rgdd: same next steps as last week, hopefully pick up the stprov milestone again.  And update hostcfg reference documentation.
    - jens: finalize ST coreboot support WP and start working on it
    - kai: continue writing
    - philipp: continue writing the project outlines, finishing OSPKG Kernel Module Signing
    - kfreds: busy with other things until Friday, when back will work on giving feedback to kai/philipp/jens
    - ln5: start writing/updating an/the ST tutorial

## Other

### UEFI stub embedding? /nielsm

    - systemd uefi stub embedded in stmgr (for creating UKIs), delete?
        - currently: we have a stub copied from systemd embedded in stmgr binary. It is used to create UKIs. Nice thing: stmgr always works out of the box. Bad: if you use an old stmgr, you might get obsolete code running on your servers without you even being aware of it.
        - q: can we just delete that, or what do we expect to break? Maybe some of our tests will need to install the package that includes the stub?
        - that one has to install dependencies is how it usually works, linus thinks it would be ok as long as we can ensure users can learn about it somehow
        - kai: thinks the project should provide a golden stub, it's an important part of stboot. The stub is important. Sure, maybe outdated. But as long as not security relevant it's fine.
        - jens: three options, commented in the issue already
        - niels: need further discussion. include the stub but not use by default?
        - kai: policy on other imports? bring your own kernel?
        - ln: we haven't done any binary releases yet. until we have that we should let people drag in the stuff that we didn't write ourselves. as soon as we start doing binary stuff, a reasonable way is to package it for the platform you use it on. people can follow their normal update procedure to get updates. it's appealing to vendor, but probably not right. we can't decide when our users update stmgr etc.
        - kai: in your tutorial: "build kernel, package stboot, etc". the same path should be taken for the stub - "where does the kernel, initramfs, systemd-stub, etc come from"
        - ln: do we want to take responsibility to keep things up-to-date? that's what we should do if we vendor. I don't think it's uncommon for a project to be in-between releasing source and releasing binaries.
        - niels: if we provide the stub we need to think about how we get it out of the "deb pkg", "rpm package" etc
        - kfreds: another way to think about it -- the entire uki is measured by the tpm. UEFI measures the entire UKI, which includes systemd stub. The role of systemd stub is effectively only to be an rt0 for the kernel. And perhaps some security critical things kai and philipp would know about. But the entire thing is not only measured by the tpm; it is also signed by UEFI SB keys. Then systemd stub disapears. So its verified, measured, and lives for a short amount of time and has a limited scope. So that's something to keep in mind.
        - ln5: one way forward -- keep it and fix the bug. Fewer changes for this initial release. Then we can have more time to think about if we want to distribute binaries like this or not. When we come to that later point, maybe we already have other machinery for publishing binaries.  Which we might need anyway. So basically what rgdd said in the initial issue, fix the obvious bug (which we already did) and then keep as is for now.
        - niels: jens suggestion for consistency when stmgr is invoked in the same ways -- seems to make sense. That we should probably go back and fix to. **Nisse will make an MR for this change**.

---

### RA docu /kai

    - remote attestation documentation form
        - Question to fredrik: "def what to understand the protocol". What kind of documentation is needed for that?
        - At this stage, fredrik would like to understand the protocol in more detail. And how the cryptography involved documented, so that it can be incremented on. Presentation material, that we can leave until the last two weeks before the meetup.
        - PDF, text, markdown...doesn't matter that much but put it in a PP repo.  Asked for the protocol documented in more detail so that fredrik and kai has a lot of details that they can discuss and agree on.
        - kai: latex and generate pdf from that, ok? Yes.
        - (markdown is annoying when needing to document cryptography and diagrams)
        - kfreds: document it to the level of technical detail that it makes sense to keep in markdown, if you need to use latex to express it -> then it is too much technical detail.
        - so: no, don't latex at this point in time.
