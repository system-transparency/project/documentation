# Proposal on how we do releases

This document proposes how System Transparency could do releases.  The goal is
to define something that is **imperfect but good enough to start with**.  In
other words, it is expected that we will gradually improve this over time.

## Proposal

### Source-code repositories

We do per-repository releases.  A release is simply a git-tag that comes with a
few simple pointers on what we will do when creating it, such as ensuring that
the repository contains RELEASES and NEWS files before making a final tag.  As
soon as a new release is ready, an email is sent to an ST mailing list.  This is
the core of the proposal, and the baseline for ST repositories that we release.

For a detailed description of this baseline, see the following Sigsum proposal:

  - https://git.glasklar.is/sigsum/project/documentation/-/blob/main/proposals/2023-09-on-releases.md#proposal-in-general

To also see an example of how this turned out for the log-go repository, see:

  - https://git.glasklar.is/sigsum/core/log-go/-/blob/main/RELEASES.md
  - https://git.glasklar.is/sigsum/core/log-go/-/blob/main/doc/release-checklists.md
  - https://git.glasklar.is/sigsum/core/log-go/-/blob/main/NEWS
  - https://lists.sigsum.org/mailman3/hyperkitty/list/sigsum-general@lists.sigsum.org/thread/P5K75YKSCNLWSZ3OODKFUDD7L56VUOXO/

This proposal further suggests that we use the same expectation of no guaranteed
backwards-compatibility, but that the upgrade path from release X to X+1 is
well-documented.  If we can set the bar higher in one or more repositories now
or later, that is fantastic but not required if it complicates getting started.

[I wanted to call this out explicitly to ensure no-one misses this part.]

In addition to creating a git-tag, we will also put the corresponding source
code on the project's GitLab release page as an (unsigned) zip, tar, or similar.

Note: adding a release page is an **extension** to the documents linked above.

### Reference repository (docs.system-transparency.org)

We plan to follow the same general release procedure for this.  But to clarify
the expectation when doing such a release: how-to:s, reference specs, etc.,
should be up-to-date and consistent.  Use tags of released software components.

If a component in, e.g., a how-to guide is not released, pin it by commit ID.
We need to ensure that we have an overview page of which tags/commits are used.

For instructions on how to upgrade from an earlier release, we can simply refer
to each repository's RELEASES and NEWS files that document their upgrade paths.

The documentation tag is semantically versioned.  The definition of this is to
be determined by a documentation maintainer; it is not in the proposal's scope.

We only render released tags on our website.  This means we can safely merge to
main without making the new entrypoint to ST inconsistent and hard-to-use.

When typing docs.system-transparency.org, the user will be redirected to
docs.system-transparency.org/LATEST-RELEASE-TAG, i.e., the latest release.

## Comparison to what we're doing today

There's no documented process.

core/stboot appears to publish the repository's source code for some tags on a
[GitLab release
page](https://git.glasklar.is/system-transparency/core/stboot/-/releases).

core/system-transparency also appears to publish the repository's source code
for some tags on a [GitLab release
page](https://git.glasklar.is/system-transparency/core/system-transparency/-/releases).
The most recent release page additionally includes an SSH-signature from ln5.

It is unknown if there has been any standalone releases of something else.

The only regression that this proposal brings compared to the above is that
there's no release signing.  We can define and add that at a later stage.

## Future improvements

Some have already been listed in the linked Sigsum proposal.  If you would like
to move any (un)listed improvement forward, please champion a proposal.  We try
to keep this proposal down in complexity just to not be stuck without a process.

A likely future improvement is to package "all of ST" based on releases of the
documentation, which will per the above definition know which components work.

Another likely future improvement is to somehow sign the release.

### What happens next

To get started, we prepare to make releases of stprov (rgdd), stboot (jens), and
stmgr (jens).

We will also make a release of the documentation repository ($TBD).  All of this
involves creating separate RELEASES and NEWS files and eventually tagging.

## Notes

The idea of per-repository releases is to reduce the complexity of releasing,
i.e., we don't have to put everything on the same release cycle and halt all
repositories and docs at the same time; and we can even have different
expectations for different repositories depending on how mature they are.

We also expect that it will become easier to get a complete ST bundle if it is
more clear that a repository has released something and against what specs.  And
if we don't provide such a bundle or not even a documentation tag, early
adopters may pick-and-choose from released repositories if it suits their needs.

Here's a basic example of how the lifecycle of our repos could look like:

  - HEAD moves forward in docs repo, updating host config
  - stprov picks up this change, makes a release.  Pins either commit-id or a
    work-in-progress tag in the documentation repo for specs.
  - HEAD moves forward in docs repo, updating trust policy
  - stprov makes some UX improvements, makes another release.  Pins either
    commit-id or a work-in-progress tag in the documentation repo for specs.
  - stboot picks up hostconfig and trust policy changes, makes a release.  Pins
    either commit-id or a work-in-progress tag in the documentation repo for
    specs.
  - HEAD moves forward in docs repo, makes all how-to:s etc consistent
  - docs repo is released, now the latest tag is rendered on docs.sto

And a final note: there has been some discussion before about just having a
mono-repository.  This proposal is not meant to preclude that option, but if we
decide to go in that direction an updated release process would be expected.
