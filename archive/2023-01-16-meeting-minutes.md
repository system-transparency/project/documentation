# System Transparency weekly

  - Date:  2023-01-16 1230-1320 UTC
  - Meet:  https://meet.sigsum.org/ST-weekly
  - Pad:   https://meet.sigsum.org/p/ST-20230116
  - Chair: ln5

## Agenda

  - Hello
  - Status rounds
  - Decisions
  - Next steps
  - Other

## Hello

  - Foxboron
  - ln5
  - Jens
  - Fredrik

## Status rounds

### Morten

  - Issues since last week, finishing up

### ln5

  - Channeling user questions, now in the Matrix chat room

### Jens

  - Abstracted OS fetching mechanism
  - Host config autodetect
  - Other issues from stboot rework

## Decisions

  - Decision: Move weekly meetings to Wednesdays 1200-1250 UTC.
  - Decision: Change ST golang module paths to system-transparency.org/stboot
    - Jens gives the details to ln5 who sets it up in the web server

## Next steps

  - Foxboron working on 2 issues
  - Jens working on
    - hostconfig changes
    - autodetect implementation
    - moving of security config -> trust policy
  - Tagging 0.0.0 as soons as module paths are fixed (see above)

## Other

### Release planning for both stboot and stprov

 - stboot mid Feb? we're still good wrt this, resulting in stboot-0.1
 - stprov mid March?

ln5 summary: As long as we're on track to do a release of stboot just
before the Feb meetup we're good.

### User feedback #1

  - Could stprov help with provisioning of credentials for fetching
    os-pkgs from provisioning servers?
    - stprov being the server-side payload that helps operators
      provision servers. Currently named MPT.
    - Some text about this at https://pad.sigsum.org/p/i0NWsCJ7ENj5TjYxh0eQ
    - Where do we keep feature requests? We should be using git.glasklar.is

  - User feedback #2: PXE booting will become valuable
    - Postponed, ln5 will bring it up later again.

  - User feedback #3: We're thinking of kexecing into a new kernel,
    for upgrading it, from our running system. Would this conflict
    with any basic premises of ST? And how would we verify an ospkg,
    practically?
    - Postponed, ln5 will bring it up later again.

  - Certificates in JSON config files
    - How do we put certificates into JSON?
    - Jens wants to put it in the host config directly. There will be
      more certificates for STAM. We need to think about how to handle
      them. PEM? PEM has newlines and JSON doesn't like newlines. See
      https://git.glasklar.is/system-transparency/core/stboot/-/issues/107
    - Base64 is the answer, see f.ex. RFC6962, discussion moved to the
      issue in gitlab

  - Next meeting
    - We will hold a first Wednesday weekly day after tomorrow, at 1200 UTC (1300 CET).
