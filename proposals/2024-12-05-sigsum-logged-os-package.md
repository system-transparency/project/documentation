# Proposal on sigsum-logged OS packages

Note: This proposal was superseded by [a more minimal
proposal](./2024-12-09-sigsum-logged-os-package-2.md).

## Motivation

To provide key-usage transparency for the keys used to sign OS
packages. Without a larger overhawl of the OS package format.

## Proposal

In stboot's `/etc/trust_policy` directory, add optional files
`ospkg_trust_policy`, with a policy file according to
https://git.glasklar.is/sigsum/core/sigsum-go/-/blob/main/doc/policy.md
and `ospkg_signers`, with one or more pubkeys in openssh format. When
these files are present, the file `ospkg_signing_root.pem` is
optional. If both files are present, stboot accepts OS packaged signed
using either method.

In the OS package descriptor (.json) file, add a field
`sigsum_proofs`, a list of strings, where each string contains a
version 2 sigsum proof,
https://git.glasklar.is/sigsum/core/sigsum-go/-/blob/main/doc/sigsum-proof.md.

When stboot validates an OS, it is sufficient that to either have a
minimum of `ospkg_signature_threshold` valid signatures rooted in the
`ospkg_signing_root.pem` certificate, or sigsum proofs valid according
to the `ospkg_trust_policy` for a minimum of
`ospkg_signature_threshold` distinct submitter keys from the
`ospkg_signers` file. Motivation: Requiring the Sigsum proofs to be
valid if they exist does not improve security, since our threat model
includes attackers modifying the OS package in transit, and then the
attacker could just delete the sigsum proofs to fall back to x509. The
way to enforce transparency is to delete x509 root cert from the trust
policy.

## Tool support

The stmgr tool needs to be extended to attach sigsum proofs to an OS
package descriptor, either by interacting directly with Sigsum
directly, or with an option to attach a proof created separately using
`sigsum-submit`.

## Open questions

Should we trust a mix of x509 and Sigsum signatures, e.g., if we have
`ospkg_signature_threshold = 2` and a single valid x509 signature and
a single valid sigsum proof?

The x509 certificates include an expiry date. Do we need any expiry
date or other time dependency when validating sigsum proofs?

Do we need any delegation of authority (aka certificates) from pubkeys
listed in `ospkg_signers` to the keys actually used for signing the
data submitted to Sigsum?
