# System Transparency weekly

  - Date:  2023-06-21 1200-1250 UTC
  - Meet:  https://meet.sigsum.org/ST-weekly
  - Pad:   https://meet.sigsum.org/p/ST-20230621
  - Chair: ln5

## Agenda

  - Hello
  - Status rounds
  - Decisions
  - Next steps
  - Other

## Hello

  - ln5
  - doctor-love
  - Kai
  - Foxboron
  - zaolin

## Status rounds

  - ln5
    - made a release, R.2. provisioning poc, using a rather quickly made up release process
      - https://git.glasklar.is/system-transparency/core/system-transparency/-/tree/ln5/releng/releng
    - worked on sthsm with Philipp
    - worked on keykeeper, the PKI provisioning system
  - Kai
    - stauth quotes, ie the loop closed; still to be merged but it's there
  - zaolin
    - sthsm code, testing and docu
    - preparing the shim for review
  - doctor-love
	- "Took ST for a spin"
	- Documented initial impressions
  - Foxboron
    - slow ospkg download

## Decisions

  - None.

## Next steps

  - ln5
    - not available thu+fri, working roughly half days next week
    - will be attending to mostly non ST things but am available
  - Kai
    - merge stauth stuff, and also the demo into system-transparency
      - integration tests, unit tests
    - want feedback from an ops perspective
  - zaolin
    - around 60%-70%, working shim review + sthsm and tooling, documentation
  - doctor-love
	  - None ST work planned
  - Foxboron
    - normal week
    - wrapping up the slow ospkg dl
    - working on stauth feedback

## Other

  - doctor-love: Somewhat unstructured discussion about first impressions (https://pad.sigsum.org/p/st-docs-notes)
    - Regarding documentation
      - What is part of the vision and what does actually exist?
      - For documentation, try to separate *users* from *devs*
      - What are the demo's actually showing?
      - It's not clear where trust policy and hostconfig is being stored
        - Should an end user use an existing UKI with stboot and just provide hostconfig?
    - Reproducible stboot UKI helps with trusting a shim from f.ex. Glasklar
      - This requires remote attestation of the trust policy (assuming that the end user will want to bring their own); either disable SB or have stboot fetch the trust policy outside of the UKI
      - Do SB to some point, measure everything, do RA with the help from a remote server with more knowledge about the expectations of the system; You get more control but it requires more infrastructure
    - A hard-to-tamper-with storage medium hosting the trust policy would be valuable for someone with limited infrastructure resources, because one could use a shim signed by Glasklar

  - ln5 wants to *not* read all the specifications on remote attestation, and is now asking for other means of learning (RA and other interesting summer time topics)
    - there are two books on TPM's, they're more hands-on
    - google has a JavaScript implementation of TPM2, low level and good for playing with the API's
      - https://google.github.io/tpm-js/
    - for crypto, there are coursera courses which you do not have to really do properly, but can just use as an audio book
  - ln5 will make sure that doctor-love get information about the august meetup in stockholm, for social interaction and the open mic session


### kfreds recommendations for videos on remote attestation and related things

Note that the lists below are in reverse order of publication date, more or less. The further down you get the more out of date things might be.

    - Usama Sardar: Specification and Formal Analysis of Attestation Mechanisms in Confidential Computing
      - https://www.youtube.com/watch?v=hqkXtGUpLUk
    - Systemization of Knowledge: Attestation in Confidential Computing
      - https://www.youtube.com/watch?v=pJXvKvJVHt8
    - Making remote attestation meaningful for external reviews by Conrad Grobler (Google) | OC3 2023
      - https://www.youtube.com/watch?v=1wZczK4X-VI
    - Using DICE Attestation for SEV and SNP Hardware Rooted Attestation - Peter Gonda
      - https://www.youtube.com/watch?v=SitfZLoEFww
    - OCP Attestation using SPDM and DICE
      - https://www.youtube.com/watch?v=qO2BrMZZy2Y
    - AMD SEV-SNP Attestation: Establishing Trust in Guests - Jeremy Powell, Advanced Micro Devices
      - https://www.youtube.com/watch?v=7R0rnQfi86I
    - Remote Attestation Procedures Architecture
      - https://www.youtube.com/watch?v=j30PhEWKgG4

#### Videos I haven't watched yet but intend to

Device Attestation in Hardware TEE based Confidential Computing - Jiewen Yao & Jun Nakajima, Intel
https://youtu.be/MJmfkd-8UoA

Understanding Trust Assumptions for Attestation in Confidential Computing
https://www.youtube.com/watch?v=iXICWegSeVQ

MicroOS Remote Attestation with TPM and Keylime
https://www.youtube.com/watch?v=6F2mxG4YRKg

Device Mapper Target Measurements for Remote Attestation using IMA
https://www.youtube.com/watch?v=YRurY9NBzAY

ARES 2021 - Remote Attestation Extended to the Analog Domain
https://www.youtube.com/watch?v=3xr6n-4gZCE

OCP 2020 Tech Week: Measurement & Attestation
https://www.youtube.com/watch?v=PrkCQTEhb7Y

OCB: Introduction to Keylime - Axel Simon and Luke Hinds (Red Hat)
https://www.youtube.com/watch?v=oFLO8U9VFoI

Firmware Integrity Measurements and Attestation
https://www.youtube.com/watch?v=wCI6YYLdJm4

Heads OEM device ownership/reownership : A tamper evident approach to remote integrity attestation
https://www.youtube.com/watch?v=oline3C-W1g

USENIX Security '20 - APEX: A Verified Architecture for Proofs of Execution on Remote Devices...
https://www.youtube.com/watch?v=1R2tyNRRR0I

Analyzing AMD SEV's Remote Attestation | Robert Buhren | Hardwear.io Webinar
https://www.youtube.com/watch?v=7AIwe3YbaK8

Keylime - An Open Source TPM Project for Remote Trust of IoT - Luke Hinds, Red Hat
https://www.youtube.com/watch?v=jtbWnod5hoY

Microchip Secure Boot using the ATECC608A
https://www.youtube.com/watch?v=XI-HWZu0E3U

"TPM based attestation - how can we use it for good?" - Matthew Garrett (LCA 2020)
https://www.youtube.com/watch?v=FobfM9S9xSI

LPC2019 - What does remote attestation buy you?
https://www.youtube.com/watch?v=c-e7dm2aGBg

Roots of Trust and Attestation | Trammell Hudson | Keynote | hardwear.io Netherlands 2019
https://www.youtube.com/watch?v=lt8YKm9CQ34

Using TPMs to Cryptographically Verify Devices at Scale - Matthew Garrett & Tom D'Netto, Google
https://www.youtube.com/watch?v=EmEymlA5Q5Q

Tutorial: Complete Platform Attestation: Remotely Verifying the... Monty Wiseman & Avani Dave
https://www.youtube.com/watch?v=571FQxYk9xs

USENIX Security '19 - VRASED: A Verified Hardware/Software Co-Design for Remote Attestation
https://www.youtube.com/watch?v=T4XpkIPAKas

COSIC Course 2019 - Trusted Computing, Jan Tobias Muehlberg (DistriNet, KU Leuven)
https://www.youtube.com/watch?v=sTt5YzJx35o

A Simple Protocol for Remote Attestation of System Integrity - Roberto Sassu
https://www.youtube.com/watch?v=7JtEbJ1FD0Q

A Canonical Event Log Structure for IMA - David Safford & Monty Wiseman, GE
https://www.youtube.com/watch?v=UxpS7NukBb0

COSIC seminar - Hardware-based Trusted Computing Architectures For Isolation And Attestation (Maene)
https://www.youtube.com/watch?v=-WWnjSisjFU


