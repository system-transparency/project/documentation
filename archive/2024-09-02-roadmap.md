# 2024-09-02

This document provides an overview of the current roadmap.

## Summary of changes

The previous roadmap (expressed as drafty priorities) was documented
[here](https://git.glasklar.is/system-transparency/project/documentation/-/blob/main/archive/2024-05-13-roadmap.md?ref_type=heads).

The overall progress can be summarized as: we took a stab at rolling out ST
component and collection releases.  The st-1.1.0 collection is stable.

  - [docs v0.3.2][]
  - [ST-announce: stboot-v0.4.3][], most notably includes the ability to
    interrupt the boot process and enter provision mode by pressing Ctrl-C.
  - [ST-announce: stprov-v0.3.9][]
  - [ST-announce: stmgr-v0.4.1][]
  - [ST-announce: st-1.1.0][]

[docs v0.3.2]: https://git.glasklar.is/system-transparency/project/docs/-/blob/v0.3.2/NEWS?ref_type=tags#L1-20
[ST-announce: stboot-v0.4.3]: https://lists.system-transparency.org/mailman3/hyperkitty/list/st-announce@lists.system-transparency.org/thread/PJ6JVUQADKD3V6WVWBCORQJSJWS2DRMX/
[ST-announce: stprov-v0.3.9]: https://lists.system-transparency.org/mailman3/hyperkitty/list/st-announce@lists.system-transparency.org/thread/HHF2WTBVVNDN3K3BRJZPW4MM6EGX6BAO/
[ST-announce: stmgr-v0.4.1]: https://lists.system-transparency.org/mailman3/hyperkitty/list/st-announce@lists.system-transparency.org/thread/KGKCD6AAY5BUI6G5JHKDGOVFII723HTX/
[ST-announce: st-1.1.0]: https://lists.system-transparency.org/mailman3/hyperkitty/list/st-announce@lists.system-transparency.org/thread/4UEL5NQN4MRGQQL663CDOM56C3KWZXVJ/

We also deployed a minimal <https://www.system-transparency.org> site.

Little or no progress is planned in the upcoming roadmap.  The pace in ST will
be picked up again towards the end of October, 2024.  Then the first priority
will be to do a maintenance and support release (st-1.2.0), followed by taking a
stab at a larger feature (TBD).  For further details and hints, see below.

## Roadmap

Other than bug fixing and support, no activities are planned by the ST core team
until 2024-10-14.  In the mean time, the platform plumbing team will continue to
explore new features that we may eventually add support for (RA, ST-VMM).

When updating the roadmap next time, we will consider which larger feature to
add first.  Which one to start with has not been decided yet.  A few candidates:

  - Remote attestation support (based on kai's work)
  - Integrate with Sigsum for OS package signing (based on Sigsum's work)
  - Provision and test procedures for Secure Boot (based on zaolin's work)

Other than making progress on a larger feature, the next roadmap will include as
its first priority a "maintenance and support release" (st-1.2.0).  A draft of
what smaller feature requests it will include is [already
available](https://git.glasklar.is/groups/system-transparency/-/milestones/24#tab-issues).
Backlogged feature requests that the ST team is currently not planning to
prioritize can be found
[here](https://git.glasklar.is/groups/system-transparency/-/issues/?sort=created_date&state=opened&label_name%5B%5D=feature-request&label_name%5B%5D=backlog&first_page_size=20).
Feedback on what is currently not being prioritized is most welcome!

## People

  - ln5, rgdd: responsible for keeping the ST project running, e.g., including
    taking the initiative to plan, ensuring that weekly meets happen, etc.
  - nisse, rgdd: development and maintenance
  - kfreds, kai, zaolin: platform plumbing

## Concrete milestones

Future ones may appear early in the [milestone tab][].  One is there already:

  - st-1.2.0: https://git.glasklar.is/groups/system-transparency/-/milestones/24#tab-issues

[milestone tab]: https://git.glasklar.is/groups/system-transparency/-/milestones
