# System Transparency weekly

- Date:  2024-11-25 1200-1250 UTC
- Meet:  https://meet.glasklar.is/ST-weekly
- Chair: nisse
- Secretary: zaolin

## Agenda

- Hello
- Status rounds
- Decisions
- Next steps
- Other

## Hello

- elias
- zaolin
- nisse
- ln5

## Status rounds

- nisse:
  - Updating some deps
  - st-1.2.0 features, e.g.,
      https://git.glasklar.is/system-transparency/core/stboot/-/issues/207#note_20981
- zaolin:
  - updated the [firecracker networking](./2024-11-25-firecracker-networking-evaluation.pdf) stack status accordingly
    (originally edited at https://docs.google.com/document/d/1Zc5l5Tl4VgodbWbO_-jpV8hCXuWCMVLgN-lXjb7rkXE/edit?usp=sharing)
  - firecracker gives low network throughput, due to using virtio-net
    and focus on security over performance
  - checked out kai's implementation/testing. Works so far as
    expected, I have some small nits to fix
  - Checked out the documentation, reviewed MR's and needed to update the docs.
- ln5:
  - close to get new runners for gitlab up today!
  - gitlab renovate is still WIP
- elias: Still starting out and reading tons of docs.
- kai: worked on guest remote attestation and finalized MR's
  - looked into Cloud Hypervisor as firecracker replacement, as an
    alternative to firecracker; a big project hosted by LF and
    probably used by most VPS providers who are not Amazon or Google

## Decisions

- none

## Next steps

- zaolin: except today, adding more docs/updates on code and maybe
  requests from the ST team to add more docs over the week(rgdd).
  ongoing here ->
  https://git.glasklar.is/system-transparency/platform-plumbing/st-platform-features/-/tree/main/virtual-machine-monitor
  - I can further checkout existing docs tomorrow onwards and see if
    there is still things missing but I think we are already in a good
    shape.
  - I also think it's not so super urgent since we still promised to
    offer additional time when needed (consulting, docs, and general
    help).
- Kai: working on cloud hv networking and finalization, documentation, MRs etc..
- ln5: will be commenting on kai's stvmm MR for stimages; from a quick
  glance it looks like it's mostly amending things and not changing
  much
- nisse: st-1.2 milestone work
- elias: getting more into the project and the MR of Kai

## Other

- nisse: Any summary of stvmm/firecracker network performance?
  - resolved see docs above
- nisse: Should we have a top-level Makefile in ST collection release
  archives? Depends on what's the purpose of having those archives,
  which appears a bit unclear and undocumented.
  - is the goal this?
    curl URL | verify-tar-ball-somehow | tar xf - && cd DIR && ./configure && make && make install
  - ln5: Support offline build? Adding documentation to the tarball
    for building st* is useful. Users shouldn't face new interfaces
    without think twice about it and discuss.
  - nisse: use cases for the tar file are missing (documentation
    somewhere).
- zaolin & kai will join next week weekly last time.
