# System Transparency weekly

- Date: 2024-01-15 1200-1250 UTC
- Meet: https://meet.sigsum.org/ST-weekly
- Chair: rgdd
- Secretary: rgdd

## Agenda

- Hello
- Status rounds
- Decisions
- Next steps
- Other

## Hello

- rgdd
- ln5
- kai
- philipp
- jens
- nisse
- quite

## Status rounds

- rgdd: some notes on go module versioning in case that helps someone else
  - https://git.glasklar.is/system-transparency/project/documentation/-/blob/main/archive/2024-01-15-notes-on-go-module-versioning.txt
- rgdd: added a draft milestone for what i'm gunning for in stprov v0.3.X
  - https://git.glasklar.is/groups/system-transparency/-/milestones/20#tab-issues
  - input welcome, starting to take a stab on this later today
- rgdd, nisse, ln5: notes on what we're planning to work towards in the near future
  - https://git.glasklar.is/system-transparency/project/documentation/-/blob/main/archive/2024-01-15-notes-on-near-term-focus.md
- nisse: Purged all use of go GO111MODULE=off in system-transparency repo.
  Wrapping up ssh agent things this week, then looking at stboot related docs.
- kai: we finally decided what route to go at the ppg (after a week of meets -
  tl;dr: better stprov and next os package)
- jens: nothing to report, see kai
- philipp: see kai, and referenced the uki proposal as requested
- ln5: worked last week with rgdd and ln5, see above
- quite: nothing to report

## Decisions

- None

## Next steps

- rgdd: stprov milestone, maybe some other ref docs; let me know if you need
  anything from me to make progress on your things.
- nisse: not much st this week
- kai, philipp: working on a better stprov
- jens: working on OS package format
- ln5: basic ospkg building

## Other

- None
