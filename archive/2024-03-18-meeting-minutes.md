# System Transparency weekly

- Date: 2024-03-18 1200-1250 UTC (1300-1350 CET)
- Meet: https://meet.sigsum.org/ST-weekly
- Chair: ln5
- Secretary: rgdd

## Agenda

- Hello
- Status rounds
- Decisions
- Next steps
- Other

## Hello

- zaolin
- ln5
- jens
- rgdd
- nisse
- kfreds
- kai (async report, not present in the meet)

## Status rounds

- zaolin: Could only work 3 days in total. Stopped finishing OSPKG loading with
  mkosi setup. Todo is still network loading for OSPKG execution. Golang code
  for OSPKG execution is ready to be tested. Build OSPKG with new stimages repo,
  seems to work fine (on gentoo laptop).
- Kai: I finished the low-level TPM \<-> host interface. I cleaned up the code I
  wrote last week, documented every function and structure. I started working on
  code to retrieve and validate the Endorsement Key certificate that is later
  used to identify the TPM as well as write code to do a quick TPM sanitiy check
  like minimum spec version implemented and known CVEs.
  - kfreds: just as a reminder, philipp and kai are doing prototypes that are
    testing different things. The thing philipp is doing, the general context
    is: question -- what if we took a systemd fanboi approach to all st
    components. Kai is working on RA, and what kfreds gaterhered so far from his
    work is.. he wanted to use go tpm lib, but then realized its in an
    intermediate state. They're moving things from one architecture to another,
    so it's in the middle. Based on how he interpretted kfreds' wishes, he
    figured he could make something smaller. Kai's approach only uses the
    minimum, so should be more easily understood. So that's roughly what the
    general context is.
- jens: Linuxboot (Kernel + u-root core) is running on HiFive unmatched board ->
  general bootchain example working. Maybe kexec needs some attention with the
  RISC-V kernel.
  - https://git.glasklar.is/system-transparency/platform-plumbing/st-platform-features/-/tree/main/coreboot-support
  - EFI and qemu examples can already be played around with. Havn't committed
    HiFive board stuff yet, it's coming. Just want to do a bit of clean up
    first, and then will put together a detailed description.
  - nisse: would be nice to have this running in our ci in the not too distant
    future, at least for x86.
  - kfreds: happen to have 2 HiFive boards -- is that something linus want to
    rack in stockholm?
  - ln5: yes, would be happy to rack them
- ln5: Release planning, goal is to release ST-1.0.0 before the April meetup.
- rgdd: sync and plan a bit in-person with nisse and ln5. We have a (soft) goal
  of making the status quo releases (AKA
  ["the first round of priorities"](https://git.glasklar.is/system-transparency/project/documentation/-/blob/main/archive/2024-01-15-notes-on-near-term-focus.md?ref_type=heads#first-round-of-priorities-and-hence-the-most-detailed))
  happen until the april meet up. If anyone want to test the current software
  early, we are not expecting any major code changes from these intermediate
  tags:
  - stmgr @v0.3.0
  - stprov @v0.3.1
  - stboot @v0.3.4, see NEWS file for drafty changes:
    https://git.glasklar.is/system-transparency/core/stboot/-/blob/main/NEWS?ref_type=heads#L1-73
- rgdd: misc smaller MRs and review + help testing and rubberducking
- nisse: Fixed stboot network interface logic
- nisse: Reworking key handling in stmgr, to support ssh-agent.
- nisse: MR for boot-from-network integration test.
  - https://git.glasklar.is/system-transparency/core/stboot/-/merge_requests/172
- kfreds: Nothing to report.

## Decisions

- Decision: cancel the meeting after the next, on April 1, because it will not
  have many Swedes around due to public holiday.
  - zaolin: me neither, see vacation.
  - jens: public holiday in Germany too
  - rgdd: prefers to cancel
  - nisse: likely not here tue/wed either
  - ln5: also thinks it makes sense to cancel, nothing prevents an adhoc meeting
    between anyone that's here. Sync between yourself as you see fit.
  - kfreds: cancel sounds good.

## Next steps

- zaolin: Finishing the test OSPKG setup with mkosi. Integration tests with
  OSPKG execution code. Writing documentation about components to be replaced.
- Kai: This week I'll continue with that high level code and start implementing
  the enrollment phase of the Remote Attestation protocol.
- ln5: Tend to whatever trouble with qa-images nisse and rgdd might have.
- jens: On the HiFive board replace u-root core initrd with ST initrd and see
  how far it boots. Check on kexec quirks on RISC-V
- rgdd: https://git.glasklar.is/system-transparency/core/stprov/-/issues/54 and
  probably the same for setting multiple prov URLs. And get on cleaning up
  docs.sto things, see
  https://git.glasklar.is/system-transparency/project/docs/-/issues/40. If time
  permits, final things in stprov milestone, see
  https://git.glasklar.is/groups/system-transparency/-/milestones/20#tab-issues.
- kfreds: pass, but let me know if anything is urgent and needs my attention

## Other

- Note that many European countries enter daylight saving time (DST) on March
  31\. This meeting will then happen at 1400 CEST.
- Random note: Playing with GrapheneOS on a Pixel8 on my free time, having fun
  with the remote attestation. Will set up building all the things, to
  contribute to reprobuilds. /ln5
- rgdd -- want to chat a few minutes with nisse in a breakout
  - https://git.glasklar.is/system-transparency/core/stboot/-/merge_requests/172#note_14029
  - i'll join, since i think it might be related to qa-images /ln5
  - (see TL;DR notes in the linked comment from this short breakout session.)
