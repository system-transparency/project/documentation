# System Transparency weekly

  - Date:  2024-09-02 1200-1250 UTC
  - Meet:  https://meet.glasklar.is/ST-weekly
  - Chair: ln5
  - Secretary: ln5
## Agenda

  - Hello
  - Status rounds
  - Decisions
  - Next steps
  - Other

## Hello

  - ln5
  - rgdd (async -- not present due to OSFC travel)
  - eshant (first five minutes)
  - zaolin from :17 to :22

## Status rounds

  - nisse, rgdd: wrapped up st-1.1.0 collection release
    - https://lists.system-transparency.org/mailman3/hyperkitty/list/st-announce@lists.system-transparency.org/thread/4UEL5NQN4MRGQQL663CDOM56C3KWZXVJ/
  - kfreds, rgdd: deployed first version of a minimal www.sto website
    - https://www.system-transparency.org/
    - https://git.glasklar.is/system-transparency/project/www
    - issues/MRs to make gradual improvements are most welcome!
  - rgdd: closed the old milestone with things that didn't change interfaces
    - (it was 60% complete, and did not reflect near-term next steps anymore)
    - https://git.glasklar.is/groups/system-transparency/-/milestones/8#tab-issues
  - rgdd: created issues and a new draft milestone named "st-1.2.0: maintenence and support".
    - we won't be working on this until after the transparency dev summit (mid oct)
    - missing things? let us know -- ms will be a draft until we're getting started
    - https://git.glasklar.is/groups/system-transparency/-/milestones/24#tab-issues
  - rgdd: suggested a roadmap based on in-person discussions, see below
  - zaolin: last week we decided that i do cross-dogfooding with kai -- zaolin will be trying out kai's
    RA documentation and implementation while kai is doing the same with zaolin's systemd stboot

## Decisions

  - Decision: Adopt the upcoming ST roadmap.
    - https://git.glasklar.is/system-transparency/project/documentation/-/merge_requests/62
    - Next roadmap update: end of October, 2024.
    - rgdd and nisse have both given it a +1
    - https://git.glasklar.is/system-transparency/project/documentation/-/blob/main/archive/2024-09-02-roadmap.md?ref_type=heads

## Next steps

  - rgdd: attend OSFC

## Other

  - eshant will come back next week and talk to more people, when we're all back from OSFC
