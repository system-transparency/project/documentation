# System Transparency weekly

- Date: 2024-01-08 1200-1250 UTC
- Meet: https://meet.sigsum.org/ST-weekly
- Chair: kfreds
- Secretary: rgdd

## Agenda

- Hello
- Status rounds
- Decisions
- Next steps
- Other

## Hello

- kfreds
- rgdd
- ln5
- jens
- kai
- nisse

## Status rounds

- rgdd: Nothing to report.
- nisse: got stboot init things merged last week, working on further cleanups
- jens: I followed niels work with interest and made some comments
- ln5: Nothing to report.
- kai: I'm not even sure if I'm supposed to be here. I've been doing research on
  things that I didn't have time for since last time.
- philipp: uefi research with kai
- kfreds: Nothing to report.

## Decisions

- None

## Next steps

- rgdd, ln5 and nisse will work together on ST tue-thu

## Other

- None
