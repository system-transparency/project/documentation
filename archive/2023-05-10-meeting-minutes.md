# System Transparency weekly

  - Date:  2023-05-10 1200-1250 UTC
  - Meet:  https://meet.sigsum.org/ST-weekly
  - Pad:   https://meet.sigsum.org/p/ST-20230510
  - Chair: ln5

## Agenda

  - Hello
  - Status rounds
  - Decisions
  - Next steps
  - Other

## Hello

  - ln5
  - Philipp
  - Foxboron
  - Fredrik
  - Kai
  - Jens
  - Frederic

## Status rounds

  - Philipp
    - Working on STPKI bug fixing
  - Foxboron
	- stprov: initial porting from MPT, syncining with stboot changes
  - Kai
	- Finally pushed the first version of the [Remote Attestation explainer/event log walkthrough](https://git.glasklar.is/system-transparency/core/stauth/-/blob/main/remote-attestation.md)
  - kfreds: back from vacation
  - ln5: back from vacation
  - jens: stboot changes from stprov review; mkuki in stmgr; moved some more stuff from scripts into stmgr

## Decisions

  - <None

## Next steps

  - philipp: stpki work based on feedback from Linus
	- Split stpki tooling
	- Remove unneeded dependencies from provisioning part
	- Add more documentation
  - jens: make stboot ready for early stprov testing (upstream)
  - foxboron: stprov first public code
  - Kai: continue to work on the remote attestation docs. Still needs a intro and an explainer on how to validate event logs.
  - ln5: catch-up and publishing the road map properly, including turning parts of it into mailestones in gl (after pushing milestones one level up)

## Other

### Kai presenting https://git.glasklar.is/system-transparency/core/stauth/-/blob/main/remote-attestation.md

- [Intel SGX Explained](https://eprint.iacr.org/2016/086) is a good paper explaining a lot of background for someone who won't read the full x86 systems documentation

- Idea: Have a Q&A document for driving knowledge transfer from in particular kai & philipp to the ST project; linus will curate such a document

- Kai also explained what happens when SMM is disabled, and will write this up in a separate document

#### kfreds feedback for Kai

 - Polish the Markdown
 - Question: What is replacing the contents of PCR0 and how is it able to do that?
   - SMM, a separate execution mode on x86 cores
   - PPAM, STM?
 - Figure out what terminology to use (e.g. what is the difference in verified, measured, trusted and secure boot)
 - Create a separate section for prevalent marketing terminology likely to confuse
 - Regarding the event log: Great idea to show how different boot chains measure things.
 - As you mentioned, create a section for how stboot measures things at the moment. And separately, how it should measure things.
 - Missing a section on TPM provisioning / enrollment
 - I'd love a list of which TPM commands we'll use
