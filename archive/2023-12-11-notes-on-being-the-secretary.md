# Notes on what the weekly meet secretary does

What is rgdd currently doing as the weekly meet secretary?

1. Ensure there is a chair for the meet.
2. Take notes during the meeting.
3. If additional pads are linked in the meet pad, first persist those unless
   whoever linked them expressed that they should not be part of the minutes.
   Replace the volatile pad URLs with their new locations in the [archive][].
4. Persist the notes after the meeting by placing them in the [archive][].
5. Replace the content of today's meet pad with a URL to where they are now
   persisted, to avoid others from adding more things in there after the meet.
6. Prepare next week's meet pad.  Note: it is helpful to do this asap after the
   meet ended, as some people add things to the next pad throughout the week.
7. Link the persisted meeting minutes and the next meet pad on matrix.
8. On the morning of the next meet, link meet pad again on matrix.  If proposals
   are queued up, give a friendly reminder for everyone to take a look.
9. Consider linking the meet pad again right before the meet starts, especially
   if there has been lots of chatting so that it is no longer easily found.
10. Now we're kind of back to 1...

Note that there's a [template][] for the ST weekly meets that can be imported.

Name the weekly meet pads https://pad.sigsum.org/p/ST-YYYYMMD, just substituting
YYYYMMDD with the appropriate date.  This makes it much easier to find pads.

If you want to do less clicking when persisting pads, consider using a script.
Lately, rgdd has been using `mdpad-save` (homegrown, pasted below).  For it to
work, just check that all blocks of bullet items are indented in the same way.
Then position your terminal in the [archive][] directory.  Type `mdpad-save -u
st`.  Check that it looks reasonable (or make minor edits), then commit+push.

I.e., what `mdpad-save` does is pull today's meet pad, indent all of it
consistently, and then save the output in the same way as previous meet pads.

See the usage message below for a full introduction to the tool.  You will need
to install `curl` and `mdformat` for it to work (`pip install mdformat`).

[archive]: https://git.glasklar.is/system-transparency/project/documentation/-/tree/main/archive
[template]: https://git.glasklar.is/system-transparency/project/documentation/-/blob/main/archive/st-weekly-template.etherpad

## mdpad-save

```
#!/bin/bash

set -eu
function die() {
        echo "ERROR: $*" >&2
        exit 1
}

NAME=meeting-minutes
function usage() {
        cat << EOF
Usage: $(basename "$0") [opts] -u URL

A program that downloads Etherpad meeting minutes in markdown,
formatting them consistently and then saving as
YYYY-MM-DD-$NAME.md.  Today's date is assumed.

All options:

  -h  Print this usage message and exit
  -n  Set part of the output file name (Default: $NAME)
  -u  Target URL to download (Default: "").

The shorthands "sigsum" and "st" imply hardcoded URLs (-u).

Examples:

  $(basename "$0") -u st
  $(basename "$0") -u sigsum
  $(basename "$0") -u https://pad.sigsum.org/p/ab12-cd34
  $(basename "$0") -n my-notes -u https://pad.sigsum.org/p/ab12-cd34

EOF
}

function get_opts() {
        while getopts "hn:u:" opt; do
                case $opt in
                        h)
                                usage
                                exit 0
                                ;;
                        n)
                                NAME=$OPTARG
                                ;;
                        u)
                                URL=$OPTARG
                                ;;
                        \?)
                                die "invalid option: -$OPTARG"
                                ;;
                        :)
                                die "option -$OPTARG requires an argument"
                                ;;
                esac
        done

        shift $((OPTIND-1))
        [[ $# -eq 0 ]] || die "trailing arguments: $*"
}

###
# Main
###
get_opts "$@"

dst=$(date +%Y-%m-%d)-$NAME.md
case "$URL" in
        st)
                URL=https://pad.sigsum.org/p/ST-$(date +%Y%m%d)
                ;;
        sigsum)
                URL=https://pad.sigsum.org/p/sigsum-$(date +%Y%m%d)
                dst=$(date +%Y-%m-%d)--$NAME.md
                ;;
        http*)
                ;;
        *)
                echo "ERROR: option -u expects \"sigsum\", \"st\", or a complete URL" >&2
                exit 1
                ;;
esac

url=$URL/export/txt
echo "INFO: downloading $url" >&2
curl -Lo "$dst" "$url"

if grep -q '^<pre>Cannot GET.*</pre>$' "$dst"; then
        echo "ERROR: pad does not appear to exist" >&2
        rm -f "$dst"
        exit 1
fi

echo "INFO: making markdown consistent" >&2
mdformat --wrap 80 "$dst"

echo "INFO: created $dst" >&2
```
