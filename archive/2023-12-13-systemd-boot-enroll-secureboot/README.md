
These are experimental tools for creating keys and certificates
(public keys) for Secure Boot. Including creating an ISO containing
`systemd-boot` and the certificates, which is used to automatically
enroll when booted on a server.

For reference, the Arch Wiki on Secure Boot:
https://wiki.archlinux.org/title/Unified_Extensible_Firmware_Interface/Secure_Boot

Required packages: openssl xorriso efitools sbsigntool mtools dosfstools systemd

systemd version 252 or higher (which supports Secure Boot enroll) is
required because the script picks up the systemd-boot EFI binary from
down in /usr/lib/...

The included UEFI shell binary `./shellx64.efi` came from:

```
curl -LO https://github.com/pbatard/UEFI-Shell/releases/download/23H2/UEFI-Shell-2.2-23H2-RELEASE.iso
mkdir out
osirrox -indev UEFI-Shell-2.2-23H2-RELEASE.iso -extract / $(pwd)/out
```

## Generating keys

You run this once. The script currently creates keys which are valid
for a number of years. The `.key` files are a secret, they contain the
actual private *private keys*, which are used for signing (EFI)
binaries that should be allowed to boot.

Proceed to generate keys and certificates in `keys/` (if this dir does
not already exist):

```
./gen-keys
```

## Enrolling Secure Boot certificates on a remote server

Build the `build/secureboot-enroll.iso` to be used:

```
./build-iso
```

The following procedure was tried on a Supermicro X12STD-F server. The
procedure and especially the menus may differ on other models. We use
the IKVM Java-applet to access the server's console.

1.  The `build/secureboot-enroll.iso` ISO file which resides on your
    local workstation is first mounted in the remote server's BMC, and
    can then be booted from etc. Do this from the `Virtual Media` menu
    of the Java-applet.

2.  Boot the server.

3.  Hit `Delete` to enter Setup/Firmware menus.

4.  Navigate to `Security » Secure Boot` in the menus. Disable Secure
    Boot if enabled. Do `Reset to Setup Mode`.

5.  Hit `F4` to save and reboot. Let the server boot the ISO you have
    mounted.

6.  systemd-boot should eventually show a menu that contains `enroll
    Secure Boot Keys: ST`. Select it and hit 'Enter'. Sometimes takes
    a couple of reboots to get to this point? systemd-boot will reset
    the system after enrolling.

7.  Hit `Delete` to enter Setup/Firmware menus.

8.  Go to the `Security » Secure Boot` menu again. Enable Secure Boot.
    You might first have to disable `CSM Support`? You should probably
    also `Enter Deployed Mode`?

9.  Hit `F4` to save and reboot. Still with the same ISO mounted. If
    systemd-boot's menu comes up again then Secure Boot probably
    worked (systemd-boot was signed with our db.key).

10. The `enroll` item should no longer be in the menu. Select `EFI
    Shell` in systemd-boot's menu. In the EFI shell you can then run
    `dmpstore SecureBoot` and ensure that the single byte bool is
    "01".
