# On "Initramfs unpacking failed: uncompression error" problem

author: elias

Related to the issue "fetching OS package from initramfs
failed: open /ospkg/debian-package.json: no such file or directory":

https://git.glasklar.is/system-transparency/core/stboot/-/issues/224

This was a problem that appeared when testing stboot on the stime test
server.

stboot failed sometimes (roughly half of the attempts, seemed random)
with this error message:

```
[ERROR] fetching OS package from initramfs failed: open /ospkg/debian-package.json: no such file or directory
```

After further testing it turned out that each time that stboot error
appeared, there was already earlier the following kernel error message
that came earlier, before even attempting to start stboot:

```
[    4.962924] Initramfs unpacking failed: uncompression error
```

Since that error comes before stboot has started, it seems like stboot
itself is not to blame.

The setting where this problem happens is that we are booting the
stime test server by selecting the "virtual cdrom" boot option, where
the virtual cdrom comes from a .iso file made available to the test
server via its BMC. One possible explanation could be that there is
some bug related to the "virtual cdrom" functionality.

One possible way to investigate further would be to modify the kernel
to output more detailed info when unpacking the initramfs, such as
printing details about the size and checksum of the initramfs before
trying to unpack it. Then it would be interesting to see if the
initramfs has a different size or checksum than what is expected (it
should be the same every time, but perhaps it is not). That could help
sort out if the kernel is doing something wrong while unpacking the
initramfs, or if the initramfs contents are corrupt already before the
kernel tries to unpack it.

For the moment, just archiving these notes and closing the issue as it
does not seem to be a bug in stboot itself and the problem may be
related to the specific test server "stime" that was used in the
tests.
