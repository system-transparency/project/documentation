# Making it easier to run integration tests on subtool-changes

author: nisse

## Objective

This proposal aims to make it easier to run the system-transparency
integration tests on changes to sub-projects. Both locally, and as
part of the ci pipelines of sub-projects.

## Current state

As far as I understand, the easiest way to integratino test changes of
a tool is to:

1. Commit the changes to a branch, pushed to the repository.

2. Check out the "envelope repository" system-transparency.git, locate
   appropriate task .yml file and replace version of the tool under
   test with the commit hash of the above branch.
   
3. Commit that change on a system-transparency branch, and push to the
   system-transparency repository.
   
4. Watch ci automatically run tests on that branch.

Example:
https://git.glasklar.is/system-transparency/core/system-transparency/-/tree/nisse/test/stboot-branch-simplify-stlog?ref_type=heads

## Suggested improvements to explore

### Move versions out of the task files

Assume we can specify versions of sub-projects in a go.mod file in the
system-transparency, and delete all explicit versions in the task
files, e.g., deleting this `STBOOT_VERSION` variable:
https://git.glasklar.is/system-transparency/core/system-transparency/-/blob/main/tasks/go.yml#L9

Then dependencies can then be redirected using the go.work mechanism. This
would be beneficial both for local testing (create a go.work file, or
set $GOWORK to point to such a file under a different name) that
specifies a local directory possibly with local changes.

It could also be used in CI pipelines for subtools: A job can checkout
the system-transparency repo, create a go.work file to point to
itself, and run relevant tests. For an example of that trick, see:
https://git.glasklar.is/sigsum/core/sigsum-go/-/blob/main/.gitlab-ci.yml#L25

One potential complication is the u-root build: There's a reference to
u-root's source directory, referenced in a command like `.../u-root
... -uroot-source=...`, and an argument
`system-transparency.org/stboot`. The current way that is setup
depends on the deprecated `${GOPATH}/src` and a mix of go commands
with `GO111MODULE=off` and git commands. It may get simpler to just
use git (possibly in combination with git submodules).

For all but u-root and stboot, moving to go.mod seems
straight-forward, see
https://git.glasklar.is/system-transparency/core/system-transparency/-/merge_requests/234#83ce6329a418d0402aeb9b068cf12cf0a69f6cd2.

### Move logic out of .gitlab-ci

It should be easy to run all relevant tests locally prior to commit,
and it should also be easy to locally reproduce any failure detected
by the ci.

Having complex scripts in .gitlab-ci makes it unnecessarily cumbersome
to reproduce the ci tests locally. Ideally the "script" of each ci job
should be a simple invokation of a single script / task / make target
defined outside of gitlab.ci.
