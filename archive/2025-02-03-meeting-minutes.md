# System Transparency weekly

  - Date:  2025-02-03 1200-1250 UTC
  - Meet:  https://meet.glasklar.is/ST-weekly
  - Chair: rgdd
  - Secretary: rgdd

## Agenda

  - Hello
  - Status rounds
  - Decisions
  - Next steps
  - Other

## Hello

  - elias
  - rgdd
  - ln5
  - nisse

## Status rounds

  - elias: started stboot release testing for pre-release tag v0.5.1 - "Testing
    on real hardware" --> "Testing boot from local disk (.iso)" part works -
    https://git.glasklar.is/system-transparency/core/stboot/-/issues/219
    - version number didn't show up as expected via our test scripts, fixed now
    - https://git.glasklar.is/system-transparency/project/documentation/-/merge_requests/68
  - elias: there were many interesting talks at FOSDEM devroom "Image-Based
    Linux and Boot Integrity" -
    https://fosdem.org/2025/schedule/track/image-based-linux/
  - nisse: not much st last week, i started looking at the description thing in
    stprov. There's an MR that rgdd has been added as reviewer on.
  - rgdd, ln5: not much to report, roadmap proposal looks the same as last week.
    Hoping for decision today.

## Decisions

  - Decision: Adopt updated roadmap, renew towards end of May
    - https://git.glasklar.is/system-transparency/project/documentation/-/merge_requests/68
    - rgdd will update so that the summary of progress matches where we're at

## Next steps

  - elias: continue with release testing (help from nisse when needed),
    currently just stboot.
    - nisse: then we will do stprov. would like you to help/do that too. similar
      process
    - nisse: then stmgr, there it isn't as much release testing because its used
      by the other components
    - rgdd: docs is the "hardest" part because of the dependency on stimages,
      but there are notes about it
  - nisse: the releases, one at a time
  - ln5: will be working on a plan for st deploy at glasklar
  - rgdd: if time permits secureboot.

## Other

  - sunday morning -- many of these presentations relevant and related to what
    you're doing.
    - (public) page of different things we want to keep an eye on? And what
      could (or couldn't right now) be mapped to what we're doing?
    - on baking images: an important part is the input form
    - another part: how you do in practise when you run that system
    - composefs -- overlayfs and a bit more
    - someone should go to all systems go later this year, ln5 is thinking about
      it
