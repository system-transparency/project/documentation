# System Transparency weekly

- Date: 2024-08-26 1200-1250 UTC
- Meet: https://meet.glasklar.is/ST-weekly
- Chair: rgdd
- Secretary: rgdd

## Agenda

- Hello
- Status rounds
- Decisions
- Next steps
- Other

## Hello

- rgdd
- nisse

## Status rounds

- nisse: wip docs component release and collection release
- nisse: stboot now prints its (runtime.Debug) version if installed with go
  install or build from a git repo. But no version from a release tarball yet.
  The latter should probably be fixed with a Makefile that uses the manifest to
  explicitly set the right versions.
  - https://git.glasklar.is/system-transparency/core/stboot/-/issues/202

## Decisions

- None

## Next steps

- nisse, rgdd: meetup
- nisse, rgdd: finalize component release for docs, and st-1.1.0 collection
  release
- nisse, rgdd: open a few issues based on user feedback
- nisse, rgdd, et al: roadmapping
- rgdd, kfreds(?): ensure simple www.sto gets MRs merged and deployed

## Other

- None
