# System Transparency weekly

- Date: 2024-10-21 1200-1250 UTC
- Meet: https://meet.glasklar.is/ST-weekly
- Chair: rgdd
- Secretary: rgdd

## Agenda

- Hello
- Status rounds
- Decisions
- Next steps
- Other

## Hello

- rgdd
- nisse

## Status rounds

- None

## Decisions

- None

## Next steps

- rgdd, ln5: st roadmap

## Other

- fyi: kfs talked about ST at the transparency-dev summit. See:
  - https://www.youtube.com/watch?v=Lo0gxBWwwQE
- fyi: rgdd also talked about Sigsum (a building block of ST):
  - https://www.youtube.com/watch?v=Mp23yQxYm2c
- fyi: nisse also talked about witnessing on a tkey (useful for sigsum/tlogs):
  - https://www.youtube.com/watch?v=fY_v7yNrl2A
