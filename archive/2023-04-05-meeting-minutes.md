# System Transparency weekly

  - Date:  2023-04-05 1200-1250 UTC
  - Meet:  https://meet.sigsum.org/ST-weekly
  - Pad:   https://meet.sigsum.org/p/ST-20230405
  - Chair: ln5

## Agenda

  - Hello
  - Status rounds
  - Decisions
  - Next steps
  - Other

## Hello

  - ln5
  - kai
  - philipp
  - jens

## Status rounds

  - jens
    - planning with ln5
    - tooling (ci/cd scripts) with the goal of stmgr producing things expected by stboot
  - kai
    - just started, figuring things out wrt stauth
  - philipp
    - pxe netbooting and shim things
	- moving towards shim review
	- yubihsm setup

## Decisions

  - none

## Next steps

  - jens
      - finish road mapping
      - tooling
  - kai
      - aim at a first early attestation roundtrip
      - away for doing some talks
  - philipp
      - yubihsm tooling for automating the setting up of things

## Other

### Road map discussions

- draft in https://pad.sigsum.org/p/jBNyPtEvcTAY2cdeNGgh

- new points in the artifact/repo/version-train discussions

  - as long as we're in go land, its versioned packages makes it
    possible to squash disparate artefacts together, but when we leave
    golang we might need the structure that a separate repo gives
    (primarily git tags?)

  - what we care about is versioning of *interfaces*, like host config
    and trust policy (stprov -> stboot -> ospkg), attestation quotes
    (stauth-srv -> stauth-cli)

- proposition: stboot, stmgr and stprov are tightly connected and
  should be versioned together

  - they are all thin layers of code using "the library" which is the
    true owner of all the versioned interfaces which stprov, stmgr and
    stboot care about

  - TODO: enumerate all the interfaces in the whole ST universe and
    draw the dependency graph showing what piece of software
    (artefact?) depend on what interfaces

- stauth has two stages: provisioning and producing quotes

    - provisioning / enrollment
      - generating a key inside the TPM
      - how to authn that key? what trust root to use?
      - using an interactive protocol? tool -> online registry service -> tool?
        - not having to depend on an online service, both for
          availability and honesty, would have great value
      - there is a provisioning protocol: prov client (run by operator
        doing the provisioning) <--> server (running on device being
        provisioned)
    - producing the quotes (freshness is important)
      - the quote is a (versioned!) "document format" being sent to a verifying client

- the provisioning part of stauth is probably tightly connected to
  stprov and could be versioned together with "the library" and thus
  stboot, stmgr and stprov

- ln5 is looking at the needs for separate versioning from a users
  point of view, claiming that the people responsible for keeping boot
  loaders up to date are not the same as those keeping os packages up
  to date (just like those building ospkgs are not the same as those
  writing the programs going into the ospkgs)

  - TODO: is this valid and what other distinct sets of users should
    we care about? will stauth verification have a disparate set of
    users too, perhaps?
