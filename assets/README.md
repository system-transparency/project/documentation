# Assets

The System Transparency brand assets were developed by [9elements GmbH](https://9elements.com).

## Font

We use a font named [Ubuntu][], licensed under [Ubuntu Font License][].

[Ubuntu]: https://fonts.google.com/specimen/Ubuntu
[Ubuntu Font License]: https://ubuntu.com/legal/font-licence

## Colors

The color codes we use are:

  - \#FC2B59, primary color 
  - \#0C6695, secondary color
  - \#0B2339, secondary color variation
  - \#4A5D6E, teriary color
  - \#FFFFFF, white, background color

## Visuals

All [visuals][] are licensed under [CC BY-SA 4.0][].

[CC BY-SA 4.0]: https://creativecommons.org/licenses/by-sa/4.0/
[visuals]: https://git.glasklar.is/system-transparency/project/documentation/-/tree/main/assets
