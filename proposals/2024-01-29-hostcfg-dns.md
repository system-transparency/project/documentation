# Backwards-compatible `"dns"` field in host configuration

## Background

The host configuration field `"dns"` [used to be][] a single IP address that was
serialized in JSON format as a "string".  To support multiple DNS servers, the
value of the `"dns"` field was changed to "list of strings".  This was an
incompatible change.  Any platform provisioned with a host configuration that
carries the old `"dns"` value needs to be re-provisioned to upgrade `stboot`.

[used to be]: https://git.glasklar.is/system-transparency/core/stboot/-/commit/8d38269e88242a714edb1fd8623950900c40205c

## Proposal

If parsing of the new `"dns"` field fails, try to parse it as the old `"dns"`
field instead.  If this succeeds, ignore the initial parsing error and treat the
result as a list of IP addresses where the list contains a single IP address.

## Motivation

The proposed change would make the transition from a single to multiple DNS
servers backwards compatible.  This avoids re-provisioning hundreds of servers.

## Required changes

The required change is [modest][]: introduce a type, unit test its `UnmarshalJSON`.

[modest]: https://git.glasklar.is/system-transparency/core/stboot/-/merge_requests/149/diffs?commit_id=942caf6a0578d165b01c7f7a5c27ddb828287be1

## How to document this

The stboot reference documentation should list which host configuration version
is implemented.  This section is amended to specify what parts of the parsing is
relaxed for the purpose of backwards-compatibility.  The relaxed parsing will be
removed eventually, but only when other interfaces anyway must be broken as
well.  This reduces the number of times that breakage is forced on users.

Note: none of this affects writers of host configurations, such as `stprov`.

## Related notes

  - [Analysis of real-world host configurations that break](https://git.glasklar.is/rgdd/hostcfg)
