# System Transparency weekly

  - Date:  2025-02-10 1200-1250 UTC
  - Meet:  https://meet.glasklar.is/ST-weekly
  - Chair: nisse
  - Secretary: elias

## Agenda

  - Hello
  - Status rounds
  - Decisions
  - Next steps
  - Other

## Hello

  - elias
  - nisse
  - ln5

## Status rounds

  - nisse: Updated stprov to populate host config description
    - https://git.glasklar.is/system-transparency/core/stprov/-/merge_requests/105
  - nisse: Looking into sigsum signed OS packages. Question: What level of support do we want in stmgr?
    - see https://git.glasklar.is/system-transparency/core/stmgr/-/issues/67
    - nisse: news files for stprov
  - elias: doing stboot release testing: https://git.glasklar.is/system-transparency/core/stboot/-/issues/219
    - trouble with booting from usb stick for test server
    - nisse: earlier another supermicro server was used for such tests, also x11 family but not precisely the same model
    - seems like trange UEFI behavior for the stime test server?
    - maybe need new BIOS? (currently installed bios is from 2020)
  - Daniel Lublin made MR for updating secure boot signing
    - https://git.glasklar.is/system-transparency/core/stmgr/-/merge_requests/95
    - uses foxboron/go-uefi

## Decisions

  - None

## Next steps

  - elias: finish stboot release testing
  - nisse: stboot release (maybe elias?)
  - nisse: Prepare other components (stprov, docs, stmgr)
  - ln5: keep an eye on ST gitlab runners, saw a memory usage problem, may need to add more memory for those runners
    - for certain kinds of gitlab jobs it may be good to allow many jobs to run in parallel (long-running vs shorter jobs)
  - nisse: will check if new tag for docs repo is needed

## Other

  - elias: stboot release testing: apart from booting from USB stick, should we test also to place stboot on a regular (not USB) disk and boot from that?
    - nisse: if we can find an easy way to test that on our test server then that could be good, otherwise maybe wait until future. Anyway not needed for this release.
    - there is some old ubuntu installed on the test server, it is okay to remove that
  - nisse: stmgr + sigsum?
    - two ways possible, see issue where that is explained
    - ln5: we want an stmgr that can do everything
    - ln5: linking to a sigsum lib may be better
    - nisse: there may be an advantage in being able to do the signing on a separate machine
    - we can start with using sigsum tooling, then it is possible to later add more such functionality in stmgr
    - first step: use sigsum-submit to get proof, then use that with stmgr
