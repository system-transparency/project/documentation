# System Transparency weekly

- Date: 2024-11-11 1200-1250 UTC
- Meet: https://meet.glasklar.is/ST-weekly
- Chair: nisse
- Secretary: rgdd

## Agenda

- Hello
- Status rounds
- Decisions
- Next steps
- Other

## Hello

- rgdd
- elias
- kai
- philipp
- nisse

## Status rounds

- rgdd: not much to report, other than the meetup last week I've just done a bit
  of review and answered questions
- elias: just trying to learn how everything works (just learned we have this
  meeting every week!).
  - rgdd: take some notes with your impressions, things that are hard, etc. Will
    be good input to help us improve things.
- kai: worked on adding stvmm guest build scripts integrated into stimages.
  Finished guest <-> host quote interface via vsock.
  - general http api, you can easily add more API calls if you want to
  - trying to get my things into a shape where you can pretty much just type
    "make"
  - rgdd: would be nice to have an entry in the "prototype dashboard README",
    whenever it's ready enough. Both we and others look there.
- zaolin: missed the meetup last week -- aside from that tried to get through
  kai's stuff and deploy it (try the demos, dogfood). Looking into network stuff
  that fredrik asked about. Will continue looking into that this week.
- nisse: looking into the feature requests, automatically detect fasted network
  interface. Think I have something that works just for the static config case.
  - rgdd: note that dhcp already lacks a lot of other features, might be able to
    cut a corner here. Nisse seems to agree -- would be nice to refactor a bit
    first before also adding more things in dhcp (no-one has asked for features
    in stprov-dhcp so far).
- nisse: looking at what it would take to upgrade u-root to latest version,
  probably should work. Main issue: u-root commandline is completely different,
  it's now a wrapper for mkuimage to create an initramfs, which works
  differently and doesn't want go package paths for everything. Want them local?
  Probably need to create a directory and populate with go.mod and go.work file
  and stuff to make it build? Has anyone used u-root in this way recently?
  Anyone used u-root in this way recently and have input?
  - Seems like no, but nisse need to dig into this to make progress

## Decisions

- None

## Next steps

- rgdd: might start paging in SB milestone, if not this week probably next week.
  Poke me if you have questions and/or need review.
- kai: document repository so everyone knows what's going on
- kai: make stimages build both guest and hypervisor ospkgs, have a few patches
  that need to be cleaned up and merged
- zaolin: continue looking into the network things that fredrik asked about and
  that i looked at last week. And write documentation on how to use kai's RA
  stuff.
- nisse: look at the milestone with smaller features for st-v1.2.0. And see what
  ST related things Elias needs help with.
- elias: if nisse or rgdd can point at reasonable things to start looking at ->
  appreaciated, already got a few things from nisse. Obviously looking for
  things that are simple enough to start with. So if there are ideas and smart
  ways to begin -> please share!

## Other

- nisse: the measurement code in stboot, are we happy to just delete the current
  code and add it back when we have a better idea of what we really want it to
  look like?
  - kai: no functionality in there expect measurements, so if you don't use them
    it doesn't matter if you take them out or not. There is a prototype that
    uses the measurements as implemented in stboot. It's not the smartest way to
    do them (you can make it better), but there is no gigantic difference. But
    if you decide to get them back in, i suggest smaller changes. Don't measure
    json file after it has been parsed, that is a major issue. I.e., stboot
    doesn't measure the file as they are on disk (so there's canonicalization
    issues here...).
  - rgdd: is this documented somewhere?
  - kai: will make a list. We don't do attestation on specific PCRs, we take the
    whole set of PCRs. Measureing individual parts of OS package doesn't make
    much sense, just measure the whole OS package. You don't even need to
    measure the files in ISO individually, because it's also measured.
  - kai will put the version we know the prototypes work on (e.g. git-commit) in
    the readme. Nisse think the prototypes should work just fine on the
    currently latest release, but then if we pull out measurements then it will
    obviouyslyt stop working.
  - (rgdd: part of the context is we're bumping dependencies and go-tpm is
    lagging and we don't really want it to begin with, in favor of kai's
    standalone library).
