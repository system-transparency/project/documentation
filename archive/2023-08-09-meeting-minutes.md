# System Transparency weekly

  - Date:  2023-08-09 1200-1250 UTC
  - Meet:  https://meet.sigsum.org/ST-weekly
  - Pad:   https://meet.sigsum.org/p/ST-20230809
  - Chair: ln5 + Kai

## Agenda

  - Hello
  - Status rounds
  - Decisions
  - Next steps
  - Other

## Hello

  - ln5
  - Kai
  - Jens
  - Philipp

## Status rounds

  - Jens: played around with some golang stuff to address parallel download and timeouts
  - Kai & Philipp: brainstorm: stboot + secureboot make easier for users
    - core idea: shift (parts of) trust policy to TPM
  - Philipp: worked on documentation. MR is open in the doc repository.

## Decisions

  - No decisions.

## Next steps

  - Jens: review MRs on stboot
  - Kai: prototype the idea and document it with security model and compare to the status quo.

## Other

  - No other business.
