# System Transparency weekly

- Date: 2024-11-18 1200-1250 UTC
- Meet: https://meet.glasklar.is/ST-weekly
- Chair: rgdd
- Secretary: rgdd

## Agenda

- Hello
- Status rounds
- Decisions
- Next steps
- Other

## Hello

- rgdd
- elias
- zaolin
- kai (async -- note here)
- ln5

## Status rounds

- kai: clean up the stvmm code and changes to ra, stimages and stmgr. stmgr PR
  has been created.
  - https://git.glasklar.is/system-transparency/core/stmgr/-/merge_requests/94
  - nisse: the stmgr MR has been merged
- zaolin: getting into the firecracker story. Reading code, discussions,
  documentation about firecracker networking, created a document as wished.
  Created a guide for Kai's RA, going to push later.
  - A guide will be pushed later today/this week (zaolin is still working on it
    and cleaning up)
- nisse: Fixed the default host name bug in stprov
  - https://git.glasklar.is/system-transparency/core/stprov/-/issues/80
- nisse: Fixed interface speed autodetected in stprov
  - https://git.glasklar.is/system-transparency/core/stprov/-/merge_requests/95
- nisse: minor other stuff like version updates etc.
- rgdd: talked dep update notififcations with nisse and elias, would like to
  talk with at least ln5 too in the other section
- ln5: fyi -- our github mirroring is broken. One of the problems is that we
  have large commits or repos (don't remember which). Thinking about if/how to
  resolve.

## Decisions

- None

## Next steps

- kai: stmgr changes are being upstreamed (stmgr!13), stvmm documentation is
  WIP, dito changes to ra.
- zaolin: review of MRs from Kai, working on documentation and st-platforms repo
  with Kai and testing.
- ln5: will be setting up gitlab workers
  - rgdd: \o/
  - if you have ideas about tags for runners let linus know, but right now
    mainly just ensuring there are more runners. Linus will $do_something to get
    started.
- nisse: just continue doing the smaller fixes for st-v1.2.0
- rgdd: will probably start looking at the secureboot things for stprov, i.e.,
  page in
- ln5: take a look at renovate, can we use it for our go repos? See other
  section.
- rgdd: take a look at github mirroring

## Other

- fyi: if you stboot on 'Dell PowerEdge R340 with idrac-5ZGTJ53 "Enterprise"'
  you might experience "NO SIGNAL" in blue text immeaditely as it stboots. The
  debugging input on matrix:
  - DRM issue, e.g. that the kernel switches the console resolution to something
    that is outside the iDRAC video range
  - Adding "video=1024x768" to cmdline seems to resolve this issue
  - (Thanks JB for reporting+testing and bluecmd for solution hints!)
- nisse: In ST collection releases, would it make sense to add go.work files in
  the archive for build-deps between components?
  - Current behavior is that when, e.g., building stprov from a collection
    release archive, go tools will download whatever stboot version is listed in
    stprov/go.mod. An stprov/go.work file could point it at ../stboot instead.
    It's not clear to me what makes the most sense here, but I'm leaning towards
    *not* doing the go.work thing generally.
  - However, running integration tests on the archive should use go.work, e.g.,
    when stboot/integration/\*.sh needs stprov and stmgr executables, it should
    use ones built from the release archive sources.
  - Another option might be to require that all inter-component dependencies in
    go.mod specify the same version as listed in the manifest. However, that
    would require release order to always match the dependency graph, and we
    can't make a collection release that bumps stboot version without also
    bumping stprov. (Which would maybe get slightly different if we cut that
    dependency and move shared code to stlib).
  - rgdd: i'd also like to avoid go.work until it is strictly necessary. And as
    you're hinting on above, things would already be a bit better if common
    libraries did not live inside stboot f.ex. (What I dislike about the go.work
    thing here is that go install on the individual components at the listet
    versions and go install inside the tar archive would give different results,
    which would be nice to not have to explain).
  - rgdd: also posted comments related to the top-level makefile in your issue
    - https://git.glasklar.is/system-transparency/core/system-transparency/-/issues/235
- How about migrating the few old things that remain in the releng repo to
  docs/archive? I.e., I'm talking about:
  - src:
    https://git.glasklar.is/system-transparency/core/system-transparency/-/tree/main/doc?ref_type=heads
  - dst:
    https://git.glasklar.is/system-transparency/project/docs/-/tree/main/content/archive?ref_type=heads
  - (This destination would be consistent with other docdoc things that we
    weren't sure if they should be rendered somewhere, or maybe they will become
    useful at a later time. But keeping it in the releng repo seems like the
    wrong place. Another option is to put it in documentation/archive, or just
    delete. But I (rgdd) think I would appreaciate peeking at it at some point
    in the future.)
  - nisse will move it
- dependencies
  - https://git.glasklar.is/glasklar/services/gitlab/-/issues/48
  - script that writes an output file, could commit it to some git repo
    everytime it changes? And then an email notification everytime it updates?
    Or just a statefile on the local machine.
  - ln5: we have an issue for this (tpo uses renovate). Linus thinks it would be
    good to see if we can just click run on renovate. Linus can take a look at
    renovate and assess. If we don't here anything until next ST weekly we move
    forward with a temporary "email issues from a bashscript" solution.
  - so what we want: notification everytime a newer version of a direct Go
    dependency is available (for each of our go repos -- stmgr, stprov, stboot,
    etc). If it creates an issue or an MR -> would be perfect.
- GH mirroring is broken (?)
  - https://git.glasklar.is/glasklar/services/gitlab/-/issues/83
  - rgdd will take a look
- ST threat model -- what is an example of what should work / what is solved /
  what are we worred about / what do you get protected against.
  - nisse: We're a bit bad on documenting this, there is a little bit on this in
    docs/stboot-system.md. But not fully covered. But agree it would be good to
    have a better threat model documented. There is probably an issue for this
    $SOMEWHERE?
  - https://git.glasklar.is/system-transparency/core/stboot/-/blob/main/docs/stboot-system.md?ref_type=heads
  - https://git.glasklar.is/system-transparency/core/stprov/-/blob/main/docs/stprov-system.md?ref_type=heads#security-considerations
  - https://git.glasklar.is/system-transparency/core/stboot/-/issues/155 (threat
    model)
