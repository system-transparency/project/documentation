# System Transparency weekly

    - Date:  2024-09-23 1200-1250 UTC
    - Meet:  https://meet.glasklar.is/ST-weekly
    - Chair: nisse
    - Secretary: nisse

## Agenda

    - Hello
    - Status rounds
    - Decisions
    - Next steps
    - Other

## Hello

    - nisse
    - Kai

## Status rounds

    - Kai: made progress on stvmm: it starts a firecracker process and sets up networking correctly. Started to add basic attestation.
    - Philipp: stvmm work

## Decisions

    - None

## Next steps

    - Kai: implement stopping the VM and cleaning up the networking artefacts (TAP devices, routing table entries, firewall rules), get attestation working.
    - Philipp: wrap up stvmm work, all-systems-go

## Other

    - None
