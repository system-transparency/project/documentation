# Optional fields for backwards-compatibility

## Background

The current host configuration format requires that all fields are explicitly
set.  For example, omitting `"gateway":null` in a DHCP configuration is invalid.

## Proposal

Allow fields that are optional and/or irrelevant for a particular variation of a
host configuration to be omitted.  For example, it would be valid to omit
`"host_ip"` and `"gateway"` if `"network_mode"` is set to `"dhcp"`.  As yet
another example, it would be valid to omit `"bonding_mode"` and `"bond_name"` if
the platform is not meant to be configured with bonding.

In other words,

  1. an omitted field and
  2. a field that is explicitly set to `null`

will be treated in the exact same way.

## Motivation

The proposed change will make it easier to upgrade `stboot` on existing
platforms without needing to re-provision them.  For example, it is a surprising
limitation that adding support for bonding would break existing deployments;
i.e., the fields `"bonding_mode"` and `"bond_name"` are known to cause issues
with already provisioned platforms.  There is no good reason for this behavior.

## Required changes

Minor edits in our host configuration specification, then delete code.

Note: this change is backwards-compatible, parsing is just slightly relaxed.

## Related notes

  - [Analysis of real-world host configurations that break](https://git.glasklar.is/rgdd/hostcfg)
