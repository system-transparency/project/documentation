# Short meeting on the choice of HSM for the MS EUFI shim key

Date and time: 2023-02-21 16:10--16:20
Present: zaolin and ln5

## Background

 - <zaolin[m]> for MS signatures/key mgmt we need a FIPS 140 level 2+
 - <zaolin[m]> two options, we can buy a yubihsm or just use azure key vault in the cloud
 - YubiHSM's cost [950 USD each](https://www.yubico.com/product/yubihsm-2-fips/)

## Notes from the call

Zaolin presents some basics.

 - High maintenance, f.ex. need an API service set up

 - The MS "hsm in the cloud" service is fine from a security
   p.o.v. for this key, since the whole thing depends on MS
   infrastructure anyway

 - If on the other hand you're planning on using it for other keys as
   well, it makes sense to use a YubiHSM.


ln5 asks questions.

 - I have experience with setting up a YubiHSM for another signing
   infrastructure project.

 - I think we do want to put Sigsum keys (logs and witnesses) on HSM's
   and if so a YubiHSM would be our first choice for that.

 - What do we need an API service for?

   - Need to interact with the YubiHSM somehow, with PKCS#11 and
     complicated stuff.
   - Ok, that's fine. Been there, done that.


Conclusions

 - How are we on time? The YubiHSM path will require time for
   ordering, shipping and setting up.

   - zaolin is not blocked for now, since he can enroll our own
     (testing) root keys for now -- this will be the last stage of
     this project, to make sure things work with a shim signed by MS.

 - ln5 will get back when we've decided which way to go.
