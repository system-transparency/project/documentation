# OSPKG to UKI transition (n of m signature party)

**Author**: *Philipp Deppenwiese*

## Objective

This proposal aims to give an overview of the current state of the transition from the OSPKG to the UKI format. It will also give an overview of the different options we have to implement the uki format and the different tasks that need to be done to implement the chosen option.

One of the main challenges of the transition is the UKI signing. The UKI format uses the PKCS7 signatures stored inside the PE file container.

At the end are the questions this proposal would like to see decided.  The
proposal in its current form is not proposing a concrete answer, that's a TODO.
We may come back to update this proposal, or create a new one based on this.

## Current state

At the moment ST makes use of the [OSPKG](https://docs.system-transparency.org/docs/reference/data-structures/os_package/) format to sign and verify the integrity of the kernel and initrd. The OSPKG format is a custom format that is not used by any other project. The UKI format instead is a format that is used by Linux developers and distributions. Therefore it is a good idea to transition to the UKI format since it fulfills our needs and is used by other projects.

![](images/ospkg-signatures.png)

## Option 1: Detached signatures
We can stay with the current implementation of the OSPKG's and just change the packaging from the current one to the UKI format. This would mean that we would have to implement the UKI format in our code base but keep the manifest and descriptor format. This would mean that we would have to implement the following things:

* UKI format parsing and execution in stboot
* Update of the manifest and descriptor format to the UKI format

![](images/detached-signatures.png)


### Pros & Cons

* Extra file and complexity
* Multiple HTTP calls
* Easier to integrate with existing model

## Option 2: Using the PKCS7 signature bundle

Instead of having an extra descriptor file we use the existing PKCS7 directory structure of the PE data format.

![](images/pe-authenticode.png)

### Pros & Cons

* Better integrated and standalone/self contained binary
* Additional effort, especially on the code side for signing/verifying
* Requires custom OID's for ED25519


## Decisions to make

1. Which option do we want to choose?
2. if we go with option 2, do we use our implementation or upstream to [morten's github](https://github.com/Foxboron/go-uefi)

## Test Code (not production worthy)

* [stmgr](https://git.glasklar.is/system-transparency/core/stmgr/-/tree/mkosi-uki)

## References

* [the uki format](https://uapi-group.org/specifications/specs/unified_kernel_image/)
* [morten's pkcs7 Authenticode implementation](https://github.com/Foxboron/go-uefi/tree/master/pkcs7)
