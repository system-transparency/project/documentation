# System Transparency weekly

  - Date:  2023-04-26 1200-1250 UTC
  - Meet:  https://meet.sigsum.org/ST-weekly
  - Pad:   https://meet.sigsum.org/p/ST-20230412
  - Chair: jens

## Agenda

  - Hello
  - Status rounds
  - Decisions
  - Next steps
  - Other

## Hello

  - Jens
  - Philipp
  - Fox
  - Kai

## Status rounds

  - Jens: Figured required changes for stprov interaction with stboot in detail
    Continued work on stmgr to produce host config and trustpolicy
  - Philipp: Implementing Linus requested changes for stpki tooling
  - Fox: mkuki finish up, bug fixes, started integrating mkuki inside Taskfiles
  - Kai: Talked to kfreds re stauth design
    Preparing a presentaion about remote attestation

## Decisions

  - None.

## Next steps

  - Jens
    - Review Milestones, Roadmap, Meetup-Minutes and sync everything
    - integrate `stmgr create hostconfig` and `stmgr create trust policy in Taskfiles
  - Philipp
    - Testing stpki tooling against UEFI secure boot (SHIM)
    - Writing documentation
  - Kai
    - Read the docs from kfreds, further syncing
  - Fox
    - padding issue in mkuki
    
## Other

  - 
