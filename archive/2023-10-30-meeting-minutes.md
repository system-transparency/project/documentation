# System Transparency weekly

- Date: 2023-10-30 1200-1250 UTC
- Meet: https://meet.sigsum.org/ST-weekly
- Chair: jens
- Secretary: rgdd

## Agenda

- Hello
- Status rounds
- Decisions
- Next steps
- Other

## Hello

- rgdd
- jens
- daniel
- nisse
- philipp

## Status rounds

- philipp: common documentation + homepage restructuring, looks pretty good now.
  Have gotten good feedback from, e.g., quite.
- rgdd: drafty proposal on how we could do releases
  - https://git.glasklar.is/system-transparency/project/documentation/-/merge_requests/44
  - based on conversations with jens, nisse, and linus last week
  - would like to move it towards decision next week, please read, comment, give
    me a thumbs up or down, etc.
- rgdd: tried to hash out the website/blog/mailing list next steps, see below
- nisse: stboot-as-init did pass tests last week, want to clean up and get
  mergeable.
- nisse: but off rest of this week
- jens: working on things discussed at the meetup (releases, docu) / project
  management; looking at rgdd's proposal and some other issue/milestone clean-up
- daniel: nothing to report really

## Decisions

- Decision: Move forward with website and mailing list milestone
  - https://git.glasklar.is/groups/system-transparency/-/milestones/9#tab-issues
  - philipp will provide some friendly advice wrt. the blog in the other section
- Decision: Pull docs.system-transparency.org repo out into its own repo under
  system-transparency/project and prepare docu website to only render most
  recent tag, not HEAD of main.
  - philipp: sounds good, expect that wait to start with versioning until we
    have versions/releases for ST.
  - philipp: and merge all MRs before moving
  - nisse: to keep history, you can use `git filter-branch`, in particular
    `--subdirectory-filter` option.

## Next steps

- philipp: merge outstanding doc MRs (so we can eventually move)
- philipp: will take care of creating separate docs.sto repo when the timing is
  right, rgdd will create an issue for it and put it on the website milestone
- philipp: want to put a how-to/tutortial on the topic of "getting st up and
  running, start to finnish". This is currently a lot harder than it should be,
  played a bit with it last week.
  - jens: how-to: smaller, a bit more general. this sounds more like a
    tutortial.
  - philipp will give it a stab and then ask for feedback
  - nisse: tutortial should tell the whole story, and it should have pointers to
    further details for various things. If it leads to how-to:s or
    references/specifications, that's not so important. The important part in a
    tutortial is the story telling.
  - jens: how-to tells you how to build up, e.g., an OS pkg with commands you
    can use but says PUTYOURARGSHERE.
- philipp: more howtos, common documentation and guidelines
- rgdd: see what we can do about roadmapping/planning, "self-organize"
- jens:
  https://git.glasklar.is/system-transparency/core/stboot/-/merge_requests/116
  - (rgdd: is short on time, willing to take that review request from me? Jens:
    yes.)
- jens: MRs i'm assigned to as reviewer, and work on my docs things. And help
  rgdd with the release proposal and the roadmapping/planning stuff.
- daniel: gonna play with custom keys 🤞, may be time to start thinking about
  building of ospkgs
  - philipp: let me know what problems you're experiencing, or if you need help
  - rgdd: feel free to put notes in our archive if you think it is helpful,
    and/or contribute things to docs.sto

## Other

- Philipp on blog
  - documentation book theme has a blog
  - and one can click on a link to see all post
  - we can also link that to the homepage
  - otherwise can recommend ghost, but not markdown; but the blog software is
    super great.
    - rgdd: then out-of-scope
