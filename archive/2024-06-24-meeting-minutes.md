# System Transparency weekly

- Date: 2024-06-24 1200-1250 UTC
- Meet: https://meet.sigsum.org/ST-weekly
- Chair: nisse
- Secretary: rgdd

## Agenda

- Hello
- Status rounds
- Decisions
- Next steps
- Other

## Hello

- rgdd
- nisse
- philipp
- jens
- kai

## Status rounds

- nisse: Merged provision-on-failure,
  https://git.glasklar.is/system-transparency/core/stboot/-/merge_requests/212
  - question: Good enough for now? Next step is a way to interrupt an otherwise
    successful boot, e.g., by pressing Ctrl-C. WIP MR:
    https://git.glasklar.is/system-transparency/core/stboot/-/merge_requests/213#note_17751
  - rgdd: need to be able to interrupt normal boot too
  - nisse: need to have a look at the state of testing
  - nisse and rgdd will talk more async
- rgdd: rubberducking, review, a bit of releng thinking wrt. collection release
- jens: last week finished first part of tutorial (adapted for st stable
  release, QEMU). Started to also do this with the unmatched board.
- kai: merged all my code. remote attestation stuff is in main in
  st-platform-features. Converted to use LFS. Don't want to run on airport/hotel
  wifi, but should be usable. Made myself a lot of friends by force pushing to
  main.
- philipp: nearly done with documentation, need to rebase on kai's force push
  changes. But it was planned, it's fine. :)

## Decisions

- None

## Next steps

- niels: try to get the provision on interrupt merged. And sync the release
  stuff with rgdd.
- rgdd: what nisse said.
- philipp: nothing, except for deleting the twitter account based on last week's
  decision.
- kai: my next step is attend the august meetup. But if you have questions
  regarding the remote attestation, just write me.
- jens: trying to finnish what i mentioned in the status round, including open
  issue with the kernel configs with random number generations.
- nisse: fyi: on vaccation from next week and forward, back in mid august.

## Other

- rgdd+nisse breakout. TL;DR:
  - Will prep a test for ctrl-c. Want it to work in CI and on SM. If this
    becomes complicated / we run into issues, then we will give the user prompt
    a go instead.
