# System Transparency weekly

    - Date:  2024-04-15 1200-1250 UTC
    - Meet:  https://meet.sigsum.org/ST-weekly
    - Chair: nisse
    - Secretary: kfreds (rgdd will persist pad after the meet, later today)

## Agenda

    - Hello
    - Status rounds
    - Decisions
    - Next steps
    - Other

## Hello

    - rgdd
    - kfreds
    - zaolin
    - nisse
    - quite (async)
    - kai

## Status rounds

    - quite: I made a little write-up on how to use TKey for git-ssh-signing
        - https://github.com/quite/test-tkey-git-ssh-signing
        - note: mc said we should be able to use the updated ssh app until wed next week, maybe sooner.  Sounded like a release was about to happen ~this week. /rgdd
    - quite: MR:ed stricter OSPKG URL checking by default in stprov, override with -f
    - rgdd: Drafty roadmap, early input welcome. Want to decide it 2 weeks from now.
        - https://git.glasklar.is/system-transparency/project/documentation/-/merge_requests/57
    - rgdd: vistor now has a CI/CD setup for testing our things, he said we can poke him if we want early integration testing (just specify tags of stboot, stmgr, stprov).
    - rgdd: more of the same from last week -- triage to get our releases finalized
    - zaolin:
        - updated the red/pink color in docs.sto template; and code blocks now look more like code blocks rather than a frame. Requested by rgdd et al.
        - meetup + post cleanup. Started to read systemd build docs
    - nisse: meetup + release preps.
        - Notes on testing procedures, https://git.glasklar.is/system-transparency/core/stboot/-/merge_requests/188
        - Notes on release process, https://git.glasklar.is/system-transparency/project/docs/-/merge_requests/20
    - kai: back to work. working on platform firmware measurements. i
      had a few false starts but now came up with a approach that looks
      promising. It's flexible enough to work on most hardware but also
      simple enough to be implementable in a reasonable time and not
      have high maintance burden.
    - kfreds: categrized things we could do at the meetup.  SB, which philipp
      has been working on.  Seems to be done enough so putting that on hold.
      Kai's most important thing is to continue with remote attestation work.
      And perhaps support philipp regarding TPM cred protection.  (Philipp is
      waiting for the core team to want to integrate SB, then we can warm it up
      again.)  Philipp will continue working on the systemd approach.  Until the
      next meetup, the primary work package will be to take the existing proof
      of concept, scale it down, build all of it from source, minimize sw supply
      chain, and see how large that makes stboot.  And how deep/wide the sw
      chain gets.  The overcall conclusion seems to be: using systemd for
      network config seems to have quite a bit of potential.  And kexec-tools
      for kexec also seems to have potential.  Obv a different architecture from
      pure Go and u-root; and a poc.  But poc take#2.  Will present how it's
      going next meet up.  Jens is going to continue to do some work, he has
      been asked to get coreboot + stboot working on chromebook as a poc.  And
      on a few risk-v board.  Hoping jens will do a presentation next meetup
      "how do you stboot your chromebook".  Fredrik is mostly busy with other
      things.  In the plumbing team, first priority is to work with Kai on
      Remote Attestation; and to give philipp and jens feedback on
      specifications they will write for their work packages.

## Decisions

    None.

## Next steps

    - zaolin: support core team for sb when requested, working on systemd stboot tcb/component reduction. Building systemd in a minimal way.
    - rgdd: pick up the releng things from where nisse left it off (see link above), figure out rendering of docs.sto (pulling in docs from stprov/stboot/stmgr).
    - nisse: continue with wrapping up the release
    - kai: going to work on measurement precomputation for stboot and get remote attestation working. I've collected enough information from enough machines that I'm confident it'll work the same way on those machines.
    - kfreds: see status round -- that's what i will be up to.

## Other

    - rasmus and nisse: It would be helpful to get a proposal, or advice, from Kai. https://git.glasklar.is/system-transparency/core/stboot/-/issues/126
    - Some links and notes on R-B tarballs
        - https://blog.josefsson.org/2024/04/13/reproducible-and-minimal-source-only-tarballs/
        - https://lwn.net/Articles/921787/ (thanks Foxboron + some brief dicussion)
        - From IRC -- seems like it is reasonable to assume the git-archive
          output is reproducible if the same git version is used.  And that there
          is no defacto "this is how you do reproducible source-only tarballs"
          yet. The recent xz incident has spurred a bit more interest in having
          something like that though.  As an approximation, we would likely be
          doing quite well for our ST releases if we git-archive while being
          intentional about the used git-version and communicating clearly which
          one was used to facilitate R-B.
    - https://chains.proj.kth.se/software-supply-chain-workshop-3.html
    - on friday next week there's a workshop at kth on supply chain
