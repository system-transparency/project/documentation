# Proposal on sigsum-logged OS packages, keeping x.509 certs

## Motivation

To provide key-usage transparency for the keys used to sign OS
packages. Without a larger overhawl of the OS package format, and
including indirection (via x.509 certificates) to identify the
appropriate signing keys, and enable key rotation.

## Proposal

In stboot's `/etc/trust_policy` directory, add an optional file
`ospkg_trust_policy`, with a policy file according to
https://git.glasklar.is/sigsum/core/sigsum-go/-/blob/main/doc/policy.md.

If this file exists, the `signatures` field in the OS package
descriptor (json) file is handled differently. The `certificates`
field is handled the same way: Each certificate is validated based on
the `/etc/trust_policy/ospkg_signing_root.pem`, the subject Ed25519
key is extracted, and duplicates are dealt with, and the
`ospkg_signature_threshold` is applied the same way.

But when verifying the corresponding entry in the `signatures` list,
the value must be a (base64-encoded) version 2 sigsum proof,
https://git.glasklar.is/sigsum/core/sigsum-go/-/blob/main/doc/sigsum-proof.md.
The proof is verified using the certificate's subject key as the only
valid submitter key, and the `ospkg_trust_policy`.

### Limitations

The aim of using Sigsum logging is to ensure that every OS package
signature that is accepted by stboot will be detected by a monitor.
However, the above verification procedure fails this objective, in the
case that the CA is compromised. Anyone who controls the CA signing
key can create and certify a new signing key, use that to sign and log
OS packages, but not tell any monitor about this signing key. The
monitor will get the corresponding leaves when tailing the log, but it
can't know that they are relevant, and hence unexpected signatures
can't be detected.

Hence, to get solid transparency with this proposal, the signing root
certificate cannot be a CA certificate (i.e., key usage extension
without CertSign). Specifying multiple certs in the trust policy is
under consideration, see issue
https://git.glasklar.is/system-transparency/core/stboot/-/issues/198.
See below for how to add cert transparency.

## Further work: Transparent certificates

We could potentially enforce transparency also for the certificates.

We might try to hook into the CT system for the OS package certs, but
I suspect that is complexity we don't need. We could also use the same
sigsum log for this purpose:

The rough idea is that each entry in the `certificates` list should
include a proof that the certificate has been logged by the CA. The CA
is expected to submit `msg = SHA256(der-encoded cert)`
to a Sigsum log, using the the same CA signing key to sign the Sigsum
submission.

When cert transparency is enforced (controlled by a flag in the trust
policy, or possibly by some custom extension in the root cert), stboot
would then need to

1. Split the "cert" entry into a x.509 cert and sigsum proof.
2. Verify x.509 validity of the cert, from the signing root cert.
3. Verify that the cert was properly logged, verifying the proof using
   the configured policy and the CA's public key as the only valid
   submitter key.
4. Parse the signature as another sigsum proof, check that it
   validates as a proof of logging for the OS package, with the cert's
   subject key as the only allowed submitter key.

(May need a special case if the cert equals the root cert).

### Sketch of how to monitor

A monitor need only be configured with the CA key(s). When tailing the
log, it extracts all entries signed by the CA key. If uses some
service of the CA's to map checksum to certificate, extracts the
subject key from the cert, and then extracts all log entries signed by
that certified key. This could even be generalized to multiple levels
of certs, if key usage information is used consistently.
