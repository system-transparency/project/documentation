# System Transparency weekly

- Date: 2023-10-16 1200-1250 UTC
- Meet: https://meet.sigsum.org/ST-weekly
- Chair: ln5
- Secretary: rgdd

## Agenda

- Hello
- Status rounds
- Decisions
- Next steps
- Other

## Hello

- rgdd
- jens
- ln5
- joel
- philipp
- kai
- nisse

## Status rounds

- rgdd: fixed stboot provision mode and the associated demo-2
  - https://git.glasklar.is/system-transparency/core/stboot/-/issues/141
  - https://git.glasklar.is/system-transparency/core/system-transparency/-/issues/220
- rgdd, kfreds: sketched on moving website/blog/mailinglist things forward
  - (rgdd will create issues and a milestone for this so others can provide
    input, but it's basically what we've discussed as possible changes before)
- nisse: Fixes for
  https://git.glasklar.is/system-transparency/core/stprov/-/issues/11 and
  https://git.glasklar.is/system-transparency/core/stmgr/-/issues/37 in review.
- nisse: Need some advice on how to debug stboot as init, log here:
  https://git.glasklar.is/system-transparency/core/system-transparency/-/jobs/8714.
  It appears validation of os package succeeds, and everything is fine, until
  kexec syscall which fails because the kernel image is somehow too short (empty
  or truncated).
- philipp: Couldn't finish as much as I wanted last week. New documentation
  homepage design with integrated feedback is public. Still working on overview
  docs.
- kai: worked on unit testing stboot: stboot!116, upstreaming metadata handoff
  (stboot!106) and researching how we get a Supermicro server into Setup Mode
  remotely.
- jens: Review, working on Issue / Milstone management / Release Workflow
- ln5: company delivering test HPE system said there's a hickup, no date
  currently
- joel: provided some feedback in the docs, some of it still unresolved.

## Decisions

- None

## Next steps

- rgdd: make website/blog/mailinglist a planned activity, would like to get
  started with the actual work sometime next week (this week mostly busy in
  stockholm)
- philipp: working on overview docs when nothing else to do. Sync regarding
  multiple matters (Secure Boot), u-root removal and others. Want to add
  something about roadmap at landingpage somewhere. Jens would also like intro
  to doc page on the landing page.
- kai: close the remaining merge requests, add info about setup mode into TPM 2
  proposal and work on what we decide on the meeting.
- jens: meetup discussion on docu / release / versioning; want to get milestone
  things back into shape.
- ln5: ST booting two Supermicros, UEFI style
  - (Might be an interesting workshop thing to do together)
- nisse: meetup

## Other

- what we have typed down (and decided) on review so far, doesn't mention the
  "DRAFT: " case though:
  - https://git.glasklar.is/system-transparency/project/documentation/-/blob/main/proposals/2023-09-25-gitlab-roles-and-conventions.md?ref_type=heads
  - if anyone want to make a follow-up that defines what you're expected to
    (not) do when assigned on a "DRAFT: " MR, please do. The common view seems
    to be we don't generally assign reviewers if the MR is in state "DRAFT: ",
    if you want review -> page someone and say explicitly what you'd like them
    to preliminary look at.
- kai: note that SREcon CFP deadline 31 oct, would like to submit ST talk
