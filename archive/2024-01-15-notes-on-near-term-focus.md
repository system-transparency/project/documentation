# What will rgdd, nisse, and ln5 be working towards in the near future?

Some drafty notes on what rgdd, nisse, and ln5 plan on working towards. There is
currently no plan to put together a formal roadmap with release dates and such.

If we have any concrete milestones, they will appear in the [milestone tab].

## First round of priorities (and hence the most detailed)

In no particular order:

- All three of us setup our own release keys for signing stuff. Use TKeys.
- Take another stab at "status quo" component releases. Scope: bug fix and
  improve testing of stprov, stboot, and stmgr. Goal: something stable we can
  support; get quality assurance via CI pipelines and real hardware tests.
- Define an ST release process that puts everything together. Probably includes
  a plan for integration testing based on different versions. Exactly what goes
  into this depends on how we test each component release.
- Take another stab at ST's reference documentation. Focus on:
  - stboot: stbood.md (protocol / system description), manual.md (cmd stuff)
  - stprov: stprov.md (protocol / system description), manual.md (cmd stuff)
  - stmgr: manual.md (cmd stuff)
  - stimage.md (prev: "ospkg.md")
  - host-config.md
  - trust-policy.md
  - efi-nvram.md (including stprov -> OS interface)
  - (If we cannot do all of this now with the level of detail we would like,
    then we will lower the bar for stboot.md, stprov.md, and the cmd stuff.)
- Take care of docs.sto, e.g., includes getting the process for versioned
  documentation in order so that can actually be depended on in the future.
- Setup an ST image repository. Bake ST images on a daily basis for Debian
  stable and Debian testing. Each ST image is signed and made available on a
  public URL. We also want to make a compiled and signed version of stboot
  available.
- Tutorial on how to get going with ST both when using libvirt and when using
  real hardware. No use of task here. Refer to the already compiled stboot and
  stimages from above.

rgdd: starts by taking a stab at "all things stprov" and other reference docs.
Culminates in bug fixes, more testing, QA on hardware, and eventually a release.

nisse: starts by taking a stab at "all things stboot" and other reference docs.
Culminates in bug fixes, more testing, QA on hardware, and eventually a release.

ln5: starts by taking a stab at stimages and prepping for the tutorial, and
supports rgdd and nisse with onboarding for hardware testing of stprov/stboot.

Once we've made a bit of progress the coming ~2 weeks, we will see where we're
at and divide and conquer based on what needs to be done to make more progress.

## Second round of priorities

Now we have "status quo" releases of what we would like to support. In lack of a
better name, let's refer to this as our "status quo series". Next steps:

- Consider if there's any refactoring that should be prioritized
- Bring in feature requests that are not altering interfaces in breaking ways

The goal for this "status quo series" is to be stable. We will bug fix and
consider bringing in "ops features" that are small-ish and backwards compatible.

Things we will likely focus on:

- MS
  [Changes not altering any interfaces](https://git.glasklar.is/groups/system-transparency/-/milestones/8#tab-issues)
- Bring back the option of doing ST with disk again -- stboot hostconfig read
  from disk, stimages loaded from disk

## Priorities later on (and hence the least detailed)

Make our implementation of ST more concept complete. For example, adopt Sigsum,
use TPMs, support remote attestation in a meaningful way, etc. The intent is to
do this in a new release series while keeping the stable "status quo series"
supported for at least one year (so that early adopters have something stable
they can depend on).

[milestone tab]: https://git.glasklar.is/groups/system-transparency/-/milestones
