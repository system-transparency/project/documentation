# System Transparency weekly

  - Date:  2023-03-29 1200-1250 UTC
  - Meet:  https://meet.sigsum.org/ST-weekly
  - Pad:   https://meet.sigsum.org/p/ST-20230329
  - Chair: ln5

## Agenda

  - Hello
  - Status rounds
  - Decisions
  - Next steps
  - Other

## Hello

  - ln5
  - zaolin
  - Foxboron

## Status rounds

  - zaolin: documentation
  - ln5: various admin plus road map planning

## Decisions

  - None.

## Next steps

  - zaolin: UEFI shim build environment set up
  - zaolin: Shim signing request review, finalize. Need input from
    Linus and Rasmus -- please add your info directly into the 'st'
    branch.
  - Foxboron: stmgr integration, including the -sbat option for mkuki.

## Other

  - Road map https://pad.sigsum.org/p/jBNyPtEvcTAY2cdeNGgh
    - Looks good, nothing sticks out as crazy and nothing is obviously missing
    - zaolin is happy about Documentation being on the road map
    - ln5 will find Jens and go over this with him too
    - With the goal of deciding on the road map at the next ST weekly,
      April 5, we might need to take another pass over this Monday or
      Tuesday. ln5 will be coordinating this.
  - https://markwhen.com/ is an alternative to
    https://mermaid.js.org/ for our documentation needs
