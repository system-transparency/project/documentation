# System Transparency weekly

  - Date:  2023-02-08 1200-1250 UTC
  - Meet:  https://meet.sigsum.org/ST-weekly
  - Pad:   https://meet.sigsum.org/p/ST-20230208
  - Chair: ln5

## Agenda

  - Hello
  - Status rounds
  - Decisions
  - Next steps
  - Other

## Hello

  - ln5
  - Foxboron
  - Jens

## Status rounds

  - Jens: Trust policy implementation; tooling (stmgr using the stboot library)
  - Foxboron: Network refactoring merged; network interfaces
  - ln5: Bug fix in MPT/stprov

## Decisions

  - Cancel next week's meeting (2023-02-15) due to schedule conflicts with the whole team

## Next steps

  - Jens: Adoptions and implementation of host config and trust policy, and tooling
  - Foxboron: Network interfaces; mkiso / kernel initramfs (Jens will assign issues)
  - ln5: Release MPT/stprov; road mapping
  - ln5: Mirror our repos to GH

## Other

  - Road map discussions
    - Postponed to the dedicated meeting tomorrow 2023-02-09
  - Would it make sense to move our Gitlab milestones up one notch, to the system-transparency group rather than on the system-transparency/core level? 
    - Sounds good. AP ln5: Do the move.
