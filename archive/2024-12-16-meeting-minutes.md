# System Transparency weekly

  - Date:  2024-12-16 1200-1250 UTC
  - Meet:  https://meet.glasklar.is/ST-weekly
  - Chair: nisse
  - Secretary: ln5

## Agenda

  - Hello
  - Status rounds
  - Decisions
  - Next steps
  - Other

## Hello

  - ln5
  - nisse

## Status rounds

  - rgdd: not here today, having a guest lecture at KAU
  - rgdd: fixed GH mirroring, we now mirror:
    - core/system-transparency
    - core/stprov
    - core/stmgr
    - core/stimages
    - core/stboot (paused, won't fix)
    - https://git.glasklar.is/glasklar/services/gitlab/-/blob/main/mirroring.md
  - nisse: Merged logging of root cert expiry dates
    - https://git.glasklar.is/system-transparency/core/stboot/-/merge_requests/235
  - ln5: all ST projects at git.glasklar.is are now using the new gitlab runners
    - note that we're pulling "debian" and "golang" container images
      from our own repository, which is *not* being updated
      automatically yet
    - one effect of this change is that only a subset of the official
      tags are reachable without specifying a registry, see
      https://git.glasklar.is/glasklar/services/gitlab/-/issues/86

## Decisions

  - Upcoming weekly meetings
    - Weekly on 2024-12-23 has been canceled due to holidays.
    - Weekly on 2024-12-30 has been canceled due to holidays.
    - Weekly on 2025-01-06 has been moved to 2025-01-07 15:00 UTC (16:00 CET) due to holidays.
    - The Nextcloud calendar has been updated to reflect these changes.

  - Multiple signing root certs
	  - https://git.glasklar.is/system-transparency/project/documentation/-/merge_requests/65
	  - async +1 from rgdd
	  - Decided.

## Next steps

  - nisse: merge and implement above proposal

## Other

  - FYI from quite @ matrix about upgrading to u-root v0.14.0
    - I think somebody mentioned the issue with u-root 0.14.0 and the
      option -uroot-source being gone. I've tried out building using
      0.14.0 and have found that it is solved by ditching that option,
      and running u-root with its source tree (repo) as current
      working directory. You will thus also need to adjust any flags
      that passes non-absolute file names.
    - related from nisse: https://git.glasklar.is/system-transparency/core/stprov/-/merge_requests/96#note_20619
  - Glasklar will start 2025 by dogfooding ST for both internal and external services.
