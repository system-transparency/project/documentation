# System Transparency weekly

- Date: 2024-04-08 1200-1250 UTC
- Meet: https://meet.sigsum.org/ST-weekly
- Chair: rgdd
- Secretary: rgdd

## Agenda

- Hello
- Status rounds
- Decisions
- Next steps
- Other

## Hello

- rgdd
- zaolin (async)
- jens

## Status rounds

- rgdd: docdoc things and triage to try and wrap up the upcoming releases
- zaolin: Finished demo for the meetup, still preparing presentations because I
  got knocked out by gastrointestinal infection for one week. I expect to have
  everything ready by tomorrow.
- jens: st working on sifive board, will have session about it on the meetup

## Decisions

- None

## Next steps

- rgdd: more docdoc things and and triage to try and wrap up the upcoming
  releases
- zaolin: meetup, finishing presentations and coordination with the platform
  plumbing team.
- jens: meetup

## Other

- Drafty guide on getting ST running by Linus (shared by rgdd as jens asked
  about it)
  - https://git.glasklar.is/system-transparency/project/docs/-/merge_requests/16
- rgdd also mentioned the refdocs that are both in main now @ docs repo, and
  also in docs/ subfolder in stprov, stboot, stmgr. In case anywant wants to
  look at it early.
- Jens would also like Go v1.21, similar to rgdd.
