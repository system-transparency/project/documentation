# System Transparency weekly

- Date: 2024-10-28 1200-1250 UTC
- Meet: https://meet.glasklar.is/ST-weekly
- Chair: rgdd
- Secretary: rgdd

## Agenda

- Hello
- Status rounds
- Decisions
- Next steps
- Other

## Hello

- rgdd
- nisse

## Status rounds

- rgdd: fixed periodic OS package signing that had stopped working (used in
  CI/demos)
  - The "fix" was to install a missing dependency that made the script fail; and
    then also create a new signing key+cert for Q3. We're updating the latter
    manually.
  - https://git.glasklar.is/glasklar/trust/glasklar-ca/-/tree/main/QA-CA/ST-signing?ref_type=heads
- rgdd: prepared and circulated roadmap, hoping for a decision today
- nisse: Started updating of ST repos to use go 1.22.

## Decisions

- Decision: Cancel 2024-11-04 weekly
  - Because most of the regulars are on an in-person Sigsum/ST meetup
- Decision: Adopt the updated roadmap, renew towards the end of January 2025
  - https://git.glasklar.is/system-transparency/project/documentation/-/merge_requests/63

## Next steps

- nisse: add an issue to maintenance milestone to finnish clean-up of
  system-transaprency repo which is now releng
- nisse: st v1.2.0 milestone
- rgdd: merge and undraft roadmap
- rgdd: maybe start reading/thinking/paging-in SB, otherwise doing that ~after
  meetup

## Other

- nisse: Should we delete the current TPM measurement code from stboot? Main
  benefit would be to drop the go-tpm dependency. When we get back to RA, I
  would expect that (i) it will be based on Kai's TPM library, and (ii) it will
  not be compatible with what's in stboot now. But I haven't looked in detail at
  what's currently being measured, but I'm particularly confused by the
  "identity" measurements, PCR 14. Docs seem to say that the UxIdentity is an id
  provisioned in the TPM, but it looks like it's read from an EFI variable, and
  then there's the DataChannel which isn't mentioned in
  st-docs/content/archive/ra/stboot-measurements.md.
  - half of stboot main is this old ra stuff, and the go-tpm dependency is not
    well maintained and the tests are a bit flakey
  - rgdd: from the top of my head -- we don't support this RA stuff (see ref
    docs). And we will likely need to change it when we're ready to support RA
    for real. So it might be the case that we should have ripped it out even
    earlier. I think kai's prototype uses this though, so would be good to at
    least make sure his demo is pinning stboot version etc so that it doesn't
    break when folks want to try it.
  - nisse will sync this with kai next week, not urgent
