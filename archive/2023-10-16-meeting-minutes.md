# System Transparency weekly

- Date: 2023-10-16 1200-1250 UTC
- Meet: https://meet.sigsum.org/ST-weekly
- Chair: Fredrik
- Secretary: rgdd

## Agenda

- Hello
- Status rounds
- Decisions
- Next steps
- Other

## Hello

- rgdd
- kfreds
- nisse
- jens
- quite
- joel
- ln5

## Status rounds

- philipp: Worked on the theme issues from Rasmus. Discussions with Fredrik &
  Kai. Started to work on howtos and common documentation improvements
- nisse: Playing with minimal kernel + init process seems to work
  - https://git.glasklar.is/nisse/minimal-boot
- nisse: Attempting to have stboot run as the init process (with no
  uroot-wrapping). Looks like it is able to load an ospackage, and no errors
  reported until the kexec_file_load syscall, where the kernel complains that
  image is too small, \< 1024 bytes.
- nisse: License files for stboot and stprov updated.
- rgdd: review and provide feedback, suggest README for stprov. Didn't find as
  much time for ST last week as intended, so will be more of that this week
  instead.
- doctor-love: Minor documentation work, spelling/phrasing fixes and feedback
  - very much appreaciated the added explainer docs!
- kfreds: Back from OSFC.
- ln5: Nothing to report. HP system delivery date 30 oct.
- quite: nil
- Jens: OSFC. Super-excited to catch up on things.

## Decisions

- None

## Next steps

- philipp: Finish howtos and common documentation.
- doctor-love: quality assurance of docs
- rgdd: fix the broken provision mode and test+doc things surrounding that
  - https://git.glasklar.is/system-transparency/core/stboot/-/issues/141
- nisse: Working on a few assigned issues, complete MRs in review.
- quite: learning more about stprov. and server hardware specs regarding uefi
  versions, tpm, etc; planning to get in touch with Mullvad folks to talk ST.
- kfreds: Prepping for ST/Sigsum meetup next week.
- ln5: Trying to be helpful when need be.

## Other

- Stboot: Do we have a plan or issue for reducing amount of copies? My
  understanding is that we (1) load ospackage into RAM, (2) unzip all
  interesting parts into RAM (3) copy kernel and initrd into files in tmpfs in
  RAM, (3) call kexec_file_load to copy into kernel memory. And on top of that,
  maybe ospackage was in RAM in the initrd to start with. I didn't find anything
  after a quick search in the issue tracker.
  - Could be improved, but until now havn't had any issues. But we're aware that
    lots of RAM is being used. And have tried to improve lazy readers for, e.g.,
    OS packages. This is something we could have a look at though.
  - Optimal, copy twice?
  - APUs w/ 4G RAM is a use-case where RAM matters. Either 4G or 8G. Maybe not
    the highest priority right now though, but would be nice to not ignore this
    use-case.
- Bump example/documentation reference distro to Ubuntu 22.04LTS?
  - (Currently 20.04 is used in examples.)
  - Would be nice to use the latest LTS in docs, shows that the project is
    alive.
  - Create an issue to get this resolved, Jens will fix.
- Report from OSFC
  - Fredrik's talk "Towards authentication of transparent systems"
  - Jens's workshop "Getting started with System Transparency"
    - hands-on session, how to get stboot up and running
    - was after fredrik's talk
    - found many people that were interested, and folks with questions about
      stam
    - turned into panel discussion (?)
    - discussion about measured boot, trusted boot, understanding overall
      concepts
    - jens offered hands-on session remotely if anyone interested, no-one
      reached out so far
    - were able to deliver good answers and insight to interested folks
    - some takeaways for jens: we need to further improve docs, and be able to
      more quickly tell people about the essence of the project. "This is the
      problem, and this is how we solve it. This is what ST is about". We should
      be able to do that in 5m. Harder than expected, even with a 30m talk.
      Something we should practise and document.
    - Q: what were people's biggest quiestions marks about ST?
      - Usage, pros and cons. Where to (not) use measured and verified boots.
    - "Five levels of explanations of ST"
  - "Market"/industry realizations
    - Companies, projects and people are moving in the direction of ST (measured
      boot, remote attestation, repro builds, translogging, "state space
      constrainment?")
  - ST project stuff
    - We should improve our website
    - We should start a blog (system-transparency.org/blog?)
    - Mailing lists?
  - Technical realizations / thoughts
    - Philipp and Kai at All Systems Go in mid-September
      - "systemd folks are doing lots of things that are related to ST"
      - Kai's TPM event log talk
      - Lennart's UKI talk
      - Lennart's TPM talk
      - mkosi talk
  - Ron's TinyGo talk
  - Vagrant's repro talk
  - AMD EPYC 3rd gen with coreboot?
    - openSIL 2026
    - (Before then -> confidential VMs with ST would have to target UEFI.)
    - Replacing OSPKG with UKI is a concrete idea of consuming other projects
      things instead of developing them ourself, as a result of changing
      landscape where there are other projects doing similar things in the same
      space
  - All systems go talks are published, OSFC talks will be published in ~2w
- Shameless plug: Tillitis would like to involve the OSS/OSHW community in the
  development process of TK2 ("what would people like?" etc)
