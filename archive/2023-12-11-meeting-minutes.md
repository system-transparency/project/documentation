# System Transparency weekly

- Date: 2023-12-11 1200-1250 UTC
- Meet: https://meet.sigsum.org/ST-weekly
- Chair: kfreds
- Secretary: rgdd

## Agenda

- Hello
- Status rounds
- Decisions
- Next steps
- Other

## Hello

- rgdd
- ln5
- nisse
- jens
- kfreds

## Status rounds

- rgdd: a bit of review/feedback etc + prepared proposal, see below
- rgdd: some quick notes on what i'm doing as the secretary, in hope that i will
  be able to hand that over to someone else sometime soon. Any volunteers?
  - https://git.glasklar.is/system-transparency/project/documentation/-/merge_requests/48
- zaolin (async report):
  - got ill last week and couldn't finish as much as wanted.
  - Worked on the meetup presentations and uki signing proposal
- kai (async report):
  - finished the slides for the meetup
  - did some bugfixing (stboot#153, stboot#151, stprov!29)
  - small improvements to stboot's output and how remote attestation stores
    provisioning information (stboot!131, stboot#150/stauth#17)
  - started integrating stauth's provisioning flow into stprov: stprov!30
- jens:
  - code review
  - thinking about (distributed) CI/CD
  - thinking about release issues/improvements
  - preparing topics for the meetup
  - getting into UKI
- kfreds:
  - many conversations about st and sigsum, with many different types of people;
    e.g., both academic and industry backgrounds with loads of experience. Core
    things sigsum, st, tillitis is depending on -> folks seem to thing it is
    spot on
  - today preparing for the meetup
- ln5:
  - stprov/stboot debugging
  - docs debugging
- nisse: managed to get stboot as init process merged, now stuck on some
  testing/integration problems

## Decisions

- Decision: Adopt proposal on cleaning up the selection of TLS signing roots
  - https://git.glasklar.is/system-transparency/project/documentation/-/merge_requests/47

## Next steps

- zaolin (async):
  - finishing all remaining presentatons + proposal before meetup tomorrow.
  - checking out new hpe hardware and meeting Linus in the office
- kai (async):
  - integrate stauth into stprov (longer term project)
  - decide how and where to run integration tests (meetup)
  - resolve the di (dependency injection) issue nisse discovered
  - (whatever comes up the the meetup)
- jens
  - finish notes for meetup
  - Stockholm meetup
- kfreds:
  - preparing for the meetup
- rgdd: unsure other than some more review/merging and creating issues based on
  the above proposal; meet-up and then see what the opportunity is.
  - issue#1: update docs.sto based on proposal
  - issue#2: the "one line" changes in our code base
  - is anyone interested in implementing the change we just decided on?
    - jens will do it

## Other

- Regarding ST, what would you like to accomplish at this meetup?
  - Retrospective on how it's going
    - How it's going
    - Where we're going
    - Has anything changed since last time?
    - Who maintains what things in ST?
    - +1 from Linus, Jens
  - For all of us to understand TPMs a lot better and hopefully decide (make
    proposals on) how we're going to use TPMs and not use TPMs
  - Same as above but for Secure Boot
  - Philipp is presenting on mkosi
    - Rasmus would like to understand how that interfaces with Sigsum
- If you had to put down an ST roadmap on paper right now, what is top of mind?
  - Make ST work again
    - It's not possible for anyone of us to boot anything on real hardware
    - The documentation is in flux. We don't know what is documented.
  - Fix the processes for how we work, and how we handle proposals. We're not
    sure of how we work. We need to re-evaluate and fix some things there.
    - Testing is a big thing, but we don't have a plan for that yet.
  - The list from November 6th is still relevant.
- What annoys you the most?
  - That we're not all running ST easily on real hardware. That's making us less
    productive when we work. It annoys me that we're not all on that place. It
    makes it hard to know what we're doing. /R
  - That ST is not working on real hardware. /L
  - There's still a lot of confusion around interfaces, fitting things together,
    and testing.
    - Changes Kai did to test stboot, supposed to be test-only changes, but
      breaks stprov. We're still a bit confused on which the interfaces are.
    - This goes back to the maintainership story.
  - LN: We have several interfaces. Sometimes we have API breakage, and
    sometimes it's other interface breakage, e.g. in the host config file.
    Mentioned some examples of how we're not in good shape. Assigning a
    maintainer is not enough. We haven't reached the bottom of the barrel of
    integration between components.
  - Jens: My concern is that making all these changes should be done one at a
    time. How to make all these changes? This conflicts with people asking to
    try it out. We're not ready for that at the moment. Some people are really
    good at structuring small changes. We're good at getting things fixed.
    - We need to decide how we do it.
  - rgdd: i think that the list from Nov 6 is still quite valid, i would move up
    (10) and (11) and frame that as what linus said:
    https://git.glasklar.is/system-transparency/project/documentation/-/blob/main/archive/2023-11-06-roadmap.md?ref_type=heads#roadmap
