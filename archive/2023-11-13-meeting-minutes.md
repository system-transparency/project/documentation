# System Transparency weekly

- Date: 2023-11-13 1200-1250 UTC
- Meet: https://meet.sigsum.org/ST-weekly
- Chair: Fredrik
- Secretary: rgdd

## Agenda

- Hello
- Status rounds
- Decisions
- Next steps
- Other

## Hello

- rgdd
- kfreds
- ln5
- quite
- jens
- philipp
- nisse

## Status rounds

- rgdd: mostly done with 'Update website and mailing lists'
  - Mailing lists: we now have st-announce and st-discuss
  - www.sto site is only missing a landing page
  - blog not to be placed on www.sto, separate site and template. See template:
    https://git.glasklar.is/rgdd/blog
- rgdd: about to cut stprov release
- nisse:
  - stprov fixes, merged stmgr date format change
    https://git.glasklar.is/system-transparency/core/stmgr/-/merge_requests/49
  - Would like jens' review on
    https://git.glasklar.is/system-transparency/core/stboot/-/merge_requests/110,
    needed for trying out stboot as init.
- kfreds:
  - 2023-11-06-roadmap.md LGTM!
  - Gave feedback on various GL issues
  - Worked on the blog (colors, tools (Procreate, Excalidraw (wow)))
  - Spent most of my time on non-ST things
- quite: begin to dig into our current services config management, researching
  some OSpkg needs. Also got a new test server actually with a TPM to prepare
  with SB certs etc.
- jens:
  - preparing releases for stboot and stmgr
  - worked on wrapping up status-quo documentation
  - sketching and discussing releases (improvements) with rgdd in general
- philipp:
  - Merge and resolved all remaining MRs.
  - Switched to new repository.
  - Made PR for adopting the old documentation repo.
  - Preparation for first release, which will happen today.
  - Add ST's brand assets
  - Planning release of doc repo ~today (RELEASES, NEWS, and overview of what is
    included. Not sure I can make it with the tags of other repos this time.)
- ln5:
  - have the HPE system, still not unpacked (would like input from philipp if
    there's something that should be setup ~today isch, otherwise won't have
    time until friday)
  - various sysadmin tasks
  - prepared for this weeks meetings with Mullvad

## Decisions

- None

## Next steps

- rgdd: cut stprov release, other than that nothing else other than
  input/feedback this week and the next (let me know what/if you need anything
  from me)
- kfreds:
  - Continue working on the blog
  - Landing page for ST website
- quite: more of the same. Looking forward to learn a little more about TPM in
  practice
- ln5: meetings with users; setting up HPE system; setting up routers (ST
  booting)
- jens: finalize releases for stmgr, stboot. Depending on what is discussed in
  other section. Will fininish wrap-up issues on documentation milestone. And
  then like 10 other GL items regarding MRs and stboot.
- philipp: work on getting documentation (still have lots of issues, like 20+,
  see what's done and not; then solve the most obvious ones). Then will need to
  do some more planning regarding the other milestones I'm taking care of.
- nisse: low-hanging stboot fixes (see below)

## Other

- stboot TPM issue? Purpose of the stboot release? (jens)
  - Option: could get kai's MR merged, but very large. And kai not here.
    - No
  - Option: could bring in the fix from kai's branch
    - Yes (nisse will work on it)
  - Option: point out that it's broken and let it be (not a preferred option)
    - No
  - Purpose of the release - status quo release for stboot.
  - quite have a comment on a tiny fix, jens will open an issue and give nisse
    the info he needs to fix this.
- ln5 and rgdd would appreciate an update from nisse on how it's going with the
  many minor issues in 'Changes not altering any interfaces' milstone
  - https://git.glasklar.is/groups/system-transparency/-/milestones/8
  - E.g., https://git.glasklar.is/system-transparency/core/stboot/-/issues/137
    and https://git.glasklar.is/system-transparency/core/stboot/-/issues/135
    seems trivial, would be really nice to get fixed sooner rather than later!
  - Short answer: havn't looked at stboot yet, and it's coming up now.
- ST on Hacker News yesterday:
  - https://news.ycombinator.com/item?id=38225724
  - someone posted a link to www.sto yesterday, fredrik participated in the
    discussion
- Videos and slides from OSFC are public. Relevant to our interests according to
  F:
  - Tamago and TinyGo by Ron Minnich
    - https://www.osfc.io/2023/talks/tamago-and-tinygo-working-with-firmware-and-kernel-written-in-go/
    - I think Go as firmware has immense potential, which is partly why I did a
      deep-dive into Go's runtime and build chain during the summer. Please
      watch this. /Fredrik
  - Towards authentication of transparent systems (by Fredrik)
    - https://www.osfc.io/2023/talks/towards-authentication-of-transparent-systems/
    - I'm glad I presented because it caused me to make progress on some things.
      Having said that, the presentation is all over the place, and I'm not
      happy with how I answered (or rather didn't answer) some of the questions.
      If anything this makes me more motivated to write as a means of
      communication. If/when I present somewhere again, it'll be based on things
      I've already written about publicly on the blog. /Fredrik
  - Reproducible builds by Vagrant Cascadian
    - https://www.osfc.io/2023/talks/reproducible-builds-all-the-way-down/
  - Shoutout to Jens and mc who held workshops on ST and TKey
    - https://www.osfc.io/2023/talks/getting-started-with-system-transparency/
    - https://www.osfc.io/2023/talks/develop-for-tkey/
  - TKey project update by Sasko
    - https://www.osfc.io/2023/talks/tkey-project-update/
- Let's try out Excalidraw? Fredrik planning to try it out.
  - Excalidraw is "an open source virtual hand-drawn style whiteboard.
    Collaborative and end-to-end encrypted."
  - https://excalidraw.com/
  - https://github.com/excalidraw/excalidraw
  - I've been looking for something like Excalidraw. Something we could use to
    make design artifacts such as flowcharts. I can see us using it for
    prototyping, design review, specification, visual documentation and other
    communication purposes.
  - I'm going to try it out using their publicly available instance.
  - q: Can it be used in batch mode, .excalidraw --> png/svg/pdf ? If we start
    using it, should we consider self-hosting?
- Doc license? Considering CC-BY-SA. + copyleft-style, - complex, - different
  from code license, diffcult if we want to copy text back and forth between
  code and docs.
  https://git.glasklar.is/system-transparency/project/www/-/issues/5
  - opinions? Please write on the issue! We will need to decide on this
    eventually.
