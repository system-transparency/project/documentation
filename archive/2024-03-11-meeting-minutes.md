# System Transparency weekly

- Date: 2024-03-11 1200-1250 UTC
- Meet: https://meet.sigsum.org/ST-weekly
- Chair: rgdd
- Secretary: rgdd

## Agenda

- Hello
- Status rounds
- Decisions
- Next steps
- Other

## Hello

- rgdd
- kai
- jens
- kfreds
- zaolin
- nisse
- ln5

## Status rounds

- kai: implemented the command and structure encoding/decoding for host \<-> TPM
  communication, w/ zero dependencies. I'm still working on the
  ActivateCredential crypto.
- rgdd: got the TLS roots file updates, can be tested by early adopters on
  stboot@v0.3.2 and stprov@v0.3.1.
  - Warning: requires build environments to update this file path !
  - https://git.glasklar.is/system-transparency/project/docu2mentation/-/blob/main/proposals/2023-12-05-clean-up-ca-selection.md?ref_type=heads
- rgdd: in the process of getting the above done, i also did a bit of doc
  clean-up in
  https://git.glasklar.is/system-transparency/core/system-transparency/. Most
  notably adding a warning of our level of support for this repository at the
  top of the README.
- rgdd: got the final backwards-compatible hostcfg things merged, also now
  available in stboot@v0.3.2 in case anyone wants to try that early as well.
- rgdd: drafty updates to OS package documentation (under review)
  - https://git.glasklar.is/system-transparency/project/docs/-/merge_requests/11
- nisse: Started on stboot NEWS entries:
  https://git.glasklar.is/system-transparency/core/stboot/-/merge_requests/165
- nisse: Updated stboot integration test to use signed images from
  https://st.glasklar.is/st/qa/
- ln5: set up signing machinery pulling CI/CD artifacts from gitlab, signing
  them, and uploading signed artifacts to st.glasklar.is/st; the qa-images repo
  will continue publishing images but they will be unsigned
- ln5: debian snapshot revival project slowly starts moving
  - service required for R-B, to be able to reproducibly build stuff (e.g.
    package built with gcc from the other year...and you need to be able to pull
    old stuff. So this is about snapshots of package repos.)
  - if you know anyone that has 200TB of disk sitting and they don't want to use
    it for anything, please let linus know! Preferrably a service (S3, etc.)
- jens: coreboot support QEMU done, tested and documented:
  https://git.glasklar.is/system-transparency/platform-plumbing/st-platform-features/-/tree/main/coreboot-support?ref_type=heads
- kfreds: Nothing to report.
- zaolin: Replacing further stboot components with systemd/os tools. Networking
  already works, including bonding. Wrote code for ospkg execution, still WIP.

## Decisions

- None

## Next steps

- kai: finish the ActivateCredential implementation, add docs and more unit
  tests. Then start with the high level interactions outlined in the Remote
  Attestation design doc. I'll start with creating and certifying keys and move
  to verifying the event log.
- rgdd: stprov milestone (need a simple stprov.md and finalize stprov @ hw
  testing)
- jens: coreboot support HiFive Unmatched board
- zaolin: Testing kexec, ospkg handling and other components to execute an ospkg
  successfully in qemu. Trying to get full stboot compliance with remaining
  components.
- ln5: release planning w/ rgdd and nisse, to pin down exactly what needs to be
  done / is remaining to get out the "status quo" release
- ln5: setting up a debian snapshot development/testing/staging environment
- ln5: have a talk on a conference in stockholm (network ops audience) -- not on
  st though (tor and anti-censorship).
- kfreds: Take care of the proposals, issues and MRs that are in limbo, some of
  which I've created. More technical stuff.
- nisse: Sync on release stuff, stboot docs and tests.

## Other

- Fredrik: have a few quick questions.
  - Want to test the entire flow from uefi stdboot ospkg, including SB and TPM
    functionality. Can I do all of that in QEMU? Does qemu have support for
    UEFI, SB, and TPM?
  - "Yes" /kai&philipp. See the stprov systemd demo, sets up this kinda things.
- Fredrik: to linus/philipp/kai/jens: about server brands. Philipp and kai have
  said recently SM is not great.
  - Kai: hw and firmware is the most affordable entry-level series for server
    system, so if you want something to depend on this is the cheapest option.
    Then a gap in price to e.g. HPE and others. But SM are fine, but these
    things don't have as good reliability and long-term maintenance and
    replacement parts that other vendors provide.
  - Philipp thinks security-wise SM is not a good pick. Same with Gigabyte.
- Fredrik: so what is so bad about security in SM security wise?
  - Philipp: bmc and uefi. Software wise the quality running on it, not good.
    Compared to HPE which is tested and well-proven / reliable.
- Fredrik: so from ops perspective, comparing SM and HPE?
  - SM: Kai says the BMC is unusable... Whereas HPE, Dell, and ... are better
    options.
  - ln5: (somewhat old intel, but) people with budget usually goes for HPE and
    Dell -- e.g. to get three years serice level. When a part breaks, there will
    be a replacement part quickly. But agrees with SM being entry level, as
    stated above.
