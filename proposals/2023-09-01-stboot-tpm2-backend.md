**NOTE:** This document is a proposal of how things could be implemented. It
differs from the status quo in that the trust policy is not signed by compiled
directly into the stboot image.


System Transparency Deployment and Security Models
==================================================

This document describes the two deployment methods supported by System
Transparency's boot loader stboot: **self-signed** and **TPM-backed**. Both
methods load the same OS packages signed with user-supplied keys, either over
the network or from a disk.

The following figure explains the components common across both deployment
methods. The boot sequence starts with the proprietary UEFI Secure Boot which
verifies the Microsoft-signed stboot/shim application. UEFI Secure Boot
requires all boot loaders to be signed by a that is in the UEFI Key Exchange
Key database (KEK). In practice, this is a key controlled by Microsoft. For
details refer to the [Secure Boot
documentation](/explanation/secureboot_concepts). The bundled shim would have a
single, Glasklar controlled, key compiled in via the VENDOR_DB_CERT define. See
[shim Authentication Flow](/explanation/shim-keys) for details.

After starting, stboot retrieves the trust policy and verifies it's signature
using the trust policy key. It consults the trust policy for the URL of the OS
package. After downloading stboot verifies that the OS package is correctly
signed by a number of signing keys. All these keys need to be certified by the
CA in the trust policy. After verifying the OS package it is booted.

```mermaid
flowchart LR
    classDef glasklar stroke:#54B1E1,fill:#265A78,color:#ccc
    classDef proprietary stroke:#F7503E,fill:#871B0F,color:#ccc
    classDef measured fill:none

    uefi[UEFI Secure Boot]:::proprietary
    uefi-->|Verifies|ms

    subgraph ms["&nbsp;&nbspSigned my Microsoft&nbsp;&nbsp"]
        shim[shim code]:::glasklar
        mok[MOK]:::glasklar
    end
    mok-->|Verifies|glasklar
    shim-->|Executes|stboot

    subgraph glasklar[Signed by Glasklar]
        stboot[stboot code]:::glasklar
    end

    subgraph ospkg[OS package]
        uki[Unified kernel image]
    end
    stboot-->|Executes|uki
    policy[Trust policy]

    root((Trust policy key))
    root-->|Verifies|policy
    glasklar-. Linked .- root

    subgraph policy[Trust policy]
        ca[CA]
        url[OS package URL]
    end
    ca-->|Certifies|key1
    ca-->|Certifies|key2
    ca-->|Certifies|key3
    url-->|Points to|ospkg

    subgraph ospkg[OS package]
        key1[Key A]
        key2[Key B]
        key3[Key C]
        sig1[Signature A]
        sig2[Signature B]
        sig3[Signature C]

        key1-->|Verifies|sig1
        key2-->|Verifies|sig2
        key3-->|Verifies|sig3
        sig1-->|Signs|uki
        sig2-->|Signs|uki
        sig3-->|Signs|uki
        uki[Unified kernel image]
    end
```

The trust policy is signed with the trust policy key. The way this trust policy
key is secured from being changed by an attacker is the main difference between
the two deployment methods. Both models assume UEFI Secure Boot is enabled.

Not pictured is the host configuration that defines how stboot connects to the
network as this is not security relevant. The figure also skips the secure data
that contains secrets for the OS package. This data structure and it's use is
described later.

### TPM 2.0 Backed Deployment

Utilizing the TPM 2.0 backed deployment method is the easiest and fastest way
to enable System Transparency. It uses official, signed stboot releases from
the System Transparency project. The official stboot release binaries are
signed with the Microsoft Secure Boot key and run on any off-the-shelf UEFI
system without changes. The released stboot binaries can be built reproducibly
to verify no backdoors were inserted.

The following section will use various TPM concepts without explaining them in
detail. It is assumed that the reader is familiar with the [TPM
primer](/explanation/tpm-capabilities) or the TPM 2.0 specifications.

In a TPM backed deployment the user-generated trust policy key is stored in
the TPM NVRAM. As part of the provisioning process the platform owner stores
the public key of the trust policy signing key in the TPM. During boot,
stboot will read the key from the TPM and verify that the trust policy to be be
used is signed by this key. The space the public key occupies in NVRAM will be
write protected.

The TPM-backed deployment method supports remote attestation that

* ensures stboot and a specific OS package have been booted on the platform,
* provides a unique hardware identity and,
* binds a human-readable identifier to the platform.

This deployment method requires a TPM 2.0 (discrete or in firmware) on the
platform.

The figure below depicts the boot sequence of the TPM-backed deployment method.
The trust policy key is securely stored in the platform's TPM NVRAM and is set
to read-only access. Similarly, the human-readable name of the platform is also
stored in the TPM NVRAM, serving as an identifier for the machine in the user
interface. Prior to executing the OS package, the signing key and identity are
measured into the TPM to establish a link with the attestable state of the
platform.

```mermaid
flowchart LR
    classDef glasklar stroke:#54B1E1,fill:#265A78,color:#ccc
    classDef proprietary stroke:#F7503E,fill:#871B0F,color:#ccc

    uefi[UEFI Secure Boot]:::proprietary
    uefi-->|Verifies|ms

    subgraph ms["&nbsp;&nbspSigned my Microsoft&nbsp;&nbsp"]
        shim[shim code]:::glasklar
        mok[MOK]:::glasklar
    end
    mok-->|Verifies|glasklar
    shim-->|Executes|stboot

    subgraph glasklar[Signed by Glasklar]
        stboot[stboot code]:::glasklar
    end

    stboot-->|Trusts|tpm
    subgraph tpm[TPM 2.0]
        direction TB
        root[Trust policy key]
        uxid[Human-readable ID]
    end
    root-->|Verifies|policy

    policy[Trust policy]
```

Figure: Overview of an stboot system using the TPM as secure key storage.
Proprietary code is marked red and Glasklar provided artifacts are blue.

If an attacker successfully breaches a stboot'd platform, they can freely
interact with the TPM, giving them the ability to write their own trust policy
signing key. This allows them to boot any OS packages of their choosing. This
is possible because stboot trusts any key found at TPM NV index 0x01aa0001.
These NV indices can be altered in two ways: by using the write command to
modify an existing index or by clearing the TPM, which erases all
non-firmware-controlled indices and creates a new index in the now available
spot.

To prevent the overwriting of the NV index, it is configured with a policy that
requires authenticated write access. To satisfy this static policy, the index
must be all zeros and marked as written to be cleared, effectively making it
write-once. However, since the provisioning phase of stboot does not have
firmware-level permissions, the index cannot be marked as firmware-controlled
and can always be deleted using the clear command. To address this
vulnerability, stboot disables the clear command until the next power cycle. As
long as the boot chain from UEFI firmware to stboot remains trustworthy, this
precaution prevents anyone other than those with access to the UEFI console
from clearing the TPM.

### Attacker Model

The attacker aims to trick the stboot boot loader into loading an OS package
signed by them. To achieve this **attackers can**:

1. Read, manipulate, remove, reorder, and inject all network and USB traffic to
   and from the platform.

2. Shutdown or start the platform at any time.

3. Due to (1) and (2), attackers can boot arbitrary code on the system. That
   code has full access to all platform components, including the TPM, disks,
   network cards, etc.

4. Read, but not write, all contents of the boot flash on the platform,
   including the BIOS region.

5. Read, write, and delete all entries in the platform NVRAM, but not the TPM NVRAM.

6. During the boot flow, the attacker has read and write access to the
   platform's serial console.

7. Read all traffic between the host CPU and the platform TPM.

Attackers **cannot**:

8. Manipulate or inject traffic between the platform CPU and the TPM. This
   vulnerability is irremediable because the TPM measurements we rely on are
   unauthenticated. An attacker with access to the TPM bus can falsify them.

9. Prevent the platform from sending measurements to the TPM either by
   disabling the responsible code or severing the connection to the TPM.

10. Write or delete parts of the platform BIOS flash. This restriction can be
    lifted if the entire UEFI code and data are protected by Intel Boot Guard
    and UEFI Secure Boot and TPM measurements are unconditional.

11. Compromise any cryptographic primitives and protocols used, as well as the
    platform security mechanisms like Boot Guard and the TPM firmware.

12. Remove or replace the platform TPM or access its internal state.

In summary, the only off-limits parts of the system are the operator's
computer, the bastion host, and the core complex of the platform: CPU, system
flash, TPM, and BMC. All other interfaces, such as Serial, network, USB, and
disks, can be controlled by an attacker without jeopardizing the secure data on
the platform.

```mermaid
flowchart LR
    classDef secure stroke:red,fill:darkred,color:#ccc

    subgraph platform[Server platform]
        direction TB

        cpu[CPU]
        flash[UEFI flash]
        tpm[TPM 2.0]
        bmc[BMC]
    end
    class platform secure

    usb[USB ports]
    disk[Disk contents]
    serial[Serial console]

    disk---platform
    usb---platform
    serial---platform

    ospkg[OS package server]
    intnet((Internal\nnetwork))
    extnet((External\nnetwork))
    bastion[Bastion server]:::secure
    operator[Operator's computer]:::secure

    ospkg---intnet
    intnet---platform
    intnet---bastion
    bastion--- extnet--- operator
```

Figure: Overview of an stboot'd system with all entities marked red that need
protection from an attacker.

### Provisioning Flow

Before booting OS packages, a platform needs to be provisioned. The general
steps are:

1. Creating a host configuration describing the platform's network
   configuration.

2. Installing `stboot` and the `shim` boot loader on the platform.

3. Generating and certifying the necessary TPM-backed keys and NVRAM objects.

4. Creating and encrypting the secure data for the platform.

Provisioning involves two parties: the operator's computer and the platform
being provisioned. Both are outside of attacker's reach during provisioning.
The operator uses their computer to certify TPM-generated objects and to sign
the trust policy for the platform.

During provisioning, the platform has internet access and connects to the
operator via the network. The operator initially connects to the platform via a
serial or graphical console.

The detailed flow is as follows:

1. (Platform) Boot an off-the-shelf Linux distribution with efivar and TPM
   support. It should have stprov installed.

2. (Platform) Use stprov to create a host configuration and write it into the
   STConfig EFI variable.

3. (Platform) Download the stboot and shim binaries to be deployed and copy
   them to the EFI System Partition.

4. (Platform) Use efibootmgr to create a boot menu entry for stboot.

5. (Platform) Generate a Storage Root Key (SRK) and Attestation Identity Key
   (AIK). Define the trust policy signing key and human-readable identity
   indices. Send their public keys, the TPM Endorsement Key, and its certificate
   to the operator.

6. (Operator) Verify the EK certificate, SRK, and AIK. Generate the inputs for
   TPM 2.0 credential activation and set the credential to a nonce. Send the
   credential to the platform.

7. (Platform) Activate the sent credential for the AIK and retrieve the nonce.
   Send the nonce and the TPM 2.0 event log to the operator.

8. (Operator) Send the unencrypted secure data, human-readable identity, signed
   trust policy and the public part of the trust policy and secure data
   authorization signing keys to the platform.

9. (Platform) Write the trust policy signing key and human-readable identity
   into their respective NVRAM indices. Certify the contents and configuration
   of both NV indices using the Attestation Key.

10. (Platform) Seal the secure data and erase the plaintext secure data from
    platform memory. Certify it's creation using the Attestation Identity Key.

11. (Platform) Send the certification structures back to the operator.

12. (Operator) Verify the NVRAM and Sealed Data Object certifications. Compute
    the policy digests for the secure data using the event log for all OS
    packages that should have access to the data. Send the signatures to the
    Platform.

13. (Platform) Write provision state to EFI variables.


After provisioning, the following EFI variables are written:

- STConfig: JSON host configuration

- TrustPolicy: signed trust policy with OS package URL and root CA public key

- SecureData: The sealed, read-only, platform specific private data that can be
  unsealed by an OS package.

- AttestationKey: The encrypted key structures of the Attestation
  Identity signing key used to vertify other TPM objects during provisioning.

- AuthorizationKey: Public part of the key signing the secure data's policy
  authorization.

- SecureDataAuthorization: Signature for the TPM2_PolicyAuthorize command
  required when unsealing SecureData. Also contains the PCR values accepted by
  the current authorization for easier debugging.

Also, the following NV indices are defined during provisioning:

- NV Index 0x01AA0001 that contains the trust policy public signing key.

- NV Index 0x01AA0002 that contains the human-readable identity string.


### Boot Flow

After provisioning, the System Transparency-enabled platform automatically
boots OS packages from either a disk or network. If the platform hasn't been
tampered with after booting, the OS package can unseal the secure data.

The boot flow has minimal branching and is designed for simplicity. After
initializing the platform, UEFI will boot the shim boot loader from the disk,
which subsequently boots stboot. The stboot boot loader fetches the trust
policy's signing key from the TPM. The trust policy is fetched and verified.
The trust policy contains the URL of the OS package to be booted. The OS
package is then fetched, verified, and booted.

The following pseudo Go code illustrates the boot flow of the TPM-backed
deployment case.

```go
// configure the network
hostCfg := ReadEfiVariable("STConfig", STBOOT_GUID)
ConfigureNetwork(hostCfg)

// fetch trust policy and it's signing key
trustPolicyKey := ReadTpmNvIndex(0x01aa0001)
if trustPolicyKey == nil {
  // Enter rescue mode
  Measure(12, UNPROV_ERROR)
  Measure(13, UNPROV_ERROR)
  Measure(14, UNPROV_ERROR)

  Reboot()
}

// load and veriy the trust policy
trustPolicy := ReadEfiVariable("TrustPolicy", STBOOT_GUID)
if VerifySignatures(trustPolicy, trustPolicyKey) == fail {
  // Enter rescue mode
  Measure(12, SIG_ERROR)
  Measure(13, SIG_ERROR)
  Measure(14, SIG_ERROR)

  Reboot()
}

Measure(12, trustPolicy)

// Fetch and verify ospkg
ospkg := HttpFetch(trustPolicy.Url)
if VerifySignatures(ospkg, trustPolicy.RootCa) == fail {
  Measure(12, SIG_ERROR)
  Measure(13, SIG_ERROR)
  Measure(14, SIG_ERROR)

  Reboot()
}

Measure(13, ospkg)

// Fetch and measure UX identity
uxIdentity := ReadTpmNvIndex(0x01aa0002)
Measure(14, uxIdentity)

// Prevent the OS from clearing the TPM
TPM2_ClearControl(TPM_RH_LOCKOUT, YES)

// OS package takes control
Kexec(ospkg.Uki)
```

### Confidential Data

After the system boots up, the OS packages require access to cryptographic keys
or other sensitive information. It is crucial that these secrets remain
inaccessible to attackers, even if they manage to run any code on the platform.
In a TPM-backed deployment, this can be achieved by employing a similar method
as used for connecting the trust policy key to stboot. Instead of storing the
secure data in the TPM NVRAM, it is encrypted using a TPM resident key. This
key can only be accessed if the OS package is booted by a legitimate stboot
release.

To access the secure data, the OS package utilizes the Unseal operation. This
operation decrypts the secure data only if the PCR values in the TPM match
those of a legitimate stboot release, and optionally, one of the predetermined
OS packages.

The following figure demonstrates the flow of unsealing the secure data that
the OS package executes. The secure data can only be unsealed after the system
has booted into an OS package, provided it has been booted by a preconfigured
combination of platform firmware and stboot.

```mermaid
flowchart LR
    classDef glasklar stroke:#54B1E1,fill:#265A78,color:#ccc
    classDef proprietary stroke:#F7503E,fill:#871B0F,color:#ccc
    classDef measured stroke:green,fill:none

    subgraph measured[Measured]
        uefi[UEFI Secure Boot]:::proprietary
        uefi-->|Verifies|ms

        subgraph ms["&nbsp;&nbspSigned my Microsoft&nbsp;&nbsp"]
            shim[shim code]:::glasklar
            mok[MOK]:::glasklar
        end
        mok-->|Verifies|glasklar
        shim-->|Executes|stboot

        subgraph glasklar[Signed by Glasklar]
            stboot[stboot code]:::glasklar
        end

        subgraph ospkg[OS package]
            uki[Unified kernel image]
        end
        stboot-->|Executes|uki
    end
    class measured measured

    secure[Secure data]

    measured-. Sealed .-secure
    ospkg-->|Consumes|secure
```

The following Go pseudocode illustrates the unsealing flow. After decrypting
the secure data, the OS package can measure parts of it (e.g. public keys) to
bind them to the platform state and make them part of remote attestation.

```go
// load state of EFI variables
authPubKey := ReadEfiVariable("AuthenticationKey", STBOOT_GUID)
secureData := ReadEfiVariable("SecureData", STBOOT_GUID)
secureDataAuth := ReadEfiVariable("SecureDataAuthorization", STBOOT_GUID)

// compute policy digest
session := StartPolicySession()
PolicyCommandCode(session, TPM2_CC_Unseal)
PolicyPCR(session, 0..23)
PolicyAuthorize(session, authPubKey, "SECURE DATA UNSEALING NONCE", secureDataAuth)

// decrypt the secure data object
plainSecureData := Unseal(session, secureData)

Measure(15, secureData.DataChannelPublicKey)

// Actions dependent on the OS package
```

### Reseal Flow

Decrypting the secure data object requires all PCR values to match those
computed during provisioning. Modifying the platform firmware, replacing
components, or altering the UEFI configuration will change the PCR values. If
this occurs, decryption of the secure data will fail since they expect the PCR
to be unchanged. The reseal flow outlines the recovery process, assuming the
change was intentional.

During resealing, new authorizations that encompass the updated PCR values are
generated. These authorizations are signed by the authorization private key
located on the operator's computer. The general procedure involves booting a
standard Linux distribution with stprov installed. Once connected via console,
the operator utilizes stprov on both the platform and their computer to
regenerate the signed authorization messages.

The detailed flow is as follows:

1. (Platform) Boot a standard Linux distribution with efivar and TPM support.
   Ensure stprov is installed.

2. (Platform) Obtain the TPM 2.0 event log and forward it to the operator.

3. (Operator) Compute the new policy digests for the sealed secure data object,
   sign the authorizations for those values and relay them to the platform.

4. (Platform) Save the authorizations to their corresponding EFI variable.

5. (Platform) Reboot into stboot.

It's important to note that the secure data blob does not need re-provisioning
during resealing.  Resealing merely reissues the authorization specifying the
correct PCR values.

## Self-signed Deployment

Instead of relying on pre-built stboot binaries, platform owners have the
option to compile stboot independently. In this scenario, the trust policy keys
can be integrated directly into stboot, rendering the use of the TPM
unnecessary.

The basic boot flow of the self-signed deployment is shown in the figure below.
The shim/stboot binary is signed with either the Microsoft Secure Boot key or a
custom key provisioned beforehand. The trust policy key is part of the signed
stboot binary and is simply read from memory. After verifying the trust policy
with it, the OS package is retrieved, it's signatures are verified and it's
booted.

```mermaid
flowchart LR
    classDef owner stroke:#54B1E1,fill:#265A78,color:#ccc
    classDef proprietary stroke:#F7503E,fill:#871B0F,color:#ccc

    uefi[UEFI Secure Boot]:::proprietary
    uefi-->|Verifies|ms

    subgraph ms["&nbsp;&nbspSigned my Microsoft&nbsp;&nbsp"]
    shim[shim code]:::owner
    mok[MOK]:::owner
    end
    mok-->|Verifies|owner
    shim-->|Executes|stboot

    subgraph owner[Signed by Platform owner]
    stboot[stboot code]:::owner
    root[Trust policy key]
    end
    root-->|Signs|policy
    policy[Trust policy]
```

As mentioned above, for a self-signed binary to operate under Secure Boot, it
must bear a signature from a key recognized by UEFI. There are two avenues for
this:

1. Submit the stboot-integrated shim boot loader for a [shim review
   process](https://github.com/rhboot/shim-review) and secure a signature
   through the Microsoft Secure Boot key.

2. Alternatively, platform owners can produce their own Secure Boot keys.
   Notably, a significant portion of UEFI-equipped devices available in the
   market facilitate the provisioning of user-generated keys. Upon provisioning
   with custom keys, platform owners have the autonomy to sign their individual
   stboot versions.

In both scenarios, the UEFI-trusted key can be used to sign the application,
either the shim or stboot. If the shim is used, it will have a key, either
pre-compiled or retrieved from an EFI variable, which verifies stboot. In the
other case, stboot will be booted directly without using the shim.

### Attacker Model

The attacker aims to trick the stboot boot loader into loading an OS package
signed by them. To achive this **attackers can**:

1. Read and reorder, and inject all network traffic to and from the
   platform.

2. Shutdown or start the platform at any time.

3. Read all traffic between the host CPU and the TPM.

4. Read, but not write, all contents of the boot flash on the platform,
   including the BIOS region.

5. During the boot flow, the attacker has read and write access to the
   platform's serial console.

**Attackers cannot**:

6. Write or delete parts of the platform BIOS flash. This restriction can
   be lifted if the entire UEFI code and data are protected by Intel Boot Guard
   and UEFI Secure Boot.

7. Compromise any cryptographic primitives and protocols used, as well as
   the platform security mechanisms like Boot Guard.

8. Access the external interfaces like USB to boot another operating
   system. This is only applicable in case the platform still trusts the
   Microsoft KEK.

9. Deploy their own Secure Boot keys e.g. by accessing the UEFI shell and
   asserting physical presence on the device.

The attacker model of the self-signed deployment does not involve the TPM in
any way and thus it grants the attacker complete control over it. On the other
hand, the whole security of the system hinges on UEFI Secure Boot and the
security of both it's key store and configuration. When the pre-provisioned
Secure Boot keys are not replaced, any off-the-shelf operating system will be
able to boot on the platform. Combined with the fact that no secure credential
store is available makes it impossible to defend against an attacker with
access to any external (physical or software) interface that can act as a boot
source.

### Enrollment Flow

In order to deploy an self-signed stboot, the platform owner must make sure
that the correct signing keys are deployed. They can do this using the UEFI
menu or UEFI shell. Most implementations require a confirmation by pressing a
physical button on the case or through the BMC interface. Afterwards the stboot
application can be copied to the platform, set as default boot target and the
host configuration is written. Because the trust policy key is included in the
stboot application no additional provisioning has to take place. The detailed
flow is as follows:

1. (Platform) Boot a standard Linux distribution with efivar support. Ensure
   stprov is installed.

2. (Operator, optional) Generate a new UEFI Platform Key and sign the custom UEFI Key
   Exchange Key with it. Send both to the platform.

3. (Platform, optional) provision the new Platform Key and Key Exchange Key through the
   UEFI Secure Boot setup interface. Reboot the platform.

4. (Platform) Use stprov to create a host configuration and write it
   into the STConfig EFI variable.

5. (Platform) Download the stboot and shim binaries to be deployed and copy
   them to the EFI System Partition.

6. (Platform) Use efibootmgr to create a boot menu entry for stboot.

### Boot Flow

The boot process for the self-signed scenario is straightforward. The trust
policy key is compiled-in and easily loaded from memory. Its integrity is
protected by UEFI Secure Boot, preventing any unauthorized changes. Once the
trust policy is verified, the stboot retrieves and verifies the OS package
before proceeding to boot.

```go
// configure the network
hostCfg := ReadEfiVariable("STConfig", STBOOT_GUID)
ConfigureNetwork(hostCfg)

// load and veriy the trust policy
trustPolicy := ReadEfiVariable("TrustPolicy", STBOOT_GUID)
if VerifySignatures(trustPolicy, TRUST_POLICY_KEY) == fail {
  Reboot()
}

// Fetch and verify ospkg
ospkg := HttpFetch(trustPolicy.Url)
if VerifySignatures(ospkg, trustPolicy.RootCa) == fail {
  Reboot()
}

// OS package takes control
Kexec(ospkg.Uki)
```

### Confidential Data

The self-signed deployment model relies on an attacker not being able to boot
their own operating system on the platform. This means that all credentials the
OS package needs after boot are stored in plain text in the platform's NVRAM.
UEFI has no mechanism to store encryption keys in a secure way and only allowing
access by some operating systems.

## Definitions

- **stboot:** A UEFI bootloader conceived by the System Transparency project.
Its primary function is to boot OS packages, but only after it validates their
signature in alignment with the platform's trust policy.

- **Platform:** Refers to a UEFI-enabled computer launched with stboot. The
domain of System Transparency predominantly concerns the host CPU, excluding
peripheral devices like NICs, GPUs, CSME, or SSDs.

- **Platform owner:** This designates the party responsible for platform
oversight, including signing OS packages and overseeing stboot installation.

- **Trust policy:** A predetermined set of regulations guiding stboot during
the OS package booting process. Elements within this policy stipulate the
minimum required valid signatures for each OS package, alongside the CA
certificate that endorses the OS package signing keys.

- **Secure data:** Denotes encrypted data exclusively accessible to the OS
package. It potentially holds private keys or other sensitive information.

- **OS package:** A digitally signed package comprising the operating system
kernel and the root file system, both of which are intended for booting by
stboot.
