# Proposal to add support for decrypting an OS package

## Motivation

To be able to safely host OS packages that contain private information
publicly, e.g., using CDN, support for decryption is required.

## Proposed changes

Add support for decrypting an OS package, including its associated descriptor,
which has been encrypted by the "age" (https://github.com/FiloSottile/age)
encryption tool, by using "age" as a new, direct dependency. Decryption is
applied on the descriptor and the OS package archive, directly after fetching
them, producing new and unencrypted archives that can be processed as usual.

The feature can be considered as an additional layer of transport encryption
and applies only to OS packages and descriptors that are fetched over the
network.

Add support for providing an "age" identity file using the X25519 recipient
type format
(https://github.com/C2SP/C2SP/blob/main/age.md#the-x25519-recipient-type) with
file name /etc/trust_policy/decryption_identities in the initramfs to enable
the proposed feature.

Use "age.ParseIdentities" (https://pkg.go.dev/filippo.io/age#ParseIdentities)
to parse the identity file, if it exists. The parser supports multiple private
keys, one per line, and ignores empty lines and ones with "#".

## Security considerations

The proposed change does not introduce obvious security risk to existing use
cases. At the same time, the attack surface increases due to the additional
logic and the introduction of the "age" library and its transitive
dependencies. Excluding "age", the changes necessary in stboot should be small
and easily verified. The "age" library is of high quality, under active
development and requires few, new dependencies, this mitigates the risk from the
increased attack surface.

Use of the feature comes with the risk that the private information in the
encrypted OS package, and its associated descriptor, becomes accessible to an
attacker who has gained access to the identity file. This risk is similar to
the one that already exists, with hosting the OS package behind password
protected HTTPS. Identity rotation can be used as mitigation, to limit the
scope.

## Backwards compatibility

The feature is intended to be backward compatible, unless the identity file is
provided. Risk of unintended use is mitigated by choosing a file name that is
unlikely to be used.

## Discussion

"age" was chosen because it is well suited for implementing this feature in
stboot. "age" is a modern, high-quality and secure file-encryption-module
implemented in Go, that provides a command-line tool and a Go-library. The
module also comes with a small footprint in terms of new dependencies. In
essence, it has all that is needed in a fitting package.

The proposal chooses to decrypt the OS package before validating signatures,
because the approach is secure and fits well in the existing stboot
implementation. From the producer side, the OS package is first signed and then
encrypted. This avoids some pitfalls with combining signing and encryption, as
described here: https://words.filippo.io/dispatches/age-authentication/ by
Filippo, the author of "age". The encryption limits the ability to verify the
OS package, as usual, to those with access to the identity file. This runs
counter to the general goal of ST to make public verification
possible. Unfortunately, this downside seems inherent in the approach.

Encryption is intended to be applied on the OS package after it is created as
usual (including signing, etc.), by using the "age" tool directly. This limits
the changes required to stboot.

Since the descriptor is closely related to the OS package, encryption should
also applied to it, for consistency.

The X25519 type was chosen for encryption over the Scrypt type, which
provides password protection,
(https://github.com/C2SP/C2SP/blob/main/age.md#the-scrypt-recipient-type)
because the former offers benefits for automated use and can support
identity rotation. As a future work, Scrypt encryption can be used on
top of the identity file as an extra safeguard.

## Implementation

See concrete implementation proposal:
https://git.glasklar.is/system-transparency/core/stboot/-/merge_requests/241
for more details.
