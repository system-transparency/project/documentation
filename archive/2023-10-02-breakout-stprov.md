Break out session pad, topic: stprov (rgdd, ln5, nisse, kai, philipp, quite)

**Be warned**: raw-dump below, not cleaned up at all!

context: stprov demo failed.

What does stprov really do.

configures networ, yes.  secret exchange? WHAT.

Came out of mullvad internel work, that wasn't even open until couple of months
ago.  The thing that is now stprov, just removed example text and a few cosmetic
changes.

primary role: we need host config to stay somewhere, and it will stay in nvram.
So we need some tool to do that.  And it is important to get the networking
configuration correct.  (Not just set ip/gateway; which interfaces are we gonna
configure, etc.)

- hostconfig (for stprov)
- hostname
- ssh private key (for ospkg, to ssh/ansible etc)

stprov contributes with randomness to the generation at the remote machine.

Operator: **very important to not have to type so much and copy-paste in the
terminal that stprov remote runs in**.  It's buggy, and you'll do it 100s of
times.

Turnaround to know if something is wrong (typed one character wrong) -> takes
long time before detected and to fix.

So goal: type as few characters as possible on the stprov remote command line.

You start the server with a secret, bootstrap a tls session with stprov local.
Now you can do the rest of provision driven from the ops local laptop.

Currently we bundle stprov in an iso; boot it and run the stuff.  Write to
EFI-NVRAM.  Then unmount, remount with stboot iso, wait, and maybe it works. =)

---

So above: now two ISOs, one for stprov and one for stboot.

---

reboots are painful and risky; (re)mount also shakey.  Sometimes bmc crashes.

Happening now: project could just treat stprov as an ospkg, and if you don't
have any disk: just put it in the initramfs.

(But we might have a bug wrt. the selection.)

This has not been tested widely.

Q: could it be just something to execute instead of stprov ospkg?

want provisioning env to be very close to stboot env, so we can verify that the
configuration (e.g., networks) works.  Same kernel, driver, etc.  "Provisioning
is doing detection", like interface - what can they do, etc.
- "As close as possible"
- And in turn, want that to be similar to the ospkg you're about to get into.

From a sec. pov: smallest possible attack surface on the provison machine.  And
keep it small and slim, so it doesn't bloat the initramfs.

Not a full-blown linux system (u-root busybox stuff) -> sometimes annoying
though, trade-off.

Everyone could roll their own stprov solution, customize wrt. what makes sense
for their environment.

Open question: do we need to reboot after provision, or could we just kexec /
"enter normal mode".

---

cons, hard reset: takes long time.  Bad for uptime.  Depending on server, can
take up to 20m.

good with reboot after provision: to know that when it is unattended and
rebooted, it will actually work.  One reboot would be enough then, which is
where we are at today.

Q: docs from the internal stprov thing?
- Would be nice to make available, linus will create an issue to make available
  somehow.  And eventually iterate on that.

Some kind of contrib/make-iso.sh + doc on how to prepare the iso.  And document
this is not really up to us, it's just one good example.  (ANd if someone uses
somethign different, would be fantastic to add that to contrib/ and run in CI /
before releases etc.)

---

Private keys in the TPM, possibly hashes of config etc if useful but nothing
else?/nisse
- kai: can use tpm for that yes
- but data has to be stored outside of tpm, it can decrypt after powercycle

stauth will increase storage reqs when adding stuff to stprov

comment: don't try to do any networkcard specific stuff in qemu, like bonding.
Don't bother with that.  But stprov will be there in some form in the future, so
some qemu tests helpful.
- rgdd will make a list of things that we could maybe test in qemu and page
  kai/linus

---

If glasklar/st can focus on providing one common use-case (regarding stprov,
initial boot/shim, etc).  One path that is useful on common systems: very
helpful.  /daniel
- Right now this is a bit hard to get.
- Same with releases, it's not easy to see what is the current release and its
  features/versions.
- Hard to follow current roadmaps / state.  Some old things seem to be around,
  but a lot of digging to just find out what can or cannot be used.
- Then just say "you can reach a different level of trust if you do..." and "you
  can tweak x, y, z" yourself...
- E.g., the "one iso" thing that is currently not working, when will that be
  released?
- So TL;DR: missing a practical user path in many places

Docs like "i want to go from qemu demo to actual machine" missing currently.

Q: is the current stboot a complete rewrite?  No, hasn't been rewritten ever to
the best of our knowledge.  But jens has been cont. refactoring.  (It used to
live in u-root, and was then pulled out.)

  - https://mullvad.net/en/blog/2019/6/3/system-transparency-future/

---

reflection from st weekly: there's no (public) text describing the overall goal
and requirements for stprov

