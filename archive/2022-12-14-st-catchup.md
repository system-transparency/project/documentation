# ST catch-up 2022-12-13 13:00-13:51

## Present at the meeting
 - Fredrik
 - Jens
 - Linus
 - Morten

## Agenda and notes

- Where are we at wrt to "stboot rework"

  - The work is represented in three milestones:
    - https://git.glasklar.is/groups/system-transparency/core/-/milestones/4
    - https://git.glasklar.is/groups/system-transparency/core/-/milestones/1
    - https://git.glasklar.is/groups/system-transparency/core/-/milestones/2

  - Jens hacking, Morten reviewing, Linus removing obstacles

  - More resources needed?

   - MPT / provisioining does not need to be accounted for wrt to this work

   - CI/CD?

     - Not right now, but Linus will get ST its own GitLab runner, to
       not interfere with Sigsum.

	 - Testing only on qemu, using code in thesystem-transparency
       pipeline, Morten will take a look at that code.

   - Testing on real hardware? Not at this point.

 - Will be done in time for the 2023-02-15 meetup.

- Where are we at wrt to ST provisioning / MPT?

  - Summary of the idea

    - [Whiteboard sketch](./2022-12-07-stprov-whiteboard.jpg)

    - One iso rather than two.

    - stboot tries to read host config, and if it fails to find a
      valid host config, it will default to a built-in st host config
      which "points back" at initramfs and uses the os-pkg found
      there, if any.

    - A valid host configuration is defined in stboot (in code and not
      in any policy language) and will probably boil down to "Do I
      have a host name and an IP address plus gateway?" and possibly
      also "Do i have ST identity and auth info?".

  - Q: Will this make things worse, from either a usabiliity or
    security perspective, from the current situation with mpt.iso and
    stboot.iso?

    - FS doesn't think this makes anything worse.

  - On host config fetching: https://git.glasklar.is/system-transparency/core/stboot/-/issues/86

  - Morten has a general idea on how to get started.

  - Linus makes morten a maintainer of the whole system-transparency
    project in GL.

- Web page

  - We've got control over the DNS records.

  - We will try to move the web page content to our own
    infrastructure. Jens will link the GH repo, for Linus to deal
    with.

  - This influences our change of go module names, in a positive way.
