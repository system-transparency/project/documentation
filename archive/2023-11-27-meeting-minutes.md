# System Transparency weekly

- Date: 2023-11-27 1200-1250 UTC
- Meet: https://meet.sigsum.org/ST-weekly
- Chair: kfreds
- Secretary: zaolin

## Agenda

- Hello
- Status rounds
- Decisions
- Next steps
- Other

## Hello

- kfreds
- zaolin
- kai
- ln5

## Status rounds

- kfreds: Nothing to report.
- zaolin: first release of documentation done after review. mkosi/uki research
  for the milestone. working on the proposal for the mkosi/uki milestone.
- kai: just came back from vacation. I'll be working on stauth milestones until
  the december meetup
- ln5: working together with community members. ST testsystem arrived and is
  ready to be setup.

## Decisions

- Nothing

## Next steps

- zaolin: finish mkosi/uki proposal and move on tasks in milestone. In parallel
  working on issues in the getting started tutorial milestone. Helping rgdd with
  sigsum docs/homepage review for CATS.
- kai: Trying to get the stauth milestones done before our meetup, so we have
  remote attestation working. It won't be 100% integrated, but at least data
  channel, id and feature list done.

## Other

- Presentations at the meetup?
  - TPM presentation by Kai
  - Secure Boot presentation by Philipp
- <add any other business here>
